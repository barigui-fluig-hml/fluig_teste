function displayFields(form,customHTML){
	
	try {
		
		form.setHidePrintLink(true);
		
		var obj_form_data = {
				
				modeView: String(form.getFormMode()),
				
				solicitante: {
						id: '',
						nome: '',
						email: ''
				},
				
				atividade: {
						id: String(getValue("WKNumState")),
						descricao: ''
				},
				
				responsavel: {
						id: String(getValue("WKUser")),
						nome: '',
						email: ''
				},
				
				solicitacao: {
						id: '',
						dataInicio: ''
				}
				
		}
		
		form.setVisibleById("S", false);
		form.setVisibleById("S0", false);
		form.setVisibleById("S1", false);
		form.setVisibleById("S2", false);
		form.setVisibleById("S3", false);
		
		switch (obj_form_data.atividade.id) {
			case "0":
				
				obj_form_data.atividade.id = '0';
				obj_form_data.atividade.descricao = 'inicio';
				
				form.setVisibleById("S", true);
				
				form.setValue('hid_sectionAtv',"atv0");
				
				var anexos = {
						//primaryFolder: '837050', //PROD
						primaryFolder: '14630', //HOMOLOG
						tree: []
				}
				
				form.setValue('hid_anexos',JSON.stringify(anexos));
				
				break;
			case "7":
				
				obj_form_data.atividade.id = '7';
				obj_form_data.atividade.descricao = 'inicio';
				
				form.setValue('hid_sectionAtv',"atv7");
				
				break;
			case "9":
				
				obj_form_data.atividade.id = '9';
				obj_form_data.atividade.descricao = 'Gestor Imediato';
				
				form.setValue('hid_sectionAtv',"atv9");
				
				break;
			case "13":
				
				obj_form_data.atividade.id = '13';
				obj_form_data.atividade.descricao = 'CEF';
				
				form.setValue('hid_sectionAtv',"atv13");
				
				break;
			case "75":
				
				obj_form_data.atividade.id = '40';
				obj_form_data.atividade.descricao = 'Fim';
				
				form.setValue('hid_sectionAtv',"atv40");
				
				break;
			default:
				
				break;
		}
		
		form.setValue('hid_infoForm',JSON.stringify(obj_form_data));
		
	} catch (e) {
		log.info("DISPLAY (14511 - Compras) FIELD ERROR :"+e)
	}
	
}