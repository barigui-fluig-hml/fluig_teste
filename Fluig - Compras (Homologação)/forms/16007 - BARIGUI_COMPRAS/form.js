class form {
	
	constructor(){
		
		this.NFe = {};
		
		this.modeView = null;
		
		this.solicitante = {
				id: null,
				nome: null,
				email: null
		}
		
		this.atividade = {
				id: null,
				descricao: null
		}
		
		this.responsavel = {
				id: null,
				nome: null,
				email: null
		}
		
		this.solicitacao = {
				id: null,
				dataInicio: null
		}
		
		this.obrigatorios = []
		
		try {
			
			if (!$('#hid_infoForm') || $('#hid_infoForm').length == 0 
					|| $('#hid_infoForm').val() == undefined 
					|| $('#hid_infoForm').val() == null
					|| $('#hid_infoForm').val() == "") {
				
				throw "As informações base do formulario não foram carregadas!!"
				
			}
			
			this.carregaAnexos(window.ANEXOS.listaAnexos());
			this.contaDocumentosAnexados();
			this._bindAnexo();
			
			var obj = JSON.parse($('#hid_infoForm').val());
			
			this.modeView = (obj.modeView == undefined || obj.modeView == null)? this.modeView : obj.modeView;
			this.solicitante = (obj.solicitante == undefined || obj.solicitante == null)? this.solicitante : obj.solicitante;
			this.atividade = (obj.atividade == undefined || obj.atividade == null)? this.atividade : obj.atividade;
			this.responsavel = (obj.responsavel == undefined || obj.responsavel == null)? this.responsavel : obj.responsavel;
			this.solicitacao = (obj.solicitacao == undefined || obj.solicitacao == null)? this.solicitacao : obj.solicitacao;
			
		} catch (e) {
			
			console.error(e);
			
		} finally {
			
			this._marcaCampoObrigatorio();
			
		}
		
	}
	
	carregaAnexos (list) {
		
		if (list.length > 0) {
			
			list.sort(function (a, b) {
				
				if (a.categoriaDoc_Descricao < b.categoriaDoc_Descricao)
					return -1
				
				if (b.categoriaDoc_Descricao < a.categoriaDoc_Descricao)
					return 1
				
				return 0;
				
			});
			
			var linhas = window.ANEXOS.formataListaPorCategoria(list);
			
			$('#anexos_listaAnexos').val(JSON.stringify(linhas));
			
		} else {
			
			window.parent.$('#workflowActions').hide();
			
			window.parent.$('button').each(function () {
				
				var attr = $(this).attr('data-send');
				
				if (typeof attr !== typeof undefined && attr !== false) {
					  
					$(this).closest('div').find('button').attr('disabled','disabled');
				
				}
				
			})
			
			_msgDanger("Erro ao carregar a lista de anexos.");
			throw "Erro ao carregar a lista de anexos.";
			
		}
		
	}
	
	contaDocumentosAnexados () {
		
		var list = [];
		
		if ($('#anexos_listaCarregados').val() != undefined && $('#anexos_listaCarregados').val() != null && $('#anexos_listaCarregados').val() != '')
			list = JSON.parse($('#anexos_listaCarregados').val());
		
		list = list.concat(window.ANEXOS.uploads);
		
		$('.anexo').find('span').html('0');
		
		for (var i in list) {
			
			var count = parseInt($('#' + list[i].key).find('span').html());
			count++;
			$('#' + list[i].key).find('span').html(count);
			$('#' + list[i].key).find('span').attr('style','background-color: #5bc0de !important');
			$('#' + list[i].key).find('span').css('color','white');
			
		}
		
	}
	
	_bindAnexo () {
		
		var that = this;
		
		var mod = JSON.parse($('#hid_infoForm').val()).modeView
		
		$('.anexo').each(function(){
			
			$(this).removeAttr('disabled');
			$(this).css('cursor','pointer');
			$(this).click(function() {
				
				$('.anexo').addClass('btn');
				
				$('.Anx_temps').remove();
				
				$(this).removeClass('btn');
				$(this).css('text-align','center');
				
				var btns = `<div style="padding-right: 2px;padding-left: 15px;margin-left: 2px;width: 47%;">
								<a class="file-input-wrapper btn btn-info" style="display: inline-grid; width: 100%;">
									<span>Anexar arquivo</span>
									<input id="Anx_temp_fileuploader" type="file" name="files" data-url="/ecm/upload" class="btn btn-primary btn-sm btn-block" title="Buscar Arquivo(s)" multiple/>
								</a>
							</div>
							<div style="padding-left: 15px;margin-right: 2px;float: right;width: 47%;padding-right: 0px;">
								<button class="btn btn-info btn-block" id="Anx_temp_googledrive" style="display: inline-grid;">
									<span>Buscar do Drive</span>
								</button>
							</div>`;
				
				var alertViewMod = `<span style="color: red;">É preciso assumir/movimentar a tarefa para anexar documentos ao processo</span>`;
				
				var temps = `<div class="Anx_temps" style="border-style: solid;">
								<div class="row">
									<div class="col-xs-12 col-md-12">
										<a class="btn-success btn-block" id="Anx_temp_visualizar" style="text-align: center; cursor: pointer;" data-toggle="popover" data-trigger="focus">Visualizar</a>
									</div>
								</div>
								<div class="row" style="margin-top: 5px; margin-bottom: 5px;display: flex; margin-right: auto;margin-left: auto;">
									` + ((mod == 'MOD' || mod == 'ADD')? btns : alertViewMod) + `
								</div>
							</div>`;
				
				$(this).closest('div').append(temps);
				that._bindTempsAnx($(this).attr('id'));
				
			});
			
		})
		
	}
	
	_bindTempsAnx (id) {
		
		var that = this;
		
		var func = async function () {
			
			that._bindUploads(id);
			that._bindClickVisualizar(id);
			
		}
		
		func();
		
	}
	
	_bindUploads (id) {
		
		var that = this;
		
		var callback = function (doc) {
			
			var count = $('#' + doc.key).find('span').html()
			count++;
			
			$('#' + doc.key).find('span').html(count);
			$('#' + doc.key).find('span').attr('style','background-color: #5bc0de !important');
			$('#' + doc.key).find('span').css('color','white');
			
		}
		
		window.ANEXOS.bindGoogleDrive(id,id,callback)
		window.ANEXOS.bindArquivoLocal($('#Anx_temp_fileuploader'),id,callback)
		
	}
	
	_bindClickVisualizar(id) {
		
		var that = this;
		
		$('#Anx_temp_visualizar').click(function() {
			
			var list = [];
			
			if ($('#anexos_listaCarregados').val() != undefined && $('#anexos_listaCarregados').val() != null && $('#anexos_listaCarregados').val() != '')
				list = JSON.parse($('#anexos_listaCarregados').val());
			
			var tipos = JSON.parse($('#anexos_listaAnexos').val());
			
			window.ANEXOS.clickShowAnexo(tipos,list,id);
			
		})
		
	}
	
	_loadSection () {
		
		console.error("Metodo de load não implementado ou não localizado");
		
	}
	
	_marcaCampoObrigatorio () {
		
		var that = this;
		
		var func = function () {
			
			if (that.obrigatorios.find(x => x == $(this).attr('id')) == undefined) {
				
				$(this).prop('obrigatorio',true);
				that.obrigatorios.push($(this).attr('id'));
				
			}
			
		}
		
		$('input:visible[obrigatorio=true]').each(func);
		$('select:visible[obrigatorio=true]').each(func);
		$('textarea:visible[obrigatorio=true]').each(func);
		
	}
	
	_getServer () {
		var y = location.host;
		var x = location.protocol;
		var url = x+"//"+y;
		return(url);
	}
	
	_getData (dataset,constrants = null) {
		
		return DatasetFactory.getDataset(dataset,null,constrants,null).values;
		
	}
	
	_initSelectInput (id,values) {
		
		$('#' + id).html(`<option value="">==SELECIONE==</option>`)
		
		values.forEach(function(v) {
			
			$('#' + id).append(`<option value="` + v.key + `">` + v.value + `</option>`)
			
		})
		
	}
	
	_msgDanger (msg,titlex) {
		titlex = (this._validaObj(titlex))?titlex:"";
		FLUIGC.toast({
	        title:titlex,
	        message:msg,
	        type:"danger" 
	    });
	}
	
	_msgWarning (msg,titlex) {
		titlex = (this._validaObj(titlex))?titlex:"";
		FLUIGC.toast({
	        title:titlex,
	        message:msg,
	        type:"warning" 
	    });
	}
	
	_msgSuccess (msg,titlex) {
		titlex = (this._validaObj(titlex))? titlex:"";
		
		FLUIGC.toast({
	        title:titlex,
	        message:msg,
	        type:"success" 
	    });
	}
	
	_validaObj (obj) {
		 if (obj != null && 
			 obj != "null" &&
			 obj != undefined && 
			 obj != "undefined" && 
			 obj != false &&
			 obj != "false" &&
			 obj != "") {
			return(true);
		}else{
			return(false);
		}
	}
	
	_ValidateForm () {
		
		var that = this;
		
		that._marcaCampoObrigatorio();
		
		$('.error').removeClass('error');
		
		that.obrigatorios.forEach(function (input) {
			
			if (!$('#' + input).val() || $('#' + input).val() == undefined
					|| $('#' + input).val() == null || $('#' + input).val().trim() == "") {
				
				$('#' + input).addClass('error');
				
			}
			
		});
		
		if ($('.error').length > 0) {
			
			that._msgDanger('Preencha os campos obrigatorios em destaque');
			return false;
			
		}
			
		return true;
		
	}
	
	_sectionValidate () {
		
		return true;
		
	}
	
	_startLoading(mensagem){
    	window.FORMULARIO.loadingForm = FLUIGC.loading(window, {
    	    textMessage: mensagem || 'Aguarde...'
    	});
    	window.FORMULARIO.loadingForm.show();
    }
    
    _stopLoading(){
    	setTimeout(function(){
    		window.FORMULARIO.loadingForm.hide();	
    	},500)
    }
	
	toString() { return JSON.stringify(this); }
	
}