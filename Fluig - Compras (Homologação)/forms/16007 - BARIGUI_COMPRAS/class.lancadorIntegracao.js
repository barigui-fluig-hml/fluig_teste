class lancadorIntegracao {
	
	formataAnexos() {
		if ($('#hid_folder').val() != '') {
			
			var prefix = $('#hid_folder').val();
			if (prefix.charAt((prefix.length - 1)) === '/') { prefix = prefix.substr(0, (prefix.length - 1)) }
			$('#hid_folder').val(prefix);
		}
		
		var that = this
		
		window.ANEXOS._createFolder();
		
		$('#S0_hid_isNFe').val('false'.toUpperCase());
		$('.S0-dadosRecebedor').show();
		$('#S0_tipo').val('DESPESA');
		
		var S0_hid_link_nota = eval($('#S0_hid_link_nota').val());
		var S0_hid_link_boleto = eval($('#S0_hid_link_boleto').val());
		var S0_hid_link_rateio = eval($('#S0_hid_link_rateio').val());
		var S0_hid_link_outros = eval($('#S0_hid_link_outros').val());
		
		for (var i in S0_hid_link_nota) {
			that.__createDocs('S0_nota',S0_hid_link_nota[i],'NF' + Math.floor(Math.random() * (4095 - 256) + 256).toString(16) + '.pdf')
		}
		
		for (var i in S0_hid_link_boleto) {
			that.__createDocs('S0_boleto',S0_hid_link_boleto[i],'Boleto' + Math.floor(Math.random() * (4095 - 256) + 256).toString(16) + '.pdf')
		}
		
		for (var i in S0_hid_link_rateio) {
			that.__createDocs('S0_rateio',S0_hid_link_rateio[i],'Rateio' + Math.floor(Math.random() * (4095 - 256) + 256).toString(16) + '.pdf')
		}

		for (var i in S0_hid_link_outros) {
			that.__createDocs('S0_outros',S0_hid_link_outros[i],'Arquivo' + Math.floor(Math.random() * (4095 - 256) + 256).toString(16) + '.pdf')
		}
			
		$('#S0_hid_link_nota').val('')
		$('#S0_hid_link_boleto').val('')
		$('#S0_hid_link_rateio').val('')
		$('#S0_hid_link_outros').val('')
		
	}
	
	__createDocs (tipo,hash,file) {
		
		var that = this
		
		var constraint = [DatasetFactory.createConstraint('hash',hash,hash,ConstraintType.MUST)];
		var data = DatasetFactory.getDataset('dsBARIGUI_getDoc_Intranet',null,constraint,null).values;
		
		if (data[0] != undefined && data[0] != null && data[0].BASE64 != undefined && data[0].BASE64 != null && data[0].BASE64 != "") {
			
			var str = [];
			
			for (var i in data) {
			
				str.push(data[i].BASE64);
			
			}
			
			var mime = 'application/pdf';
			console.log(str.join(''))
			var blob = window.ANEXOS.base64toBlob(str.join(''),mime);
			var form = new FormData();
			form.append("blob", blob, window.ANEXOS.renameFile(file));
			
			$.ajax({
	            async : false,
	            type : 'POST',
	            processData: false,
	            contentType: false,
	            url : '/ecm/upload',
	    		data: form,
	    		error: function(e) {
	    			console.error('error on load file')
	    		},
	    		success: function(data) { 
	    			
	    			var f = JSON.parse(data).files[0]
	    			
	    			var doc = {
							key: tipo,
							value: {
								link: URL.createObjectURL(blob),
								thumb: '',
								thumbName: '',
								name: f.name,
								createDate: new Date()
							}
						}
	    			
	    			var value = DatasetFactory.getDataset(
		        					'dsGeraThumbnail',
		        					null,
		        					[DatasetFactory.createConstraint('fileName',f.name,f.name,ConstraintType.MUST)],
		        					null
		        		    	).values[0];
	    			
	    			doc.value.name = value.fileName;
        			var blobThumb = that.base64toBlob(value.base64,'application/pdf');
	    			var blob_link = URL.createObjectURL(blobThumb);
	    			
	    			if (value.resultado == 'OK') {
	    				doc.value.thumb = blob_link;
	    				doc.value.thumbName = value.thumbName;
	    			}
	    			
	    			window.ANEXOS.uploads.push(doc);
	    			window.FORMULARIO.contaDocumentosAnexados()
	    			
	    		}
	    	});
			
		}
		
	}
	
	base64toBlob(base64Data, contentType) {
		
		try {
			
	        contentType = contentType || '';
	        var sliceSize = 1024;
	        var byteCharacters = atob(base64Data);
	        var bytesLength = byteCharacters.length;
	        var slicesCount = Math.ceil(bytesLength / sliceSize);
	        var byteArrays = new Array(slicesCount);
	        for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
	            var begin = sliceIndex * sliceSize;
	            var end = Math.min(begin + sliceSize, bytesLength);
	            var bytes = new Array(end - begin);
	            for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
	                bytes[i] = byteCharacters[offset].charCodeAt(0);
	            }
	            byteArrays[sliceIndex] = new Uint8Array(bytes);
	        }
	        
	        return new Blob(byteArrays, { type: contentType });
	        
		} catch (e) {
			
			return new Blob();
			
		}
		
    }
	
	
}