function compras_init() {
	
	window.parent.$('#tab-attachments').closest('li').remove();
	window.UTILS = new atvUtil();
	
	const PRODUCAO	= (getServer() == "https://fluig.grupobarigui.com.br")?true:false 
	console.log("PRODUCAO",PRODUCAO);

	var anexosJson = {  processCod: 'BARIGUI_COMPRAS', 
						processoTipo_Descricao: 'Centro de Escrituração Fiscal - Anexos', 
						processoTipo_PastaProd: 'prod',
						processoTipo_PastaHomolog: 'homolog',
						isProd: PRODUCAO };
	
	window.ANEXOS = new anexos(JSON.stringify(anexosJson));
	
	window.FORMULARIO = eval('new ' + $('#hid_sectionAtv').val() + '()');
	window.UTILS._beforLoadSection();
	
	if ($('#hid_sectionAtv').val() == 'atv0')
		window.ANEXOS._createFolder();
	
	window.FORMULARIO._loadSection();
	window.UTILS._afterLoadSection();
	
	window.parent.$('.dropdown-menu.js-option-list-drop').find('a[data-save]').click(function(event) {
		var flag = window.ANEXOS.uploadToBucket();
		window.FORMULARIO.contaDocumentosAnexados();
	})
	
	window.parent.$('.dropdown-menu.js-option-list-drop').find('a[data-transfer]').click(function(event) {
		var flag = window.ANEXOS.uploadToBucket();
		window.FORMULARIO.contaDocumentosAnexados();
	})
	
	bind();
	
}

function getServer () {
	var y = location.host;
	var x = location.protocol;
	var url = x+"//"+y;
	return(url);
}

function bind() {
	
	$('.money').mask("#.##0,00", {reverse: true});
	
	FLUIGC.calendar('.calendar', {
	    pickDate: true,
	    pickTime: false
	});
	
	/*$('.calendar').keydown(function(e){
		$(this).val($(this).val());
		e.preventDefault();
	})*/
	
	$('.calendar').on('keydown', function(e){
		if (e.which == 8)
			$(this).val('');
		else
			$(this).val($(this).val());
		
		e.preventDefault();
	})
	
	$('.cpf').change(function () {
		
		$(this).removeClass('error');
		
		if (!TestaCPF($(this).val())) {
			
			$(this).addClass('error');
			$(this).val('');
			
		}
		
	})
	
	$('.cnpj').change(function () {
		
		$(this).removeClass('error');
		
		if (!TestaCNPJ($(this).val())) {
			
			$(this).addClass('error');
			$(this).val('');
			
		}
		
	})
	
}

function TestaCNPJ (cnpj) {
	
	cnpj = cnpj.replace(/[^\d]+/g,'');
	 
    if(cnpj == '') return false;
     
    if (cnpj.length != 14)
        return false;
 
    // Elimina CNPJs invalidos conhecidos
    if (cnpj == "00000000000000" || 
        cnpj == "11111111111111" || 
        cnpj == "22222222222222" || 
        cnpj == "33333333333333" || 
        cnpj == "44444444444444" || 
        cnpj == "55555555555555" || 
        cnpj == "66666666666666" || 
        cnpj == "77777777777777" || 
        cnpj == "88888888888888" || 
        cnpj == "99999999999999")
        return false;
         
    // Valida DVs
    tamanho = cnpj.length - 2
    numeros = cnpj.substring(0,tamanho);
    digitos = cnpj.substring(tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(0))
        return false;
         
    tamanho = tamanho + 1;
    numeros = cnpj.substring(0,tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1))
          return false;
           
    return true;
	
}

function TestaCPF(CPF) {
	
	var strCPF = CPF.replace(/[^\d]+/g,'');
    var Soma;
    var Resto;
    Soma = 0;
  if (strCPF == "00000000000") return false;
     
  for (i=1; i<=9; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
  Resto = (Soma * 10) % 11;
   
    if ((Resto == 10) || (Resto == 11))  Resto = 0;
    if (Resto != parseInt(strCPF.substring(9, 10)) ) return false;
   
  Soma = 0;
    for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
    Resto = (Soma * 10) % 11;
   
    if ((Resto == 10) || (Resto == 11))  Resto = 0;
    if (Resto != parseInt(strCPF.substring(10, 11) ) ) return false;
    return true;
}

function formataFileName(file) {
	
	var arr = file.split('.');
	
	var str = arr[0];
	
	str = str.trim().split(/\s/g).join('_').split(/\t/g).join('_').split(/\s{2,}/g).join('_').split(/(\r\n|\n|\r)/g).join("_").trim();
	
	var spChars = [`º`,`ª`,`/`,`?`,`|`,`"`,`!`,`@`,`#`,`$`,`%`,`¨`,`&`,`*`,`-`,`+`,`=`,`§`,`^`,`~`,`°`,`:`,`.`,`º`,`ª`,`/`,`?`,`|`,`´`,"`","'",`;`,`>`,`<`];
	
	for (var i in spChars) {
		
		str = str.split(spChars[i]).join('_');
		
	}
	
	str = str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
	
	return str + '.' + arr[arr.length - 1];
	
}

var beforeSendValidate = function (numState, nextState) {
	
	if (window.FORMULARIO._ValidateForm() && window.FORMULARIO._sectionValidate()) {
		
		//Upload files
		flag = window.ANEXOS.uploadToBucket();
		window.FORMULARIO.contaDocumentosAnexados();
		
		if (flag)
			$('input').removeAttr('disabled');
			$('select').removeAttr('disabled');
			
		return flag;
		
	}
	
	return false;
	
}