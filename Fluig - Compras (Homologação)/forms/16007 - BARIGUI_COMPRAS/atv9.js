class atv9 extends form {
	
	constructor(){
		
		super();
		
		$(".S0-disabled").addClass("disabled");
		$( ".S0-disabled" ).keydown(function() {
			return false
		});
		
		$('.S0_forComAF').hide();
		
		if ($('#S0_hid_isNFe').val() != undefined && $('#S0_hid_isNFe').val() != null && $('#S0_hid_isNFe').val().toUpperCase() != 'false'.toUpperCase()) {
			
			$('#S0_forNFe').show();
			
			if ($('#S0_hid_link_xml').val() == '') {
				
				$('#S0_xml').closest('div').hide();
				$('#S0_hid_link_xml').val('');
				
			} else {
				
				$('#S0_xml').attr('chave',$('#S0_link_xml').val());
				//$('#S0_xml').attr('download','xml_nota_' + $('#S0_numnota').val() + '.xml');
				
			}
			
			$('#S0_xml').click(function(){
				
				var data = {
					companyId: 1,
					serviceCode: 'CapturaNFE',
					endpoint : '/api/document/?key='+$(this).attr('chave')+'&format=xml&download=true&encode=false',
					method: 'get',
					timeoutService: '100'
				};
				
				window.parent.WCMAPI.Create({
		    	    url: '/api/public/2.0/authorize/client/invoke',
		    	    contentType: "text/json",
		    	    dataType: "json",
		    	    async: false,
		    	    data: JSON.stringify(data),
		    	    success: function(data){
		    	    	
		    	    	var result = data.content.result.split('"').join('')
		    	    	var base64 = btoa(result)
		    	    	var blob = window.ANEXOS.base64toBlob(base64,"text/xml")
		    	    	const url = window.URL.createObjectURL(blob);
		    	        const a = document.createElement('a');
		    	        a.style.display = 'none';
		    	        a.href = url;
		    	        a.download = 'xml-' + $('#S0_numnota').val() + '.xml';
		    	        document.body.appendChild(a);
		    	        a.click();
		    	        window.URL.revokeObjectURL(url);
		    	    	
		    	    }
				})
				
			})
			
		}
		
		window.UTILS.bindNumParcelas();
		
	}
	
	_loadSection () {
		
		var that = this;
		
		$('#S1_aprovacao').val('');
		
		$('#S0').show();
		$('#S1').show();
		
		$('#S1_Obs').val('');
		
	}
	
	_sectionValidate () {
		
		var that = this;
		
		if ($('#S1_aprovacao').val().toUpperCase() == 'sim'.toUpperCase()) {
			
			if ($('#S0_dtvencimento').val() != undefined && $('#S0_dtvencimento').val() != null && $('#S0_dtvencimento').val() != "") {
				
				var S0_dtvencimento = $('#S0_dtvencimento').val();
				var arr = S0_dtvencimento.split('/');
				var dt = new Date(arr[2] + '-' + arr[1] + '-' + arr[0]);
				var aux = new Date();
				
				aux.setDate(aux.getDate() + 4);
				
				if (dt < aux) {
					
					this._msgDanger('Data de Vencimento não permitida, necessário solicitar ao Fornecedor nova data de pagamento e sem acréscimo de juros conforme Política de Compra.');
					$('#S0_dtvencimento').addClass('error');
					return false;
					
				}
				
			}
		
		} else if ($('#S1_aprovacao').val().toUpperCase() == 'nao'.toUpperCase()) {
			
			if ($('#S1_Obs').val() == undefined || $('#S1_Obs').val() == null || $('#S1_Obs').val() == "") {
				
				$('#S1_Obs').addClass('error');
				
				that._msgDanger('O Campo observação é obrigatório');
				return false;
				
			}
			
		} else {
			
			$('#S1_aprovacao').addClass('error');
			that._msgDanger('Resposta inválida!');
			return false;
			
		}
		
		return true;
		
	}
	
}