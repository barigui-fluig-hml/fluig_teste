class telemetrixIntegracao {
	
	constructor(mod){
		
		this.modeView = mod || 'VIEW'
		
	}
	
	formataAnexos() {
		var that = this
				
		if ($('#hid_folder').val() != '') {
			
			var prefix = $('#hid_folder').val();
			if (prefix.charAt((prefix.length - 1)) === '/') { prefix = prefix.substr(0, (prefix.length - 1)) }
			$('#hid_folder').val(prefix);
		}
		
		if (that.modeView == 'MOD' || that.modeView == 'ADD') {
		
			window.ANEXOS._createFolder();
			var S0_hid_link_nota = eval($('#S0_hid_link_nota').val());
			var S0_hid_link_boleto = eval($('#S0_hid_link_boleto').val());
			var S0_hid_link_rateio = eval($('#S0_hid_link_rateio').val());
			var S0_hid_link_outros = eval($('#S0_hid_link_outros').val());
			var S0_hid_link_xml =  eval($('#S0_hid_link_xml').val());
			
			if (S0_hid_link_xml && S0_hid_link_xml != undefined && S0_hid_link_xml != null && S0_hid_link_xml.length > 0) {
				that.__saveXML(S0_hid_link_xml)
			}
			
			for (var i in S0_hid_link_nota) {
				that.__createDocs('S0_nota',S0_hid_link_nota[i],'NF' + Math.floor(Math.random() * (4095 - 256) + 256).toString(16) + '.pdf')
			}
			
			for (var i in S0_hid_link_boleto) {
				that.__createDocs('S0_boleto',S0_hid_link_boleto[i],'Boleto' + Math.floor(Math.random() * (4095 - 256) + 256).toString(16) + '.pdf')
			}
			
			for (var i in S0_hid_link_rateio) {
				that.__createDocs('S0_rateio',S0_hid_link_rateio[i],'Rateio' + Math.floor(Math.random() * (4095 - 256) + 256).toString(16) + '.pdf')
			}
	
			for (var i in S0_hid_link_outros) {
				that.__createDocs('S0_outros',S0_hid_link_outros[i],'Arquivo' + Math.floor(Math.random() * (4095 - 256) + 256).toString(16) + '.pdf')
			}
			
			$('#S0_hid_link_nota').val('')
			$('#S0_hid_link_boleto').val('')
			$('#S0_hid_link_rateio').val('')
			$('#S0_hid_link_outros').val('')
			$('#S0_hid_link_xml').val('')
		
		} else {
			
			$('.anexo').click(function() {
				
				var div = $(this).closest('div');
				var html = $(div).html();
				
				html = '<span style="color: red;">É preciso assumir/movimentar a tarefa para visualizar/anexar documentos ao processo</span>' + html;
				
				$(div).html('');
				$(div).append(html);
				
			});
			
			//that._msgDanger('É preciso assumir/movimentar a tarefa para visualizar os documentos corretamente');
			
			$('.anexo').removeAttr('disabled');
			$('.anexo').css('cursor','pointer');
			
		}
		
		
	}
	
	__saveXML (link) {
		
		var that = this
		
		fetch(link).then(function (res) {
			res.text().then(function(xmpString) { 
				
				var blob = new Blob([xmpString], {type: "text/xml"});
				
				var form = new FormData();
				form.append("blob", blob, 'XML_nota.xml');
				
				$.ajax({
	                async : false,
	                type : 'POST',
	                processData: false,
	                contentType: false,
	                url : '/ecm/upload',
	        		data: form,
	        		error: function(e) {
	        			console.error('error on load file')
	        		},
	        		success: function(data) {
	        			
	        			$('#S0_link_xml').val('XML_nota.xml');
	        			$('#S0_xml').attr('href',URL.createObjectURL(blob));
        				$('#S0_xml').attr('download','xml_nota_' + $('#S0_numnota').val() + '.xml');
        				
	        		}
	        	});
				
			})
		})
		
	}
	
	__createDocs (tipo,link,file) {
		var that = this
		
		fetch(link).then(function (res) {
			res.blob().then(function(blob) {
				
				var form = new FormData();
				form.append("blob", blob, window.ANEXOS.renameFile(file));
				
				$.ajax({
		            async : false,
		            type : 'POST',
		            processData: false,
		            contentType: false,
		            url : '/ecm/upload',
		    		data: form,
		    		error: function(e) {
		    			console.error('error on load file')
		    		},
		    		success: function(data) { 
		    			
		    			var f = JSON.parse(data).files[0]
		    			
		    			var doc = {
								key: tipo,
								value: {
									link: URL.createObjectURL(blob),
									thumb: '',
									thumbName: '',
									name: f.name,
									createDate: new Date()
								}
							}
		    			
		    			var value = DatasetFactory.getDataset(
			        					'dsGeraThumbnail',
			        					null,
			        					[DatasetFactory.createConstraint('fileName',f.name,f.name,ConstraintType.MUST)],
			        					null
			        		    	).values[0];
		    			doc.value.name = value.fileName;
	        			var blobThumb = that.base64toBlob(value.base64,'application/pdf');
		    			var blob_link = URL.createObjectURL(blobThumb);
		    			
		    			if (value.resultado == 'OK') {
		    				doc.value.thumb = blob_link;
		    				doc.value.thumbName = value.thumbName;
		    			}
		    			
		    			window.ANEXOS.uploads.push(doc);
		    			window.FORMULARIO.contaDocumentosAnexados()
		    			
		    		}
		    	});
				
			})
		})
		
	}
	
	base64toBlob(base64Data, contentType) {
		try {
			
	        contentType = contentType || '';
	        var sliceSize = 1024;
	        var byteCharacters = atob(base64Data);
	        var bytesLength = byteCharacters.length;
	        var slicesCount = Math.ceil(bytesLength / sliceSize);
	        var byteArrays = new Array(slicesCount);
	        for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
	            var begin = sliceIndex * sliceSize;
	            var end = Math.min(begin + sliceSize, bytesLength);
	            var bytes = new Array(end - begin);
	            for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
	                bytes[i] = byteCharacters[offset].charCodeAt(0);
	            }
	            byteArrays[sliceIndex] = new Uint8Array(bytes);
	        }
	        
	        return new Blob(byteArrays, { type: contentType });
	        
		} catch (e) {
			
			return new Blob();
			
		}
		
    }
	
}