//Classe desenvolvida para execução de funções e eventos comuns a todas as atividades do processo, porem (diferente do form.js) com regras
//especificas para o processo
class atvUtil extends UtilityCore {
	
	constructor() { super(); }
	
	_afterLoadSection() {
		
		$('section').find('input').change(function(){
			
			$(this).val($(this).val().toUpperCase())
			
		})
		
		$('section').find('textarea').change(function(){
			
			$(this).val($(this).val().toUpperCase())
			
		})
		
		$('section').find('input').css('text-transform','uppercase');
		$('section').find('textarea').css('text-transform','uppercase');
		
	}
	
	_beforLoadSection() {
		
		this.configuraS0();
		
	}
	
	configuraS0 () {
		
		$('.error').removeClass('error');
		
		$(".S0-dadosClassNF").hide();
		
		$('.S0_forComAF').hide();
		$('#S0_contagerencial').closest('div').hide();
		$('#S0_dtvencimento').closest('div').hide();
		$('#S0_condicaoPagamento').closest('div').hide();
		$('#S0_resumoRateio').closest('div').hide();
		$('#S0_boleto').closest('div').hide();
		$('#S0_rateio').closest('div').hide();
		$('#S0_outros').closest('div').hide();
		$('#S0_camposSelecao').closest('div').hide();
		$('#S0_infoNFSP').closest('div').hide();
		$('#S0_check_NFSP').closest('div').hide();
		$('#S0_numOS').closest('div').hide();
		$('#S0_nraf').closest('div').hide();
		$('#S0_placa').closest('div').hide();
		$('#S0_informacaoPlaca').closest('div').hide();
		$('.S0-dadosPagamento').hide();
		$('.S0-dadosDeposito').hide();
		$('.S0-dadosPix').hide();
		
		var hid_infoForm = JSON.parse($('#hid_infoForm').val());
		if (hid_infoForm.atividade.id != '0') {
			
			if (hid_infoForm.modeView == "VIEW" && $('#S0_informaRateio').html() != '==SELECIONE==') {
				$('#S0_informaRateio').closest('div').show();
			
			} else if ($('#S0_informaRateio').val() != '') {
				$('#S0_informaRateio').closest('div').show();
			
			} else {
				$('#S0_informaRateio').closest('div').hide();
			}
		}
		
		window.FORMULARIO.obrigatorios = [];
		
		//$('#S0_xml').attr('href',$('#S0_hid_link_xml').val());
		
		if ($('#S0_complemento') && $('#S0_complemento').length > 0 
				|| (($('#S0_complemento').val() != undefined 
						&& $('#S0_complemento').val() != null
						&& $('#S0_complemento').val() != "")
				|| ($('#S0_complemento').html() != undefined 
						&& $('#S0_complemento').html() != null
						&& $('#S0_complemento').html() != ""))) {
			
			var S0_complemento = ($('#S0_complemento').val() != undefined 
											&& $('#S0_complemento').val() != null
											&& $('#S0_complemento').val() != "") ? 
													$('#S0_complemento').val() : $('#S0_complemento').html();
			
			$('#S0_complemento').val(S0_complemento.toUpperCase())
													
			switch (S0_complemento.toUpperCase()) {
			
				case "Marketing".toUpperCase():
					$(".S0-dadosClassNF").show();
					$('.S0_forComAF').hide();
					$('#S0_contagerencial').closest('div').show();
					$('#S0_dtvencimento').closest('div').show();
					$('#S0_condicaoPagamento').closest('div').show();
					$('#S0_resumoRateio').closest('div').show();
					$('#S0_boleto').closest('div').show();
					$('#S0_rateio').closest('div').show();
					$('#S0_outros').closest('div').show();
					$('.S0-dadosPagamento').show();
					break;
				case "Extraordinária".toUpperCase():
					$(".S0-dadosClassNF").show();
					$('.S0_forComAF').hide();
					$('#S0_contagerencial').closest('div').show();
					$('#S0_dtvencimento').closest('div').show();
					$('#S0_condicaoPagamento').closest('div').show();
					$('#S0_resumoRateio').closest('div').show();
					$('#S0_boleto').closest('div').show();
					$('#S0_rateio').closest('div').show();
					$('#S0_outros').closest('div').show();
					$('.S0-rowAprovador').hide();
					$('.S0-dadosPagamento').show();
					break;
				case "Nota Fiscal sem Pagamento".toUpperCase():
					$(".S0-dadosClassNF").show();
					$('.S0_forComAF').hide();
					//$('#S0_contagerencial').closest('div').show();
					$('#S0_resumoRateio').closest('div').show();
					$('#S0_rateio').closest('div').show();
					$('#S0_outros').closest('div').show();
					$('#S0_camposSelecao').closest('div').show();
					$('#S0_infoNFSP').closest('div').show();
					$('#S0_check_NFSP').closest('div').show();
					$('.S0-rowAprovador').hide();
					break;
				case "Tecnologia da Informação".toUpperCase():
					$(".S0-dadosClassNF").show();
					$('.S0_forComAF').hide();
					$('#S0_contagerencial').closest('div').show();
					$('#S0_dtvencimento').closest('div').show();
					$('#S0_condicaoPagamento').closest('div').show();
					$('#S0_resumoRateio').closest('div').show();
					$('#S0_boleto').closest('div').show();
					$('#S0_rateio').closest('div').show();
					$('#S0_outros').closest('div').show();
					$('.S0-rowAprovador').hide();
					$('.S0-dadosPagamento').show();
					break;
				case "Oficina".toUpperCase():
					$(".S0-dadosClassNF").show();
					$('.S0_forComAF').hide();
					//$('#S0_contagerencial').closest('div').show();
					$('#S0_dtvencimento').closest('div').show();
					$('#S0_condicaoPagamento').closest('div').show();
					$('#S0_resumoRateio').closest('div').show();
					$('#S0_boleto').closest('div').show();
					$('#S0_rateio').closest('div').show();
					$('#S0_outros').closest('div').show();
					$('#S0_numOS').closest('div').show();
					$('#S0_nraf').closest('div').show();
					$('.S0-dadosPagamento').show();
					break;
				case "Seminovos".toUpperCase():
					$(".S0-dadosClassNF").show();
					$('.S0_forComAF').hide();
					//$('#S0_contagerencial').closest('div').show();
					$('#S0_dtvencimento').closest('div').show();
					$('#S0_condicaoPagamento').closest('div').show();
					$('#S0_resumoRateio').closest('div').show();
					$('#S0_boleto').closest('div').show();
					$('#S0_rateio').closest('div').show();
					$('#S0_outros').closest('div').show();
					$('#S0_placa').closest('div').show();
					$('#S0_informacaoPlaca').closest('div').show();
					$('.S0-dadosPagamento').show();
					break;
				case "Com AF - Telemetrix".toUpperCase():
					$(".S0-dadosClassNF").show();
					$('.S0_forComAF').show();
					$('#S0_contagerencial').closest('div').show();
					$('#S0_dtvencimento').closest('div').show();
					$('#S0_condicaoPagamento').closest('div').show();
					$('#S0_resumoRateio').closest('div').show();
					$('#S0_boleto').closest('div').show();
					$('#S0_rateio').closest('div').show();
					$('#S0_outros').closest('div').show();
					$('.S0-dadosPagamento').show();
					break;
				case "COM AF - BASE B":
					$(".S0-dadosClassNF").show();
					$('.S0_forComAF').show();
					$('#S0_contagerencial').closest('div').show();
					$('#S0_dtvencimento').closest('div').show();
					$('#S0_condicaoPagamento').closest('div').show();
					$('#S0_resumoRateio').closest('div').show();
					$('#S0_boleto').closest('div').show();
					$('#S0_rateio').closest('div').show();
					$('#S0_outros').closest('div').show();
					$('.S0-dadosPagamento').show();
					break;
				case "Novo Varejo".toUpperCase():
					$(".S0-dadosClassNF").show();
					$('.S0_forComAF').hide();
					$('#S0_contagerencial').closest('div').show();
					$('#S0_dtvencimento').closest('div').show();
					$('#S0_condicaoPagamento').closest('div').show();
					$('#S0_resumoRateio').closest('div').show();
					$('#S0_boleto').closest('div').show();
					$('#S0_rateio').closest('div').show();
					$('#S0_outros').closest('div').show();
					$('.S0-dadosPagamento').show();
					break;
		
			}

		}
		
		if ($('#S0_condicaoPagamento').val().toUpperCase() == 'Depósito'.toUpperCase() || $('#S0_condicaoPagamento').html().toUpperCase() == 'Depósito'.toUpperCase()) {
			
			$('.S0-dadosDeposito').show();
			
		}
		
		if ($('#S0_condicaoPagamento').val().toUpperCase() == 'Pix'.toUpperCase() || $('#S0_condicaoPagamento').html().toUpperCase() == 'Pix'.toUpperCase()) {
			
			$('.S0-dadosPix').show();
			
		}
		
		
	}
	
	bindNumParcelas () {
		
		var str2date = function (str) {
			
			if(str == null || str == '')
				return null;
			var partes = str.split(' ');
			
			partes[1] = '00:00:00.001';
			
			var arrdata = partes[0].split('/').reverse();
			var arrhora = partes[1].split(':');
			
			var rtn = new Date(arrdata[0], arrdata[1], arrdata[2], arrhora[0], arrhora[1], arrhora[2]);
			rtn.setMonth(rtn.getMonth() - 1);
			rtn.setHours(rtn.getHours() - 3);

			return rtn;
			
		}
		
		var hid_infoForm = JSON.parse($('#hid_infoForm').val());
		
		$('#S0_dtvencimento').unbind();
		$('#S0_dtvencimento').attr('readonly','');
		
		var isAction = window.FORMULARIO != undefined? (window.FORMULARIO.atividade.id == '0' || window.FORMULARIO.atividade.id == '7') : false;

		var isCef = false
		
		if (!isAction && window.FORMULARIO != undefined && (window.FORMULARIO.atividade.id == '0' || window.FORMULARIO.atividade.id == '7'))
			if ($('#S0_numParcelas').val() == undefined || $('#S0_numParcelas').val() == null || $('#S0_numParcelas').val() == '' || $('#S0_numParcelas').val() == '0')
				isAction = true;
		
		if (window.FORMULARIO != undefined && window.FORMULARIO.atividade.id == '13') {
			
			isAction = true;
			isCef = true;
			
		}
		
		if (hid_infoForm.modeView == 'VIEW')
			isAction = false;
			
		var view = { linhas: [] };
		
		var val = $('#hid_parcelas').val();
		
		if (val != undefined && val != null && val != "") {
			
			$('#hid_parcelas').val(val.split(`'`).join(`"`));
			
			var json = JSON.parse(val.split(`'`).join(`"`));
			
			if (json.length != $('#S0_numParcelas').val()) {
				
				$('#S0_numParcelas').val(json.length);
				
			}
			
			view.linhas = json;
				
		} else {
			
			$('#S0_numParcelas').val(0);
			
		}
		
		var template = `<div class="col-xs-12 col-md-12">
							<div class="col-xs-12 col-md-12" id="temp_div_parcelas">
							{{#linhas}}
								<div id="div_parcela_{{numero}}" class="col-xs-12 col-md-12 div_parcela" style="margin-top:10px;">
									<div class="col-xs-12 col-md-3">
										<label for="temp_numParcela_{{numero}}">Nº Parcela</label>
										<input type="text" id="temp_numParcela_{{numero}}" name="temp_numParcela_{{numero}}" class="form-control temp_numParcela" value="{{numero}}" readonly/>
									</div>
									<div class="col-xs-12 col-md-3">
										<label for="temp_data_{{numero}}">Data Vencimento</label>
										<input type="text" id="temp_data_{{numero}}" name="temp_data_{{numero}}" class="form-control temp_data" value="{{data_vencimento}}" ` + (isAction? ( !isCef? `` : `readonly`):`readonly`) +  ` obrigatorio="true" onkeydown="return false"/>
									</div>
									<div class="col-xs-12 col-md-3">
										<label for="temp_valor_{{numero}}">Valor</label>
										<input type="text" id="temp_valor_{{numero}}" name="temp_valor_{{numero}}" class="form-control temp_valor" value="{{valor}}" ` + (isAction? ``:`readonly`) +  ` obrigatorio="true"/>
									</div>
									<div class="col-xs-12 col-md-3" style="text-align: center;display: grid;">
										<label for="{{numero}}">Remover</label>
										<a class="temp_link_del" id="{{numero}}" name="{{numero}}" style="cursor: not-allowed;">
											<i class="fluigicon fluigicon-trash icon-sm"></i>
										</a>
									</div>
								</div>
							{{/linhas}}
							</div>
							` +  (isAction ? (!isCef? `
							<div class="col-xs-12 col-md-12" style="margin-top:10px;">
								<div class="col-xs-12 col-md-2" style="float: right;">
									<buttom class="btn btn-info btn-block" id="temp_btn_add">Adicionar Parcela</buttom>
								</div>
							</div>` : ``) : ``) +
						`</div>`;
		
		$('#S0_div_dadosParcela').html('');
		$('#S0_div_dadosParcela').append(Mustache.render(template,view));
		
		$('.temp_link_del').css('cursor','not-allowed');
		
		if (isAction) {
			
			$('.temp_valor').mask("#.##0,00", {reverse: true});
			
			var onChange = function () {
				
				$('#S0_numParcelas').val($('#temp_div_parcelas').find('.temp_numParcela').length);
				
				var parcelas = [];
				
				$('#temp_div_parcelas').children('div').each(function () {
					
					parcelas.push({
						numero: $(this).find('.temp_numParcela').val(),
						data_vencimento: $(this).find('.temp_data').val(),
						valor: $(this).find('.temp_valor').val()
					});
					
				});
				
				$('#hid_parcelas').val(JSON.stringify(parcelas));
				
				if ($('#temp_div_parcelas').children('div').length == 0 ) {
					
					$('#S0_dtvencimento').val('')
				
				}else {
				
					$('#S0_dtvencimento').val($('#temp_data_' + parcelas[0].numero).val());
					
				}
				
				var hj = new Date();
				var arr = $('#S0_dtvencimento').val().split('/');
				var S0_dtvencimento = new Date(arr[2] + '-' + arr[1] + '-' + arr[0]);
				
				hj.setDate(hj.getDate() - 2);
				
				//$('#hid_prazo').val(new Date(S0_dtvencimento.getTime() - 172800000 + 10800000).toLocaleDateString());
				
				var hours = Math.abs(hj - S0_dtvencimento) / 36e5;
				
				$('#hid_prazo').val(hours);
				
			};
			
			$('.temp_valor').change(function() {
				
				onChange();
				
			});
			
			if (!isCef) {
				
				$('.temp_link_del').css('cursor','pointer');
				
				FLUIGC.calendar('.temp_data', {
				    pickDate: true,
				    pickTime: false,
				    language: 'pt-br'
				});
				
				$('.temp_data').change(function() {
					
					var dt = str2date($(this).val());
					var hj = new Date();
					
					$(this).css('border','1px solid #ccc');
					
					if (hj > dt) {
						$(this).val(null);
						$(this).css('border','1px solid red');
					}
					
					onChange();
					
				});
				
				$('.temp_link_del').click(function () {
					
					var num = parseInt($(this).attr('id'));
					
					$(this).closest('div').closest('.div_parcela').remove();
					
					$('#temp_div_parcelas').children('div').each(function () {
						
						if (parseInt($(this).find('.temp_numParcela').val()) > num) {
							
							var aux = (parseInt($(this).find('.temp_numParcela').val()) - 1).toString().split();
							
							if (aux.length == 1) {
								var new_num = '00' + aux.join('');
							} else if (aux.length == 2) {
								var new_num = '0' + aux.join('');
							} else {
								var new_num = aux.join('');
							}
							
							$(this).find('.temp_numParcela').val(new_num);
							
							$(this).find('.temp_numParcela').attr('id','temp_numParcela_' + new_num);
							$(this).find('.temp_numParcela').attr('name','temp_numParcela_' + new_num);
							
							$(this).find('.temp_data').attr('id','temp_data_' + new_num);
							$(this).find('.temp_data').attr('name','temp_data_' + new_num);
							
							$(this).find('.temp_valor').attr('id','temp_valor_' + new_num);
							$(this).find('.temp_valor').attr('name','temp_valor_' + new_num);
							
							$(this).find('.temp_link_del').attr('id',new_num);
							$(this).find('.temp_link_del').attr('name',new_num);
							
						}
						
					})
					
					onChange();
					
				});
				
				$('#temp_btn_add').click(function () {
					
					var num = (parseInt($('#temp_div_parcelas').find('.temp_numParcela').length) + 1).toString();
					
					var aux = num.split();
					if (aux.length == 1) {
						
						num = '00' + num;
						
					} else if (aux.length == 2) {
						
						num = '0' + num;
						
					}
					
					$('#temp_div_parcelas').append(`<div id="div_parcela_` + num + `" class="col-xs-12 col-md-12 div_parcela" style="margin-top:10px;">
														<div class="col-xs-12 col-md-3">
															<label for="temp_numParcela_` + num + `">Nº Parcela</label>
															<input type="text" id="temp_numParcela_` + num + `" name="temp_numParcela_` + num + `" class="form-control temp_numParcela" style="padding-right: 0px;" value="` + num + `" readonly/>
														</div>
														<div class="col-xs-12 col-md-3">
															<label for="temp_data_` + num + `">Data Vencimento</label>
															<input type="text" id="temp_data_` + num + `" name="temp_data_` + num + `" class="form-control temp_data" style="padding-right: 0px;" obrigatorio="true" onkeydown="return false"/>
														</div>
														<div class="col-xs-12 col-md-3">
															<label for="temp_valor_` + num + `">Valor</label>
															<input type="text" id="temp_valor_` + num + `" name="temp_valor_` + num + `" class="form-control temp_valor" style="padding-right: 0px;" obrigatorio="true"/>
														</div>
														<div class="col-xs-12 col-md-3" style="text-align: center;display: grid;">
															<label for="` + num + `">Remover</label>
															<a class="temp_link_del" id="` + num + `" name="` + num + `" style="cursor: pointer;">
																<i class="fluigicon fluigicon-trash icon-sm"></i>
															</a>
														</div>
													</div>`)
													
					FLUIGC.calendar('#temp_data_' + num, {
					    pickDate: true,
					    pickTime: false,
					    language: 'pt-br'
					});
					
					$('#temp_data_' + num).on('keyup',function(e){
						$(this).val($(this).val());
						e.preventDefault();
					})
					
					$('#temp_valor_' + num).mask("#.##0,00", {reverse: true});
					
					$('#temp_valor_' + num).change(function() {
						
						onChange();
						
					});
					
					$('#temp_data_' + num).change(function() {
						
						var dt = str2date($(this).val());
						var hj = new Date();
						
						$(this).css('border','1px solid #ccc');
						
						if (hj > dt) {
							$(this).val(null);
							$(this).css('border','1px solid red');
						}
						
						onChange();
						
					});
										
					$('.temp_link_del').unbind();
					$('.temp_link_del').click(function () {
						
						var num = parseInt($(this).attr('id'));
						
						$(this).closest('div').closest('.div_parcela').remove();
						
						$('#temp_div_parcelas').children('div').each(function () {
							
							if (parseInt($(this).find('.temp_numParcela').val()) > num) {
								
								var aux = (parseInt($(this).find('.temp_numParcela').val()) - 1).toString().split();
								
								if (aux.length == 1) {
									var new_num = '00' + aux.join('');
								} else if (aux.length == 2) {
									var new_num = '0' + aux.join('');
								} else {
									var new_num = aux.join('');
								}
								
								$(this).find('.temp_numParcela').val(new_num);
								
								$(this).find('.temp_numParcela').attr('id','temp_numParcela_' + new_num);
								$(this).find('.temp_numParcela').attr('name','temp_numParcela_' + new_num);
								
								$(this).find('.temp_data').attr('id','temp_data_' + new_num);
								$(this).find('.temp_data').attr('name','temp_data_' + new_num);
								
								$(this).find('.temp_valor').attr('id','temp_valor_' + new_num);
								$(this).find('.temp_valor').attr('name','temp_valor_' + new_num);
								
								$(this).find('.temp_link_del').attr('id',new_num);
								$(this).find('.temp_link_del').attr('name',new_num);
								
							}
							
						})
						
						onChange();
						
					})
					
					onChange();
					
				})
				
			}
		
		}
		
	}
}