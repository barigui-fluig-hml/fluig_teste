class atv7 extends form {
	
	constructor(){
		
		super();
		
	}
	
	_loadSection () {
		
		var that = this;
		
		$('#S0_marca').attr('readonly','');
		$('#S0_filial').attr('readonly','');
		$('#S0_CNPJ_filial').attr('readonly','');
		
		$('#S0_numnota').attr('readonly','');
		$('#S0_serie').attr('readonly','');
		$('#S0_valor').attr('readonly','');
		$('#S0_dat_emissao').attr('readonly','');
		
		$('#S0_fornecedor').attr('readonly','');
		$('#S0_tipo_CNPJCPF').attr('readonly','');
		$('#S0_CNPJCPF').attr('readonly','');
		
		if ( $('#S0_hid_SemAF').val() == 'true' ) {
			
			that.loadSemAF();
			
		} else {
			
			that.loadComAF();
			window.UTILS.configuraS0();
			
		}
		
		window.UTILS.bindNumParcelas();
		$('#S0').show();
		
		function convertePlacaParaMercosul(placa) {
			var letraPlaca = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];
			return placa.replace(placa[4], letraPlaca[placa[4]]);
		}

		function converteMercosulParaPlaca(placa) {
			var letraPlaca = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];
			return placa.replace(placa[4], letraPlaca.indexOf(placa[4]));
		}

		function retornaInformacaoUsado(url) {
			var retorno = null;
			var data = {
					companyId: 1,//WCMAPI.organizationId,
					serviceCode: 'CoreREST',
					endpoint : url,
					method: 'get',
					timeoutService: '100'
				};

			window.parent.WCMAPI.Create({
				url: '/api/public/2.0/authorize/client/invoke',
				contentType: "text/json",
				dataType: "json",
				async: false,
				data: JSON.stringify(data),
				success: function(data){
					
					if(data.message && data.message.message == 'OK') {
						var response = data.content;
						if (response.httpStatusResult == 200) {
							if(response.result) {
								
								retorno = JSON.parse(response.result);
								
							}
						}
					}
					
				}
			});
			return retorno;
		}

		

		$('#S0_placa').on('change', function() {
			var filial = that._getData('dsBARIGUI_lojasIntranet',null).find(x => x.CNPJ == $('#S0_CNPJ_filial').val().replace(/[^a-zA-Z0-9]/g,'') && x.Status == "A");
			var url = '/api/fluig/consultasDados/GetVendaVeiculoByPlaca/' + filial.LojaID + '/';
			var placa = $('#S0_placa').val().trim().toUpperCase();
			var placa_opcional = '';

			const regexPlacaNormal = /^[a-zA-Z]{3}[0-9]{4}$/;
			const regexPlacaMercosul = /^[a-zA-Z]{3}[0-9]{1}[a-zA-Z]{1}[0-9]{2}$/;

			if(regexPlacaNormal.test(placa)){
				 
				 placa_opcional = convertePlacaParaMercosul(placa).toUpperCase();

			} else if (regexPlacaMercosul.test(placa)){
				
				placa_opcional = converteMercosulParaPlaca(placa).toUpperCase();
				
			} else {
				
				$('#S0_informacaoPlaca').val('Placa inválida');
				return;

			}
			var infoSeminovo = retornaInformacaoUsado(url + placa);
			
			if (infoSeminovo != null) {
			
				if (infoSeminovo.length == 0) {
					infoSeminovo = retornaInformacaoUsado(url + placa_opcional);
					if (infoSeminovo.length == 0) {
						$('#S0_informacaoPlaca').val('Seminovo fora do Estoque');
					} else {
						$('#S0_informacaoPlaca').val('Seminovo em Estoque');
					}
				
				} else {
					$('#S0_informacaoPlaca').val('Seminovo em Estoque');
				}
			
			} else {
				that._msgWarning('Erro durante a conexão ao Banco de Dados');
				$('#S0_informacaoPlaca').val('');
			}
			
			
		});
		
	}
	
	validaAnexos () {
		var that = this;
		
		var ret = true;
		
		$('.error').removeClass('error');
		
		if ($('#S0_informaRateio').val() == 'SIM') {
			
			if (parseInt($('#S0_rateio span')[0].innerHTML) <= 0) {
				
				ret = false;
				$('#S0_rateio').addClass('error')
				that._msgDanger("O Anexo Rateio é obrigatório!!");
				
			}
			
		}
		
		if (parseInt($('#S0_nota span')[0].innerHTML) <= 0) {
			
			ret = false;
			$('#S0_nota').addClass('error')
			that._msgDanger("O Anexo NotaFiscal é obrigatório!!");
			
		}
		
		return ret;
		
	}
	
	loadSemAF () {
		
		var that = this;
		
		$('.S0_forComAF').hide();
		
		if ($('#S0_hid_isNFe').val() != undefined && $('#S0_hid_isNFe').val() != null && $('#S0_hid_isNFe').val().toUpperCase() != 'FALSE') {
			
			$('#S0_forNFe').show();
			
			if ($('#S0_hid_link_xml').val() == '') {
				
				$('#S0_xml').closest('div').hide();
				$('#S0_hid_link_xml').val('');
				
			} else {
				
				$('#S0_xml').attr('chave',$('#S0_link_xml').val());
				//$('#S0_xml').attr('download','xml_nota_' + $('#S0_numnota').val() + '.xml');
				
			}
			
			$('#S0_xml').click(function(){
				
				var data = {
					companyId: 1,
					serviceCode: 'CapturaNFE',
					endpoint : '/api/document/?key='+$(this).attr('chave')+'&format=xml&download=true&encode=false',
					method: 'get',
					timeoutService: '100'
				};
				
				window.parent.WCMAPI.Create({
		    	    url: '/api/public/2.0/authorize/client/invoke',
		    	    contentType: "text/json",
		    	    dataType: "json",
		    	    async: false,
		    	    data: JSON.stringify(data),
		    	    success: function(data){
		    	    	
		    	    	var result = data.content.result.split('"').join('')
		    	    	var base64 = btoa(result)
		    	    	var blob = window.ANEXOS.base64toBlob(base64,"text/xml")
		    	    	const url = window.URL.createObjectURL(blob);
		    	        const a = document.createElement('a');
		    	        a.style.display = 'none';
		    	        a.href = url;
		    	        a.download = 'xml-' + $('#S0_numnota').val() + '.xml';
		    	        document.body.appendChild(a);
		    	        a.click();
		    	        window.URL.revokeObjectURL(url);
		    	    	
		    	    }
				})
				
			})
			
		}
		
		$('#S0_Obs').val('');
		
		that.bindAtv7();
		
	}
	
	loadComAF () {
		
		var that = this;
		
		$('.S0_forComAF').show();
		
		$('#S0_xml').attr('href',$('#S0_hid_link_xml').val());
		
		$('#S0_tipo').attr('readonly','');
		$('#S0_complemento').attr('readonly','');
		
		$('#S0_contagerencial').attr('readonly','');
		$('#S0_departamento').attr('readonly','');
		
		$('#S0_condicaoPagamento').attr('readonly','');
		
	}
	
	bindAtv7 () {
		
		window.FORMULARIO._startLoading();
		
		var that = this;
		
		var hid_infoForm = JSON.parse($('#hid_infoForm').val());
		
		if (hid_infoForm.modeView != 'VIEW') {
		
			if ($('#S1_aprovacao').val() == "" || $('#S1_aprovacao').val().toUpperCase() == "NAO") {
				
				$('#S0_valor').removeAttr('readonly');
				$('#S0_dat_emissao').removeAttr('readonly');
				
				$('#S0_nraf').removeAttr('readonly');
				$('#S0_numOS').removeAttr('readonly');
				$('#S0_placa').removeAttr('readonly');
				
				$('#S0_dat_emissao').on('keydown', function(e){
					if (e.which == 8)
						$(this).val('');
					else
						$(this).val($(this).val());
					
					e.preventDefault();
				})
				
				FLUIGC.calendar('#S0_dat_emissao', {
				    pickDate: true,
				    pickTime: false
				});
				
				var S0_tipo = $('#S0_tipo').val();
				that.bindTipo();
				$('#S0_tipo').val(S0_tipo);
				
				if ($('#S0_complemento').val() != "") {
					
					var S0_complemento = $('#S0_complemento').val();
					that.bindComplemento();
					$('#S0_complemento').val(S0_complemento);
					
					if ($('#S0_complemento').val().toUpperCase() != 'Nota Fiscal sem Pagamento'.toUpperCase()) {
						
						var S0_condicaoPagamento = $('#S0_condicaoPagamento').val();
						that.bindCondicaoPagamento();
						$('#S0_condicaoPagamento').val(S0_condicaoPagamento);
					
					}
					
					var S0_contagerencial = $('#S0_contagerencial').val();
					that.bindContagerencial();
					$('#S0_contagerencial').val(S0_contagerencial);
					
					var S0_departamento = $('#S0_departamento').val();
					that.bindDepartamento();
					$('#S0_departamento').val(S0_departamento);
					
					if (($('#S0_complemento').val().toUpperCase() != 'Marketing'.toUpperCase() || $('#S0_tipo').val().toUpperCase() != 'Custo'.toUpperCase())
							&& S0_departamento != undefined && S0_departamento != null && S0_departamento != "") {
						
						var S0_aprovador_matricula = $('#S0_aprovador_matricula').val();
						that.bindAprovador();
						$('#S0_temp_aprovador').val(S0_aprovador_matricula.toUpperCase());
						
					}
					
					var S0_deposito_banco = $('#S0_deposito_banco').val();
					that.bindDeposito_Banco();
					$('#S0_deposito_banco').val(S0_deposito_banco.toUpperCase());
					
				}
			
			} else {
				
				if ($('#S0_hid_isNFe').val() != undefined && $('#S0_hid_isNFe').val() != null && $('#S0_hid_isNFe').val().toUpperCase() != 'FALSE') {
					
					$('#S0_forNFe').show();
					
				}
				
				$('#S0_numParcelas').attr('readonly','');
				
				var div = $('#S0_tipo').closest('div');
				var S0_tipo = $('#S0_tipo').val();
				$('#S0_tipo').remove();
				$(div).append(`<input type="text" id="S0_tipo" name="S0_tipo" class="form-control" obrigatorio="true"/>`);
				$('#S0_tipo').val(S0_tipo);
				
				$('#S0_tipo').attr('readonly','');
				$('#S0_complemento').attr('readonly','');
				$('#S0_camposSelecao').attr('readonly','');
				
				$('#S0_dtrecebimento').attr('readonly','');
				
				$('#S0_contagerencial').attr('readonly','');
				$('#S0_departamento').attr('readonly','');
				
				$('#S0_aprovador').attr('readonly','');
				
				$('#S0_numOS').attr('readonly','');
				$('#S0_numAF').attr('readonly','');
				
				$('#S0_placa').attr('readonly','');
				
				var S0_condicaoPagamento = $('#S0_condicaoPagamento').val();
				that.bindCondicaoPagamento();
				$('#S0_condicaoPagamento').val(S0_condicaoPagamento.toUpperCase());
				
				var S0_deposito_banco = $('#S0_deposito_banco').val();
				that.bindDeposito_Banco();
				$('#S0_deposito_banco').val(S0_deposito_banco.toUpperCase());
				
			}
			
		}
		
		window.UTILS.configuraS0();
		window.FORMULARIO._stopLoading();
		
	}
	
	bindTipo () {
		
		var that = this;
		
		$('#S0_tipo').html(`<option value="">==SELECIONE==</option>
				<option value="CUSTO">CUSTO</option>
				<option value="DESPESA">DESPESA</option>`);
		
		$('#S0_tipo').change(function() {
			
			window.FORMULARIO._startLoading();
			
			that.changeTipo();
			
			window.FORMULARIO._stopLoading();
			
		})
		
	}
	
	bindComplemento () {
		
		var that = this;
		
		var div = $('#S0_complemento').closest('div');
		$('#S0_complemento').remove();
		
		$(div).hide();
		
		if ($('#S0_tipo').val().toUpperCase() == 'Custo'.toUpperCase()) {
			
			$(div).append(`<select id="S0_complemento" name="S0_complemento" class="form-control" obrigatorio="true">
								<option value="">==SELECIONE==</option>
								<option value="OFICINA">OFICINA</option>
								<option value="SEMINOVOS">SEMINOVOS</option>
							</select>`);
			
		} else if ($('#S0_tipo').val().toUpperCase() == 'Despesa'.toUpperCase()) {

			var constraints = [DatasetFactory.createConstraint('workflowColleagueRolePK.colleagueId',that.responsavel.id,that.responsavel.id,ConstraintType.MUST)];
			var roles = that._getData('workflowColleagueRole',constraints);
			
			if (roles.find(x => x['workflowColleagueRolePK.roleId'] == 'CEF_tipo_NF_extraordinaria') != undefined) {
				
				$(div).append(`<select id="S0_complemento" name="S0_complemento" class="form-control" obrigatorio="true">
						<option value="">==SELECIONE==</option>
						<option value="EXTRAORDINÁRIA">EXTRAORDINÁRIA</option>
						<option value="MARKETING">MARKETING</option>
						<option value="NOTA FISCAL SEM PAGAMENTO">NOTA FISCAL SEM PAGAMENTO</option>
						<option value="NOVO VAREJO">NOVO VAREJO</option>
						<option value="TECNOLOGIA DA INFORMAÇÃO">TECNOLOGIA DA INFORMAÇÃO</option>
					</select>`);
				
			} else {
				
				$(div).append(`<select id="S0_complemento" name="S0_complemento" class="form-control" obrigatorio="true">
						<option value="">==SELECIONE==</option>
						<option value="MARKETING">MARKETING</option>
						<option value="NOTA FISCAL SEM PAGAMENTO">NOTA FISCAL SEM PAGAMENTO</option>
						<option value="NOVO VAREJO">NOVO VAREJO</option>
						<option value="TECNOLOGIA DA INFORMAÇÃO">TECNOLOGIA DA INFORMAÇÃO</option>
					</select>`);
				
			}
			
		} else {
			
			$(div).append(`<select id="S0_complemento" name="S0_complemento" class="form-control" obrigatorio="true">
								<option value="">==SELECIONE==</option>
							</select>`);
			
		}
		
		$(div).show();
		
		$('#S0_complemento').change(function () {
			
			that.changeComplemento();
			
		})
		
	}
	
	bindDepartamento () {
		
		var that = this;
		
		var div = $('#S0_departamento').closest('div');
		$('#S0_departamento').remove();
		
		var linhas = [];
		var constraint = [];
		
		if ($('#S0_complemento').val().toUpperCase() == 'Oficina'.toUpperCase()) {
			
			constraint.push(DatasetFactory.createConstraint('nome_departamento','41 OFICINA P.VENDAS(LOJA)','41 OFICINA P.VENDAS(LOJA)',ConstraintType.SHOULD));
			constraint.push(DatasetFactory.createConstraint('nome_departamento','49 FUN/PINT-P.VENDAS(LOJA)','49 FUN/PINT-P.VENDAS(LOJA)',ConstraintType.SHOULD));
			
		} else if ($('#S0_complemento').val().toUpperCase() == 'Seminovos'.toUpperCase()) {
			
			constraint.push(DatasetFactory.createConstraint('nome_departamento','10 S.NOVOS-VENDAS(LOJA)','10 S.NOVOS-VENDAS(LOJA)',ConstraintType.SHOULD));
			constraint.push(DatasetFactory.createConstraint('nome_departamento','13 REVISÃO CENTRAL S.NOVOS','13 REVISÃO CENTRAL S.NOVOS',ConstraintType.SHOULD));
			
		} else if ($('#S0_complemento').val().toUpperCase() == 'Novo Varejo'.toUpperCase()) {

			constraint.push(DatasetFactory.createConstraint('nome_departamento','30 PEÇAS-P.VENDAS','30 PEÇAS-P.VENDAS',ConstraintType.SHOULD));
			constraint.push(DatasetFactory.createConstraint('nome_departamento','77 DESENVOLVIMENTO/TECNOLOGIA','77 DESENVOLVIMENTO/TECNOLOGIA',ConstraintType.SHOULD));
			constraint.push(DatasetFactory.createConstraint('nome_departamento','NOVO VAREJO - GERAL','NOVO VAREJO - GERAL',ConstraintType.SHOULD));
			constraint.push(DatasetFactory.createConstraint('nome_departamento','NOVO VAREJO - PLATAFORMA COMMERCE','NOVO VAREJO - PLATAFORMA COMMERCE',ConstraintType.SHOULD));
			constraint.push(DatasetFactory.createConstraint('nome_departamento','NOVO VAREJO - PLATAFORMA HUB','NOVO VAREJO - PLATAFORMA HUB',ConstraintType.SHOULD));
			constraint.push(DatasetFactory.createConstraint('nome_departamento','NOVO VAREJO - PLATAFORMA ICARROS','NOVO VAREJO - PLATAFORMA ICARROS',ConstraintType.SHOULD));

		}	
		
		var filial = that._getData('dsBARIGUI_lojasIntranet',null).find(x => x.Nome == $('#S0_filial').val());
		
		var constraints = [];
		constraints.push(DatasetFactory.createConstraint('cnpj_filial',filial.CNPJ,filial.CNPJ,ConstraintType.MUST));
		
		if ($('#S0_complemento').val().toUpperCase() == 'Novo Varejo'.toUpperCase()) {
			var departamentos = that._getData('dsBARIGUI_aprovadoresNovoVarejo',constraint);
		}
		
		else if ($('#S0_complemento').val().toUpperCase() != 'Novo Varejo'.toUpperCase()) {
			var departamentos = that._getData('dsTELEMETRIX_view_Fluig_Aprovadores',constraint);
		}

		
		departamentos.sort(function (a, b) {
			
			if (a.nome_departamento < b.nome_departamento) {
				return -1 
			}
			
			if (b.nome_departamento < a.nome_departamento) {
				return 1
			}
			
			return 0;
			
		}).forEach(function (departamento) { 
			if (linhas.length == 0 || linhas.map(x => x.nome_departamento).indexOf(departamento.nome_departamento) == -1) 
			{ 
				linhas.push( { nome_departamento: departamento.nome_departamento.toUpperCase() } )
			} 
		});
		
		var template = `<select id="S0_departamento" name="S0_departamento" class="form-control" obrigatorio="true">
							<option value="">==SELECIONE==</option>
							{{#linhas}}
								<option value="{{nome_departamento}}">{{nome_departamento}}</option>
							{{/linhas}}
						</select>`

		var view = { linhas }
		
		$(div).append(Mustache.render(template,view));
		$(div).show();
		
		$('#S0_departamento').change(function () {
			
			that.changeDepartamento();
			
		})
		
	}
	
	bindContagerencial () {
		
		var that = this;
		
		var div = $('#S0_contagerencial').closest('div');
		var val = $('#S0_contagerencial').val()
		$('#S0_contagerencial').remove();
		
		var filial = that._getData('dsBARIGUI_lojasIntranet',null).find(x => x.CNPJ == $('#S0_CNPJ_filial').val().replace(/[^a-zA-Z0-9]/g,'') && x.Status == 'A');
		
		var contas = that._getData('dsBARIGUI_contaGerencialPorTipoNF',[]).filter(x => x.ErpID == filial.ErpID && x.tipoNF.toUpperCase() == $('#S0_complemento').val().toUpperCase())
									.map(function (v) { return {value: v.Conta.toUpperCase()} });
		
		
		function removeDuplicates(originalArray, prop) {
            var newArray = [];
            var lookupObject  = {};

            for(var i in originalArray) {
               lookupObject[originalArray[i][prop]] = originalArray[i];
            }

            for(i in lookupObject) {
                newArray.push(lookupObject[i]);
            }
             return newArray;
        }

        contas = removeDuplicates(contas, "value");
        
		contas.sort(function (a, b) {
			
			if (a.value < b.value) {
				return -1 
			}
			
			if (b.value < a.value) {
				return 1
			}
			
			return 0;
			
		})
		
		var view = { linhas: contas };
		
		var template = `<select id="S0_contagerencial" name="S0_contagerencial" class="form-control" obrigatorio="true">
							<option value="">==SELECIONE==</option>
							{{#linhas}}
								<option value="{{value}}">{{value}}</option>
							{{/linhas}}
						</select>`;
		
		$(div).append(Mustache.render(template,view));
		$('#S0_contagerencial').val(val)
		
	}
	
	bindCondicaoPagamento () {
		
		var that = this;
		
		var div = $('#S0_condicaoPagamento').closest('div');
		$('#S0_condicaoPagamento').remove();
		
		var ds_condPagamento = [
			{CODIGO:'1',NOME:'Acerto pelo Caixa',STATUS:'A'}
			,{CODIGO:'2',NOME:'Adiantamento',STATUS:'A'}
			,{CODIGO:'3',NOME:'Boleto',STATUS:'A'}
			,{CODIGO:'4',NOME:'Débito Automático',STATUS:'A'}
			,{CODIGO:'5',NOME:'Depósito',STATUS:'A'}
			,{CODIGO:'6',NOME:'Encontro de Contas',STATUS:'A'}
			,{CODIGO:'7',NOME:'Pix',STATUS:'A'}
	    ]
		
		var condPagamento = ds_condPagamento.map(function (v) { return {value: v.NOME.toUpperCase()} });
		
		var view = { linhas: condPagamento };
		
		var template = `<select id="S0_condicaoPagamento" name="S0_condicaoPagamento" class="form-control" obrigatorio="true">
							<option value="">==SELECIONE==</option>
							{{#linhas}}
								<option value="{{value}}">{{value}}</option>
							{{/linhas}}
						</select>`;
		
		$(div).append(Mustache.render(template,view));
		$(div).show();
		
		$('#S0_condicaoPagamento').change(function () {
			
			that.changeCondicaoPagamento();
			
		})
		
	}
	
	bindDeposito_Banco () {
		
		var that = this;
		
		var bancos = that._getData('dsCadastroBancos',[]);
		
		var div = $('#S0_deposito_banco').closest('div');
		$('#S0_deposito_banco').remove();
		
		bancos.sort(function(a,b){
			
			if ( a.Banco_Sigla > b.Banco_Sigla) return 1
			if ( a.Banco_Sigla < b.Banco_Sigla) return -1
			return 0
			
		})
		
		var view = { linhas: bancos.map(function (x) { return { id: x['Banco_Sigla'].toUpperCase() + " - " + x['Banco_Descricao'].toUpperCase(), nome: x['Banco_Sigla'].toUpperCase() + " - " + x['Banco_Descricao'].toUpperCase() } }) }
		
		var template = `<select id="S0_deposito_banco" name="S0_deposito_banco" class="form-control">
							<option value="">==SELECIONE==</option>
							{{#linhas}}
								<option value="{{id}}">{{nome}}</option>
							{{/linhas}}
						</select>`;
		
		$(div).append(Mustache.render(template,view));
		
	}
	
	bindAprovador () {
		
		var that = this;
		
		$('#S0_temp_forTempAprovador').html('');
		$('#S0_aprovador').closest('div').hide();
		
		if ($('#S0_complemento').val().toUpperCase() == 'Marketing'.toUpperCase()) {
				
				var constraints_group = [];
				constraints_group.push(DatasetFactory.createConstraint('colleagueGroupPK.groupId','aprov_CEF_mktg','aprov_CEF_mktg',ConstraintType.MUST));
				var group = that._getData('colleagueGroup',constraints_group);
				
				var constraints = [];
				
				for (var i in group) {
					
					var user = group[i];
					
					constraints.push(DatasetFactory.createConstraint('colleaguePK.colleagueId',user['colleagueGroupPK.colleagueId'],user['colleagueGroupPK.colleagueId'],ConstraintType.SHOULD));
					
				}
			
		}else if ($('#S0_complemento').val().toUpperCase() == 'Novo Varejo'.toUpperCase()) {
					
					var constraints_group = [];
					constraints_group.push(DatasetFactory.createConstraint('colleagueGroupPK.groupId','aprov_CEF_NovoVarejo','aprov_CEF_NovoVarejo',ConstraintType.MUST));
					var group = that._getData('colleagueGroup',constraints_group);
					
					var constraints = [];
					
					for (var i in group) {
						
						var user = group[i];
						
						constraints.push(DatasetFactory.createConstraint('colleaguePK.colleagueId',user['colleagueGroupPK.colleagueId'],user['colleagueGroupPK.colleagueId'],ConstraintType.SHOULD));
						
					}
		} else {
			
			var constraints = [];
			constraints.push(DatasetFactory.createConstraint('nome_departamento',$('#S0_departamento').val(),$('#S0_departamento').val(),ConstraintType.MUST));
			
			var filial = that._getData('dsBARIGUI_lojasIntranet',null).find(x => x.Nome == $('#S0_filial').val());
			
			var constraints = [];
			constraints.push(DatasetFactory.createConstraint('cnpj_filial',filial.CNPJ,filial.CNPJ,ConstraintType.MUST));
			
			var aprovadores = that._getData('dsTELEMETRIX_view_Fluig_Aprovadores',constraints);
			
			constraints = [];//[DatasetFactory.createConstraint('login','','',ConstraintType.SHOULD)];
			aprovadores.forEach(function (ap) { constraints.push(DatasetFactory.createConstraint('login',ap.login_usuario,ap.login_usuario,ConstraintType.SHOULD)); });
			
		}
		
		var users = that._getData('colleague',constraints);
		
		users.sort(function(a,b){
			
			if (a.colleagueName > b.colleagueName) return 1
			if (a.colleagueName < b.colleagueName) return -1
			return 0
			
		})
		
		var view = { linhas: users.map(function (x) { return { id: x['colleaguePK.colleagueId'], nome: x.colleagueName.toUpperCase() } }) }
		
		var template = `<label for="S0_temp_aprovador">Aprovador</label>
						<select id="S0_temp_aprovador" name="S0_temp_aprovador" class="form-control">
							<option value="">==SELECIONE==</option>
							{{#linhas}}
								<option value="{{id}}">{{nome}}</option>
							{{/linhas}}
						</select>`;
		
		$('#S0_temp_forTempAprovador').append(Mustache.render(template,view));
		$('#S0_temp_forTempAprovador').show();
		
		$('#S0_temp_aprovador').change(function() {
			
			that.changeAprovador();
			
		})
		
	}
	
	changeDepartamento () {
		
		$('#S0_loginaprovador').val('');
		$('#S0_emailaprovador').val('');
		$('#S0_aprovador').val('');
		$('#S0_aprovador_matricula').val('');
		
		$('#S1_aprovacao').val('');
		$('#S2_aprovacao').val('');
		$('#S3_aprovacao').val('');
		
		this.bindAprovador();
		
	}
	
	changeComplemento () {
		
		window.FORMULARIO._startLoading();
		
		$('#S1_aprovacao').val('');
		$('#S2_aprovacao').val('');
		$('#S3_aprovacao').val('');
		
		var that = this;
		
		var func = async function () {
			
			if ($('#S0_condicaoPagamento:visible').length > 0) {
				
				that.bindCondicaoPagamento();
			
			}
			
			that.bindContagerencial();
			that.bindDepartamento();
			
			await window.UTILS.configuraS0();
			window.FORMULARIO._stopLoading();
			
		}
		
		func();
		
	}
	
	changeCondicaoPagamento () {
		
		var that = this;
		
		$('.S0-dadosDeposito').hide();
		
		if ($('#S0_condicaoPagamento').val().toUpperCase() == 'DEPÓSITO') {
			
			$('.S0-dadosDeposito').show();
			
			that.bindDeposito_Banco();
			
			$('#S0_deposito_CPF_CNPJ').change(function () {
				
				$('#S0_deposito_CPF_CNPJ').removeClass('error');
				
				if ($('#S0_deposito_tipo').val().toUpperCase() == 'CPF') {
					
					if (!TestaCPF($('#S0_deposito_CPF_CNPJ').val())) {
						
						$('#S0_deposito_CPF_CNPJ').addClass('error');
						$('#S0_deposito_CPF_CNPJ').val('');
						$('#S0_deposito_nome').val('');
						
					} else {
						
						var constraint = [];
						
						var val = $('#S0_deposito_CPF_CNPJ').val().split('.').join('').split('-').join('');
						
						var f = that._getData('dsBARIGUI_lojasIntranet',[]);
						
						var cod = f.find(x => x.Nome == $('#S0_filial').val());
						
						constraint.push(DatasetFactory.createConstraint('Pessoa_DocIdentificador',val,val,ConstraintType.MUST));
						constraint.push(DatasetFactory.createConstraint('lojaId',cod.LojaID,cod.LojaID,ConstraintType.MUST));
						var pessoa = that._getData('dsBARIGUI_fornecedoresAPI',constraint);
						
						if (!pessoa || pessoa == undefined || pessoa == null || pessoa == '' || pessoa.length == 0) {
							
							$('#S0_deposito_CPF_CNPJ').addClass('error');
							$('#S0_deposito_CPF_CNPJ').val('');
							$('#S0_deposito_nome').val('');
							
						} else {
							
							$('#S0_deposito_nome').val(pessoa[0].Pessoa_Nome.toUpperCase());
							
						}
						
					}
					
				} else if ($('#S0_deposito_tipo').val().toUpperCase() == 'CNPJ') {
					
					if (!TestaCNPJ($('#S0_deposito_CPF_CNPJ').val())) {
						
						$('#S0_deposito_CPF_CNPJ').addClass('error');
						$('#S0_deposito_CPF_CNPJ').val('');
						$('#S0_deposito_nome').val('');
						
					} else {
						
						var constraint = [];
						
						var val = $('#S0_deposito_CPF_CNPJ').val().split('.').join('').split('-').join('').split('/').join('');
						
						var f = that._getData('dsBARIGUI_lojasIntranet',[]);
						
						var cod = f.find(x => x.Nome == $('#S0_filial').val());
						
						constraint.push(DatasetFactory.createConstraint('Pessoa_DocIdentificador',val,val,ConstraintType.MUST));
						constraint.push(DatasetFactory.createConstraint('lojaId',cod.LojaID,cod.LojaID,ConstraintType.MUST));
						var pessoa = that._getData('dsBARIGUI_fornecedoresAPI',constraint);
						
						if (!pessoa || pessoa == undefined || pessoa == null || pessoa == '' || pessoa.length == 0) {
							
							$('#S0_deposito_CPF_CNPJ').addClass('error');
							$('#S0_deposito_CPF_CNPJ').val('');
							$('#S0_deposito_nome').val('');
							
						} else {
							
							$('#S0_deposito_nome').val(pessoa[0].Pessoa_Nome.toUpperCase());
							
						}
						
					}
					
				} else {
					$('#S0_deposito_nome').val('');
					$('#S0_deposito_CPF_CNPJ').val('');
					$('#S0_deposito_CPF_CNPJ').attr('disabled','disabled');
				}
				
			})
			
			$('#S0_deposito_tipo').unbind();
			$('#S0_deposito_tipo').change(function () {
				
				if ($(this).val() == 'CPF') {
					$('#S0_deposito_CPF_CNPJ').mask('000.000.000-00');
				} else if ($(this).val() == 'CNPJ') {
					$('#S0_deposito_CPF_CNPJ').mask('00.000.000/0000-00');
				} else {
					$('#S0_deposito_CPF_CNPJ').attr('disabled','disabled');
				}
				
			});
			
			$('#S0_deposito_CPF_CNPJ').mask('00.000.000/0000-00');
			
		}  else if ($('#S0_condicaoPagamento').val().toUpperCase() == 'PIX') {
			
			$('.S0-dadosPix').show();
			
			var div = $('#S0_pix_tipo_chave').closest('div');
			var val = $('#S0_pix_tipo_chave').val();
			$('#S0_pix_tipo_chave').remove();
			
			var view = { linhas: [ 
				{ value: 'Chave Aleatoria'.toUpperCase() },
				{ value: 'e-mail'.toUpperCase() },
				{ value: 'CNPJ'.toUpperCase() },
				{ value: 'CPF'.toUpperCase() },
				{ value: 'Telefone'.toUpperCase() }
			] };
			
			var template = `<select id="S0_pix_tipo_chave" name="S0_pix_tipo_chave" class="form-control" obrigatorio="true">
								<option value="">==SELECIONE==</option>
								{{#linhas}}
									<option value="{{value}}">{{value}}</option>
								{{/linhas}}
							</select>`;
			
			$(div).append(Mustache.render(template,view));
			$('#S0_pix_tipo_chave').val(val);
			
			$('#S0_pix_tipo_chave').change(function () {
				
				var v = $(this).val()
				
				$('#S0_pix_chave').val('')
				
				if (v.toUpperCase() == 'CPF') {
					
					$('#S0_pix_chave').mask("000.000.000-00", {reverse: true});
					
				} else if (v.toUpperCase() == 'CNPJ') {
					
					$('#S0_pix_chave').mask("00.000.000/0000-00", {reverse: true});
					
				} else if (v.toUpperCase() == 'Telefone'.toUpperCase()) {
					
					$('#S0_pix_chave').mask("000000000000");
					
				} else if (v.toUpperCase() == 'e-mail'.toUpperCase()) {
					
					$('#S0_pix_chave').unmask();
					
				} else if (v.toUpperCase() == 'Chave Aleatoria'.toUpperCase()) {
					
					$('#S0_pix_chave').unmask();
					
				}
				
			})
			
			$('#S0_pix_tipo_chave').trigger('change');
			
		}
		
	}
	
	changeAprovador () {
		
		$('#S1_aprovacao').val('');
		$('#S2_aprovacao').val('');
		$('#S3_aprovacao').val('');
		
		var that = this;
		
		var login = $('#S0_temp_aprovador').val();
		var nome = $('#S0_temp_aprovador').find(':selected').html();
		
		$('#S0_aprovador').val(nome);
		$('#S0_aprovador_matricula').val(login);
		
		var constraints = [];
		constraints.push(DatasetFactory.createConstraint('colleaguePK.colleagueId',login,login,ConstraintType.MUST));
		var user = that._getData('colleague',constraints);
		
		$('#S0_loginaprovador').val(user[0].login.toUpperCase());
		$('#S0_emailaprovador').val(user[0].mail.toUpperCase());
		
	}
	
	changeTipo () {
		
		$('#S1_aprovacao').val('');
		$('#S2_aprovacao').val('');
		$('#S3_aprovacao').val('');
		
		this.bindComplemento();
		window.UTILS.configuraS0();
		
	}
	
	_sectionValidate () {
		
		var that = this;
		
		/*if ($('#S1_aprovacao').val() == 'nao' || $('#S2_aprovacao').val() == 'nao' ||
				$('#S3_aprovacao').val() == 'nao' || $('#S4_aprovacao').val() == 'pendenciaInicio') {
			
			if ($('#S0_Obs').val() == undefined || $('#S0_Obs').val() == null || $('#S0_Obs').val() == "") {
				
				$('#S0_Obs').addClass('error');
				
				that._msgDanger('O Campo observação é obrigatório');
				return false;
				
			}
			
		}*/
		
		if ($('#S0_condicaoPagamento').val().toUpperCase() == 'DEPÓSITO') {
			
			$('.S0-dadosDeposito').find('input').each(function () {
				
				if ($(this).val() == undefined || $(this).val() == null || $(this).val() == '') {
					
					$(this).addClass('error');
					
				}
				
			})
			
			$('.S0-dadosDeposito').find('select').each(function () {
				
				if ($(this).val() == undefined || $(this).val() == null || $(this).val() == '') {
					
					$(this).addClass('error');
					
				}
				
			})
			
		}
		
		if ($('.error').length > 0) {
			
			this._msgDanger('Preencha os campos obrigatórios em destaque');
			return false;
			
		}
		
		/*if ($('#S0_tpdespesa_complemento').val() == 'Oficina') {
			
			if ((!$('#S0_numOS').val() || $('#S0_numOS').val() == null || $('#S0_numOS').val() == '') 
				&& (!$('#S0_numAF').val() || $('#S0_numAF').val() == null || $('#S0_numAF').val() == '')) {
				
				this._msgDanger('Informe o Numero da O.S. ou AF para continuar');
				$('#S0_numOS').addClass('error');
				$('#S0_numAF').addClass('error');
				return false;
				
			}
			
		}*/
		
		if ($('#S0_tipo').val().toUpperCase() != 'Despesa'.toUpperCase() 
			&& ($('#S0_aprovador').val() == undefined || $('#S0_aprovador').val() == null || $('#S0_aprovador').val() == "")) {
			
			$('#S0_aprovador').addClass('error');
			
			that._msgDanger('É obrigatório informar um aprovador');
			return false;
			
		}
		
		if ($('#S0_complemento').val().toUpperCase() != 'Nota Fiscal sem Pagamento'.toUpperCase()) {
			if ((!$('#S0_numParcelas').val() || $('#S0_numParcelas').val() == undefined || $('#S0_numParcelas').val() == null || $('#S0_numParcelas').val() == '' || $('#S0_numParcelas').val() == '0')
				|| (!$('#hid_parcelas').val() || $('#hid_parcelas').val() == undefined || $('#hid_parcelas').val() == null || $('#hid_parcelas').val() == '')) 
			{
				
				this._msgDanger('Informe os dados de pagamento da NF');
				$('#S0_numParcelas').addClass('error');
				$('#S0_link_verPacelas').css('color','red');
				return false;
				
			} else {
				
				var list = JSON.parse($('#hid_parcelas').val())
				
				if (list != undefined && list != null && list != {} && list != []) {
					
					var vl_total = 0;
					
					for (var i in list) {
						
						var obj = list[i];
						
						if (obj.numero == undefined || obj.numero == null || obj.numero == ""
							|| obj.data_vencimento == undefined || obj.data_vencimento == null || obj.data_vencimento == ""
							|| obj.valor == undefined || obj.valor == null || obj.valor == "") 
						{
							
							this._msgDanger('Dados da parcela "' 
									+ ((obj.numero == undefined || obj.numero == null || obj.numero == "")? "XXX" : obj.numero) 
									+ '" Incompletos ou invalidos');
							$('#S0_numParcelas').addClass('error');
							$('#S0_link_verPacelas').css('color','red');
							return false;
							
						} else {
							
							vl_total += parseFloat(obj.valor.split('.').join('').replace(',','.'));
							
						}
						
					}
					
					var S0_valor = parseFloat($('#S0_valor').val().replace('R$','').split('.').join('').replace(',','.'));
					if (vl_total != S0_valor) {
						
						this._msgDanger('O valor das parcelas não está correto!! verificar valor total da nota');
						$('#S0_numParcelas').addClass('error');
						$('#S0_link_verPacelas').css('color','red');
						return false;
						
					}
					
				} else {
					
					this._msgDanger('Informe os dados de pagamento da NF');
					$('#S0_numParcelas').addClass('error');
					$('#S0_link_verPacelas').css('color','red');
					return false;
					
				}
				
			}
		}
		
		
		if (!that.validaAnexos()) {
			return false;
		}
		
		window.parent.$('#workflowActions').hide();
		
		window.parent.$('button').each(function () {
			
			var attr = $(this).attr('data-send');
			
			if (typeof attr !== typeof undefined && attr !== false) {
				  
				$(this).closest('div').find('button').attr('disabled','disabled');
			
			}
			
		})
		
		return true;
		
	}
	
}