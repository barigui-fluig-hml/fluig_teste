//Versão: 02/08/2021
//Origem: BARIGUI_COMPRAS
//Editor: André Luiz Sanches Ferreira
class anexos { 
	
	constructor (json) {
		
		if (json && json != undefined && json != null && json != "") {
			
			var obj = JSON.parse(json);
			
			if (!obj.processCod || obj.processCod == undefined || obj.processCod == null || obj.processCod == "") {
				
				window.parent.$('#workflowActions').hide();
				
				window.parent.$('button').each(function () {
					
					var attr = $(this).attr('data-send');
					
					if (typeof attr !== typeof undefined && attr !== false) {
						  
						$(this).closest('div').find('button').attr('disabled','disabled');
					
					}
					
				})
				
				_msgDanger("Erro ao iniciar o controle de anexos, codigo de processo invalido");
				throw "Erro ao iniciar o controle de anexos, codigo de processo invalido";
				
			}
			
			this.processCod = obj.processCod; // Codigo do Processo
			this.processoTipo_Descricao = obj.processoTipo_Descricao; // Processo Descrição
			this.processoTipo_PastaProd = obj.processoTipo_PastaProd; // Pasta do Processo (PROD)
			this.processoTipo_PastaHomolog = obj.processoTipo_PastaHomolog; // Pasta do Processo (HOMOLOG)
			
			this.processoTipo_Pasta = (obj.isProd ? (obj.processoTipo_PastaProd) : (obj.processoTipo_PastaHomolog));
			
			this.usuarioCriador = (obj.isProd ? ("infra") : ("suporte"));
			
			this.solicitacaoFolder = obj.solicitacaoFolder;  // Pasta da solicitação atual
			this.tipos = (obj.tipos == undefined || obj.tipos == null)? [] : obj.tipos; // Listas de tipos de anexo da solicitação
			
			this.uploads = []; //{link, prefix}
			
			this.processFolder = $('#hid_folder').val();
			
		} else {
			
			window.parent.$('#workflowActions').hide();
			
			window.parent.$('button').each(function () {
				
				var attr = $(this).attr('data-send');
				
				if (typeof attr !== typeof undefined && attr !== false) {
					  
					$(this).closest('div').find('button').attr('disabled','disabled');
				
				}
				
			})
			
			_msgDanger("Erro ao iniciar o controle de anexos!");
			throw "Erro ao iniciar o controle de anexos!";
			
		}
		
	}
	
	formataListaPorCategoria (list) {
		
		var groups = [];
		
		for (var i in list) {
			
			if (groups.length > 0) {
				
				if (groups.find(x => x.key.key == list[i].categoriaDoc) == undefined)
					groups.push({key: { key: list[i].categoriaDoc, value: list[i].categoriaDoc_Descricao },
								value: [{ key: list[i].documentoTipo
										,value: { descricao: list[i].documentoTipo_Descricao
													,obrigatorio: list[i].documentoTipo_obrigatorio
													,solicitado: false
													,validado: false} }] });
				else 
					groups.find(x => x.key.key == list[i].categoriaDoc).value.push({ key: list[i].documentoTipo
																					,value: {descricao: list[i].documentoTipo_Descricao
																							,obrigatorio: list[i].documentoTipo_obrigatorio
																							,solicitado: false
																							,validado: false} });
				
			} else {
				
				groups.push({key: { key: list[i].categoriaDoc, value: list[i].categoriaDoc_Descricao },
									value: [{ key: list[i].documentoTipo, 
												value: {descricao: list[i].documentoTipo_Descricao
														,obrigatorio: list[i].documentoTipo_obrigatorio
														,solicitado: false
														,validado: false} }] });
				
			}
			
		}
		
		return groups;
		
	}

	listaAnexos () {
		
		var ListCategorias = [
			
			{ categoriaDoc_ChaveAuxiliar: null, categoriaDoc: 'anexos', categoriaDoc_Descricao: 'Anexos' }
			
		];
		
		var ListDocumentos = [
			
			{ categoriaDoc: 'anexos', documentoTipo: 'S0_nota', documentoTipo_ChaveAuxiliar: null, documentoTipo_Descricao: 'Nota Fiscal', documentoTipo_obrigatorio: true }
			,{ categoriaDoc: 'anexos', documentoTipo: 'S0_boleto', documentoTipo_ChaveAuxiliar: null, documentoTipo_Descricao: 'Boleto', documentoTipo_obrigatorio: false }
			,{ categoriaDoc: 'anexos', documentoTipo: 'S0_rateio', documentoTipo_ChaveAuxiliar: null, documentoTipo_Descricao: 'Rateio', documentoTipo_obrigatorio: false }
			,{ categoriaDoc: 'anexos', documentoTipo: 'S0_outros', documentoTipo_ChaveAuxiliar: null, documentoTipo_Descricao: 'Outros', documentoTipo_obrigatorio: false }
			,{ categoriaDoc: 'anexos', documentoTipo: 'S3_anexos', documentoTipo_ChaveAuxiliar: null, documentoTipo_Descricao: 'Anexos', documentoTipo_obrigatorio: false }
			
		];
		
		var list = [];
		
		var processoTipo = this.processCod;
		var processoTipo_Descricao = this.processoTipo_Descricao;
		var processoTipo_PastaProd = this.processoTipo_PastaProd;
		var processoTipo_PastaHomolog = this.processoTipo_PastaHomolog;
		
		for (var i in ListCategorias) {
			
			var categoria = ListCategorias[i];
			
			var categoriaDoc = categoria.categoriaDoc;
			var categoriaDoc_ChaveAuxiliar = categoria.categoriaDoc_ChaveAuxiliar;
			var categoriaDoc_Descricao = categoria.categoriaDoc_Descricao;
			
			for (var j in ListDocumentos) {
				
				var documento = ListDocumentos[j];
				
				if (documento.categoriaDoc == categoriaDoc) {
					
					var documentoTipo = documento.documentoTipo;
					var documentoTipo_ChaveAuxiliar = documento.documentoTipo_ChaveAuxiliar;
					var documentoTipo_Descricao = documento.documentoTipo_Descricao;
					var documentoTipo_obrigatorio = documento.documentoTipo_obrigatorio;
					
					list.push({ "processoTipo": processoTipo,
								"processoTipo_Descricao": processoTipo_Descricao,
								"processoTipo_PastaProd": processoTipo_PastaProd,
								"processoTipo_PastaHomolog": processoTipo_PastaHomolog,
								"categoriaDoc": categoriaDoc,
								"categoriaDoc_ChaveAuxiliar": categoriaDoc_ChaveAuxiliar,
								"categoriaDoc_Descricao": categoriaDoc_Descricao,
								"documentoTipo": documentoTipo,
								"documentoTipo_ChaveAuxiliar": documentoTipo_ChaveAuxiliar,
								"documentoTipo_Descricao": documentoTipo_Descricao,
								"documentoTipo_obrigatorio": documentoTipo_obrigatorio });
				
				}
				
			}
			
		}
		
		return list;
		
	}

	_createFolder () {
		
		var that = this;
		
		var name = new Date().getTime().toString(16);
		
		var primaryFolder = that.processoTipo_Pasta;
		
		var processFolder = that.processCod;
		
		var key = Math.floor(Math.random() * (4095 - 256) + 256).toString(16);
		
		var prefix = `${processFolder}/${primaryFolder}/${name}${key}`;
			
		that.processFolder = prefix;
		
		if ($('#hid_folder').val() == '') {
			$('#hid_folder').val(that.processFolder);
		}
		
	}
	
	renameFile(name,count = 0) {
		
		var that = this;
		
		var formartter = name.split('.')
		for (var i in formartter) {
			formartter[i] = formartter[i].replace(/[^\w\s]/gi, '')
		}
		if (count > 0)
			formartter.splice((formartter.length - 1), 0, '(' + count.toString() + ')')
		formartter.splice((formartter.length - 1), 0, '.')
		name = formartter.join('')
		
		var json = $('#anexos_listaCarregados').val();
		
		var anexos = [];
		
		if (json && json != undefined && json != null && json != '')
			anexos = JSON.parse(json);
		
		anexos = anexos.concat(window.ANEXOS.uploads);
		
		var index = anexos.findIndex(x => x.value.name.split('.')[0] == name.split('.')[0])
		
		if (index > -1) {
			
			var arr = name.split('.')
			var arr_name = arr[0].split('(' + count.toString() + ')')
			count++
			var new_name = arr_name[0] + '.' + arr[1]
			name = that.renameFile(new_name,count);
			
		}
		
		return name
			
		
	}
	
	bindGoogleDrive (id,tipo,callback) {
		
		var that = this;
		
		var gdrive = FluigGDriveFactory.getInstance({
			
			apiKey: 'AIzaSyBRtpufZSOl__ScRKsI76dRRor4aIyEbQU',
			clientId: '248300343067-408v45sqlfcv1qes6d8gbcj716honb61.apps.googleusercontent.com',
			
			buttonEl: document.getElementById(id),
			onStart: function(element) {
			},
			onEnd: function(cancel) {
			},
			onSelect: function(file) {
				
				file.name = gdrive.removeAcentos(file.name);

				var extensao = null;
				var ponto = file.name.lastIndexOf('.');
				
				if(ponto != -1) {
					extensao = file.name.substr(ponto+1);
				}
				
				switch(file.mimeType) {
				case 'application/pdf':
					if(extensao != 'pdf') {
						file.name += '.pdf';
					}
					break;
					
				case 'image/jpeg':
				case 'image/jpg':
					if(extensao != 'jpg' && extensao != 'jpeg') {
						file.name += '.jpg';
					}
					break;
				}
				
				gdrive.downloadFile(file.id, function(data) {
					
					var blob = new Blob([data], {type: file.mimeType});
					blob.name = that.renameFile(file.name);
					
					if (!that.validateFile(blob))
						return
						
					var form = new FormData();
					form.append("blob",blob,blob.name)
					
					$.ajax({
	                async : false,
	                type : 'POST',
	                processData: false,
	                contentType: false,
	                url : '/ecm/upload',
	        		data: form,
	        		error: function(e) {
	        			console.error('error on load file')
	        		},
	        		success: function(data) { 
	        			
		        			var f = JSON.parse(data).files[0]
		        			
		        			var doc = {
		    						key: tipo,
		    						value: {
		    							link: URL.createObjectURL(blob),
		    							thumb: '',
		    							thumbName: '',
		    							name: f.name,
		    							createDate: new Date()
		    						}
		    					}
		        			var c1 = DatasetFactory.createConstraint("fileName", f.name, f.name, ConstraintType.MUST);
		        			var c2 = DatasetFactory.createConstraint("thumbnailFormat", 'png', 'png', ConstraintType.MUST);
		        			var constraints = new Array (c1, c2);
		        			
		        			var value = DatasetFactory.getDataset('dsGeraThumbnail', null, constraints, null).values[0];
		        			
		        			doc.value.name = value.fileName;
		        			var blobThumb = that.base64toBlob(value.base64,'application/pdf');
			    			var blob_link = URL.createObjectURL(blobThumb);
			    			
			    			if (value.resultado == 'OK') {
			    				doc.value.thumb = blob_link;
			    				doc.value.thumbName = value.thumbName;
			    			}
		        			
		        			that.uploads.push(doc);
		        			callback(doc);
	        			
		        		}
		        	});
					
				})
			}
		});
		
	}
	
	bindArquivoLocal (input,tipo,callback) {
		
		var that = this;
		
		$(input).change(function () {
			
			for (var file of this.files) {
				
				if (!that.validateFile(file))
					return
				
			}
			
			for (var file of this.files) {
				
				var form = new FormData();
				form.append("blob",file,that.renameFile(file.name))
				
				$.ajax({
	                async : false,
	                type : 'POST',
	                processData: false,
	                contentType: false,
	                url : '/ecm/upload',
	        		data: form,
	        		error: function(e) {
	        			console.error('error on load file')
	        		},
	        		success: function(data) { 
	        			
	        			var f = JSON.parse(data).files[0]
	        			
	        			var doc = {
	    						key: tipo,
	    						value: {
	    							link: URL.createObjectURL(file),
	    							thumb: '',
	    							thumbName: '',
	    							name: f.name,
	    							createDate: new Date()
	    						}
	    					}
	        			
	        			var value = DatasetFactory.getDataset(
			        					'dsGeraThumbnail',
			        					null,
			        					[DatasetFactory.createConstraint('fileName',f.name,f.name,ConstraintType.MUST)],
			        					null
			        		    	).values[0];
	        			
	        			doc.value.name = value.fileName;
	        			var blobThumb = that.base64toBlob(value.base64,'application/pdf');
		    			var blob_link = URL.createObjectURL(blobThumb);
		    			
		    			if (value.resultado == 'OK') {
		    				doc.value.thumb = blob_link;
		    				doc.value.thumbName = value.thumbName;
		    			}
	        			
	        			that.uploads.push(doc);
	        			callback(doc);
	        			
	        		}
	        	});
			
			}
			
		})
		
	}
	
	validateFile (file) {
		
		var uploadErrors = [];
        var acceptFileTypes = /^(image\/(jpe?g|png)|application\/pdf)$/i;
        if(!acceptFileTypes.test(file['type'])) {
            uploadErrors.push('Tipo de arquivo não permitido. Selecione arquivos PDF ou JPG');
        }
        if(file.size && file.size > 2097152) {
            uploadErrors.push('O arquivo excede o tamanho permitido de 2Mb');
        }
        if(uploadErrors.length > 0) {
            alert(uploadErrors.join("\n"));
            return false;
        }
        
        return true;
		
	}
	
	clickShowAnexo (tipos,anexos,focus = null) {
		
		var that = this;
		
		var view = { };
		var template =
			`<div class="modal fade" id="modalLong" tabindex="-1" role="dialog" aria-labelledby="modalLongTitle" aria-hidden="true" style="width: 99% !important;">
				<div class="modal-dialog" role="document" style="width: 100%;margin-top: 0px !important;margin-left: 0px !important;margin-right: 0px !important;">
			    	<div class="modal-content">
			    		<div class="modal-header" style="background-color: azure;padding-bottom: 0;padding-top: 0;">
			    			<div class="row" style="margin-bottom: 5px;">
			    				<div class="col-xs-12 col-md-12">
			    					<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: 0px;">
			    						<i aria-hidden="true" class="fluigicon fluigicon-remove icon-xs"></i>
							    	</button>
			    				</div>
			    			</div>
					    </div>
			      		<div class="modal-body" style="overflow: hidden;padding-bottom: 0px;padding-top: 0px;">
			      			
			      			<link type="text/css" rel="stylesheet" href="/portal/resources/style-guide/css/fluig-style-guide.min.css"/>
				    		<link type="text/css" rel="stylesheet" href="css/style.css"/>
				    		
				    		<div>
				    			` + that.templateForVisualizador() + `
				    		</div>
							
						</div>
			    	</div>
			  	</div>
			</div>`;
		
		window.parent.$('#modalLong').remove();
		
		var modal = Mustache.render(template,view);
		
		window.parent.$('#wcm-content').append(modal);
		
	    window.parent.$('#modalLong').modal();
	    
	    that.templateArquivosForVisualizador(that.retornaGruposDeArquivos(tipos,anexos));
	    
	    that.bindVisualizador();
	    
	    window.parent.$('#block_' + focus).find('.thumbsArq').first().click();

	}
	
	templateForVisualizador() {
		
		var that = this;
		
		var getATV = JSON.parse($('#hid_infoForm').val());
		var template = ``;
		
		if ((getATV.atividade.id == 13 || getATV.atividade.id == 15) && window.FORMULARIO.modeView != "VIEW"){
			
			template =
				`<div id="imgsPdfView" style="margin-bottom: 3px;height: 37rem;max-height: 37rem;overflow-y: scroll;width: 12%;float: left;">
				</div>
				<div id="pdfView" style="max-height: 37rem;height: 37rem;width: 66%;float: left;">
					<div class="pdfInfo-button" style="float:right;max-height: 1.5rem;height: 1.5rem;">
							<b>
								<a type="button" target="_blank" style="margin-right: 10px;float:left;">
									<span>Abrir documento em nova guia</span>
									<i class="fluigicon fluigicon-export icon-xs"></i>
								</a>
							</b>
						</div>
						<div id="no-selected-pdf" class="col-xs-12 col-md-12" style="height: 33.5rem;max-height: 33.5rem;display: grid;color: mediumblue;float: left;">
							<div style="margin-bottom: auto;margin-top: auto;text-align: center;">
								<i class="fluigicon fluigicon-info-sign icon-thumbnail-lg" style="font-size: 15rem;"></i>
							</div>
							<div style="text-align: center;">
								<p style="font-size: larger; "><b>SELECIONE UM DOS ARQUIVOS AO LADO PARA VISUALIZA-LO AQUI</b></p>
							</div>
						</div>
						<div id="selected-pdf" class="col-xs-12 col-md-12" style="height: 33.5rem;max-height: 33.5rem;display: grid;color: mediumblue;">
					</div>
				</div>
				<div id="formInfo" style="margin-bottom: 3px;height: 35rem;max-height: 35rem;overflow-y: scroll;width: 22%;float: right;">
				` + that.bindInfoForm(); `
				</div>`;
				
			
			return template;
			
		} else {
		
			var template =
				`<div class="row">
					<div id="imgsPdfView" class="col-md-2" style="margin-bottom: 3px;max-height: 36rem;height: 36rem;overflow-y: scroll;float: left;padding: 0;">
					</div>
					<div id="pdfView" class="col-md-10" style="max-height: 36rem;height: 36rem;float: right;padding: 0;">
						<div class="pdfInfo-button" style="float:right;max-height: 1.5rem;height: 1.5rem;">
							<b>
								<a type="button" target="_blank" style="margin-right: 10px;float:left;">
									<span>Abrir documento em nova guia</span>
									<i class="fluigicon fluigicon-export icon-xs"></i>
								</a>
							</b>
						</div>
						<div id="no-selected-pdf" class="col-xs-12 col-md-12" style="height: 33.5rem;max-height: 33.5rem;display: grid;color: mediumblue;float: left;">
							<div style="margin-bottom: auto;margin-top: auto;text-align: center;">
								<i class="fluigicon fluigicon-info-sign icon-thumbnail-lg" style="font-size: 15rem;"></i>
							</div>
							<div style="text-align: center;">
								<p style="font-size: larger; "><b>SELECIONE UM DOS ARQUIVOS AO LADO PARA VISUALIZA-LO AQUI</b></p>
							</div>
						</div>
						<div id="selected-pdf" class="col-xs-12 col-md-12" style="height: 33.5rem;max-height: 33.5rem;display: grid;color: mediumblue;">
						</div>
					</div>
				</div>`;
			
			return template;
			
		}
		
	}
	
	retornaGruposDeArquivos (tipos,anexos) {
		var that = this;
		
		var groups = [];
		
		anexos = anexos.concat(that.uploads)
		
		for (var i in tipos) {
			for (var j in tipos[i].value) {
				
				var key = tipos[i].value[j].key;
				var title = tipos[i].value[j].value.descricao;
				
				var value = anexos.filter(x => x.key == key).map(x => x.value);
				
				groups.push({key: key, value: value, title: title })
				
			}
		}
		
		for (var i in groups) {
			for (var j in groups[i].value) {
				if (groups[i].value[j].thumb) {
					var link = groups[i].value[j].thumb
					if (!link.includes('blob:http://') && !link.includes('blob:https://')) {
						
						var data = {
							companyId: 1,
							serviceCode: 'bucketBari',
							endpoint : link,
							method: 'get',
							timeoutService: '100'
						};
						
						window.parent.WCMAPI.Create({
				    	    url: '/api/public/2.0/authorize/client/invoke',
				    	    contentType: "text/json",
				    	    dataType: "json",
				    	    async: false,
				    	    data: JSON.stringify(data),
				    	    success: function(data){
				    	    	var result = data.content.result.split('"').join('')
				    	    	var file = that.base64toBlob(result,'application/octet-stream')
				    	    	var blob_link = URL.createObjectURL(file)
				    	    	groups[i].value[j].thumb = blob_link
				    	    	
				    	    }
						})
						
					}
					
				}
				groups[i].value[j].horario = new Date(groups[i].value[j].createDate).toLocaleTimeString();
				groups[i].value[j].data = new Date(groups[i].value[j].createDate).toLocaleDateString();
				groups[i].value[j].thumb = groups[i].value[j].thumb
				groups[i].value[j].link = groups[i].value[j].link
				
			}
		}
		
		return groups;
		
	}
	
	templateArquivosForVisualizador(groups) {
		
		var that = this;
				
		var template = 
			`{{#linhas}}
				<div id="block_{{key}}" class="block-imgs" style="margin-bottom: 5px;display: none;margin: 0 0 0 0;padding-bottom: 8px;">
					<div style="background-color: #d0ffff;min-height: 2rem;margin-bottom:10px;">
						<div>
							<h5 class="modal-title" style="text-align: center;font-size: large;margin-top: 0px;color: black !important;line-height: 30px !important;">{{title}}</h5>
						</div>
					</div>
					{{#value}}
						<div class="row thumbsArq" style="position:relative;margin-right:auto;margin-left:auto;max-height:9rem;max-width:6rem;display:block;margin-bottom:10px;">
							<div class="imgInfo" style="max-height:8rem;max-width:6rem;height:8rem;width:100px;position:absolute;background-color:rgba(0, 0, 0, 0.54);color:white;display:none;text-align:center;">
								<div class="row">
									Publicado em:
								</div>
								<div class="row">
									{{data}}
								</div>
								<div class="row">
									Hora de Criação:
								</div>
								<div class="row">
									{{horario}}
								</div>
								<div class="row">
									Nome:
								</div>
								<div class="row" style="text-overflow: ellipsis; white-space: nowrap; overflow: hidden; max-width:6rem; margin-left: auto;margin-right: auto;">
									{{name}}
								</div>
							</div>
							<img data-toggle="popover" link="{{link}}" src="{{thumb}}"  style="max-height:8rem;max-width:6rem;height:8rem;width:100px;border-color:black;border-style:solid;border-width: 1px 1px 1px 1px;box-shadow: 5px 5px 6px gray;">
							</img>
						</div>
					{{/value}}
				</div>
			{{/linhas}}`;
		
		var view = { linhas: groups };
		
		var list = Mustache.render(template,view);
		
		window.parent.$('#imgsPdfView').append(list);
		
		for (var i in groups) {
			
			if (groups[i].value.length > 0) {
				
				window.parent.$('#block_' + groups[i].key).show();
				
			}
			
		}
		
	}
	
	base64toBlob(base64Data, contentType) {
		
		try {
			
	        contentType = contentType || '';
	        var sliceSize = 1024;
	        var byteCharacters = atob(base64Data);
	        var bytesLength = byteCharacters.length;
	        var slicesCount = Math.ceil(bytesLength / sliceSize);
	        var byteArrays = new Array(slicesCount);
	        for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
	            var begin = sliceIndex * sliceSize;
	            var end = Math.min(begin + sliceSize, bytesLength);
	            var bytes = new Array(end - begin);
	            for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
	                bytes[i] = byteCharacters[offset].charCodeAt(0);
	            }
	            byteArrays[sliceIndex] = new Uint8Array(bytes);
	        }
	        
	        return new Blob(byteArrays, { type: contentType });
	        
		} catch (e) {
			
			return new Blob();
			
		}
		
    }
	
	bindVisualizador () {
		
		var that = this;
		
		window.parent.$('.thumbsArq').hover(
			function() {
				$(this).find('div').show();
			},function() {
				$(this).find('div').hide();
			}	
		)
		
		window.parent.$('.thumbsArq').click(function() {
			
			var link = $(this).find('img').attr('link');
			
			window.parent.$('#no-selected-pdf').hide();
			
			window.parent.$('#pdfView').find('.pdfInfo-button').hide();
			
			window.parent.$('.thumbsArq').find('img').css('border-color','black');
			$(this).find('img').css('border-color','blue');
			
			window.parent.$('.thumbsArq').find('img').css('box-shadow','5px 5px 6px gray');
			$(this).find('img').css('box-shadow','5px 5px 6px darkslateblue');
			
			window.parent.$('#selected-pdf').show();
			window.parent.$('#selected-pdf').html('');
			window.parent.$('#selected-pdf').append('Loading...');
			
			if (link.includes('blob:http://') || link.includes('blob:https://')) {
			
				fetch(link).then(d => d.blob().then(file => {
					
					var blob_link = URL.createObjectURL(file)
					
					var html = `<embed class="pdfobject" src="${blob_link}" type="application/pdf" style="overflow: auto; width: 100%; height: 100%;"></embed>`;
					window.parent.$('#selected-pdf').html('');
					window.parent.$('#selected-pdf').append(`<div id="Div1">${html}</div>`);
					
					window.parent.$('#pdfView').find('.pdfInfo-button').find('a').attr('href',blob_link);
					window.parent.$('#pdfView').find('.pdfInfo-button').show();
					
				}))
			
			} else {
				var input = $(this);
				
				var data = {
					companyId: 1,
					serviceCode: 'bucketBari',
					endpoint : link,
					method: 'get',
					timeoutService: '100'
				};
				
				window.parent.WCMAPI.Create({
		    	    url: '/api/public/2.0/authorize/client/invoke',
		    	    contentType: "text/json",
		    	    dataType: "json",
		    	    async: false,
		    	    data: JSON.stringify(data),
		    	    success: function(data){
		    	    	var result = data.content.result.split('"').join('')
		    	    	var file = that.base64toBlob(result,'application/pdf')
		    	    	var blob_link = URL.createObjectURL(file)
						
						var html = `<embed class="pdfobject" src="${blob_link}" type="application/pdf" style="overflow: auto; width: 100%; height: 100%;"></embed>`;
						window.parent.$('#selected-pdf').html('');
						window.parent.$('#selected-pdf').append(`<div id="Div1">${html}</div>`);
						
						window.parent.$('#pdfView').find('.pdfInfo-button').find('a').attr('href',blob_link);
						window.parent.$('#pdfView').find('.pdfInfo-button').show();
						
						$(input).find('img').attr('link',blob_link);
		    	    	
		    	    }
				})
				
			}
			
		})
		
		//inicio da mudança no forms
		//CEF
		window.parent.$('#Anx_CEF_OK').click(function(){ that.bindAtualizaForm(); });
		window.parent.$('#Anx_CEF_NO').click(function(){ that.bindAtualizaForm(); });
		window.parent.$('#Anx_CEF_lancamento').keyup(function(){ that.bindAtualizaForm(); });
		window.parent.$('#Anx_CEF_obs').keyup(function(){ that.bindAtualizaForm(); });
		//Pagamento
		window.parent.$('#Anx_pgto_DataPagamento').change(function(){ that.bindAtualizaForm(); });
		//fim
		
	}
	
	bindInfoForm() {
		var that = this;
		var infoForm = ``;
		infoForm = `
					<input type="hidden" id="hid_infoAtv" name="hid_infoAtv"/>
					<div id="AnxInfo" style="opacity: 0.9">
						<div class="col-xs-12">
							<label for="Anx_info_filial">Filial</label>
							<input type="text" id="Anx_info_filial" name="Anx_info_filial" class="form-control" value="` + $('#S0_filial').val() + `" readonly/>
						</div>
						<div class="col-xs-12" style="margin-top:2px">
							<label for="Anx_info_complemento">Complemento</label>
							<input type="text" id="Anx_info_complemento" name="Anx_info_complemento" class="form-control" value="` + $('#S0_complemento').val() + `" readonly/>
						</div>
						<div class="col-xs-12" style="margin-top:2px">
							<label for="Anx_info_cnpjCpf">CNPJ/CPF</label>
							<input type="text" id="Anx_info_cnpjCpf" name="Anx_info_cnpjCpf" class="form-control" value="` + $('#S0_CNPJCPF').val() + `" readonly/>
						</div>
						<div class="col-xs-12" style="margin-top:2px">
							<label for="Anx_info_nota">Nº Nota</label>
							<input id="Anx_info_nota" name="Anx_info_nota" class="form-control" value="` + $('#S0_numnota').val() + `-` + $('#S0_serie').val() + `" readonly/>
						</div>
						<div class="col-xs-12" style="margin-top:2px">
							<label for="Anx_info_departamento">Departamento</label>
							<input type="text" id="Anx_info_departamento" name="Anx_info_departamento" class="form-control" value="` + $('#S0_departamento').val() + `" readonly/>
						</div>
						<div class="col-xs-12" style="margin-top:2px" id="div_anx_info_contaGerencial">
							<label for="Anx_info_contaGerencial">Conta Gerencial</label>
							<input type="text" id="Anx_info_contaGerencial" name="Anx_info_contaGerencial" class="form-control" value="` + $('#S0_contagerencial').val() + `" readonly/>
						</div>
						<div class="col-xs-12" style="margin-top:2px" id="div_anx_info_af">
							<label for="Anx_info_af">Nº AF</label>
							<input type="text" id="Anx_info_af" name="Anx_info_af" class="form-control" value="` + $('#S0_nraf').val() + `" readonly/>
						</div>
						<div class="col-xs-12" style="margin-top:2px" id="div_anx_info_placaOS">
							<label for="Anx_info_placaOS">Placa / O.S</label>
							<input type="text" id="Anx_info_placaOS" name="Anx_info_placaOS" class="form-control" value="Placa:` + $('#S0_placa').val() + ` | OS:` + $('#S0_numOS').val() + `" readonly/>
						</div>
						<div class="col-xs-12" style="margin-top:2px" id="div_anx_info_rateio">
							<label for="Anx_info_rateio">Rateio</label>
							<input type="text" id="Anx_info_rateio" name="Anx_info_rateio" class="form-control" value="` + $('#S0_resumoRateio').val() + `" readonly/>
						</div>
						<div class="col-xs-12" style="margin-top:2px" id="div_anx_info_cPagamento">
							<label for="Anx_info_cPagamento">Condição de Pagamento</label>
							<input type="text" id="Anx_info_cPagamento" name="Anx_info_cPagamento" class="form-control" value="` + $('#S0_condicaoPagamento').val() + `" readonly/>
						</div>
						<div class="col-xs-12" style="margin-top:2px" id="div_anx_info_dVencimento">
							<label for="Anx_info_dVencimento">Data Vencimento</label>
							<input type="text" id="Anx_info_dVencimento" name="Anx_info_dVencimento" class="form-control" value="` + $('#temp_data_001').val() + `" readonly/>
						</div>
						<div class="col-xs-12 col-md-12">
							<label for="Anx_info_obs">Observação</label>
							<textarea id="Anx_info_obs" name="Anx_info_obs" class="form-control" rows="3" readonly></textarea>
						</div>
					</div>
					<div id="AnxAprovCef">
						<div class="col-xs-12">
							<p style="font-size:150%; color: grey; margin-top: 10px;"> NF tem Retenção? </p>
						</div>
						<div class="col-xs-12">
							<label>
								<input type="radio" name="Anx_CEF_retencao" ` + ($('input[name=S3_retencao]:checked').val() == 'SIM'? 'checked' : '') + ` value="SIM"/>
								SIM
							</label>
							<label>
								<input type="radio" name="Anx_CEF_retencao" ` + ($('input[name=S3_retencao]:checked').val() == 'NAO'? 'checked' : '') + ` value="NAO"/>
								NÃO
							</label>
						</div>
						<div class="col-xs-12">
							<p style="font-size:150%; color: grey; margin-top: 10px;"> Aprovação CEF </p>
						</div>
						<div class="col-xs-12" id="div_anx_CEF_aprovacao">
							<label for="Anx_CEF_OK" style="float:left;">SIM</label>
							<input class="check_validation" type="checkbox" id="Anx_CEF_OK" name="Anx_CEF_OK" style="float:left; margin-left: 5px"/>
							<label for="Anx_CEF_NO" style="float:left; margin-left: 15px">NÃO</label>
							<input class="check_validation" type="checkbox" id="Anx_CEF_NO" name="Anx_CEF_NO" style="margin-left: 5px"/>
						</div>
						<div class="col-xs-12" id="div_Anx_CEF_lancamento">
							<label for="Anx_CEF_lancamento">Nº Lançamento</label>
							<input type="number" id="Anx_CEF_lancamento" name="Anx_CEF_lancamento" class="form-control" readonly/>
						</div>
						<div class="col-xs-12 col-md-12" id="div_anx_CEF_obs">
							<label for="Anx_CEF_obs">Observação</label>
							<textarea id="Anx_CEF_obs" name="Anx_CEF_obs" class="form-control" rows="3"></textarea>
						</div>
					</div>
					<div id="AnxAprovCP">
						<div class="col-xs-12">
							<p style="font-size:150%; color: grey; margin-top: 10px;"> Aprovação CP </p>
						</div>
						<div class="col-xs-12">
							<label for="Anx_info_CP">Aprovado</label>
							<select id="Anx_info_CP" name="Anx_info_CP" class="form-control">
								` + that.loadSelectOption(); infoForm = infoForm +`
							</select>
						</div>
						<div class="col-xs-12 col-md-12" id="div_anx_obs_CP">
							<label for="Anx_CEF_obs_cp">Observação</label>
							<textarea id="Anx_CEF_obs_cp" name="Anx_CEF_obs_cp" class="form-control" rows="3"></textarea>
						</div>
					</div>
					<div id="AnxPagamento">
						<div class="col-xs-12">
							<p style="font-size:150%; color: grey; margin-top: 10px;"> Pagamento </p>
						</div>
						` + that.loadTable() +`
						<div class="col-xs-12" style="margin-top: 5px">
							<label for="Anx_pgto_DataPagamento">Data Pagamento</label>
							<input type="date" id="Anx_pgto_DataPagamento" name="Anx_pgto_DataPagamento" class="form-control" onkeydown="return false">
						</div>
					</div>
					<div style="margin-top: 15px">
						<div class="col-xs-12" style="margin-top: 8px">
							<button type="button" class="btn btn-primary" style="float: right;" data-dismiss="modal">Fechar</button>
							<button type="button" class="btn btn-primary" style="float: right; margin-right: 5px" data-send onClick="validateForm()">Enviar</button>
						</div>
					</div>
					<script>
							$('#Anx_info_obs').val("` + $('#S0_Obs').val() + `");
							var infoAtv = "` + JSON.parse($('#hid_infoForm').val()).atividade.id + `";
							var infoMarca = "` + $('#S0_marca').val() + `";
							var infoTipo =  "` + $('#S0_tipo').val() + `";
							var infoComplemento = "` + $('#S0_complemento').val() + `";
							

							if (infoComplemento == "NOTA FISCAL SEM PAGAMENTO") {
								document.getElementById("div_anx_info_contaGerencial").style.display = 'none';
								document.getElementById("div_anx_info_cPagamento").style.display = 'none';
								document.getElementById("div_anx_info_dVencimento").style.display = 'none';
								
							} if (infoTipo != "TELEMETRIX" && infoTipo != "BASE B") {
								document.getElementById("div_anx_info_af").style.display = 'none';
								document.getElementById("div_anx_info_rateio").style.display = 'none';
							
							} if (infoTipo != "CUSTO") {
								document.getElementById("div_anx_info_placaOS").style.display = 'none';
							
							} if (infoAtv != 15) {
								var aprovCP = "`+ $('#S4_aprovacao').val() +`";
								if (aprovCP != 'pendenciaFiscal') {
									document.getElementById("AnxAprovCP").style.display = 'none';
									document.getElementById("AnxPagamento").style.display = 'none';
								
								} else if (aprovCP == 'pendenciaFiscal') {
									$('#AnxAprovCP').css({"pointer-events": "none" , "opacity" : "0.9"});
									document.getElementById("AnxPagamento").style.display = 'none';
								}
							
							} if (infoAtv == 15) {
								$('#AnxAprovCef').css({"opacity" : "0.9"});
								$('#div_anx_CEF_aprovacao').css({"pointer-events": "none", "opacity" : "0.9"});
								$("#Anx_CEF_lancamento").attr("readonly", true);
								$("#Anx_CEF_obs").attr("readonly", true);
							}
							
							//validação CEF
							
							var infoAprovacao = "` + $('#S3_aprovacao').val() + `";
							var infoLancamento = "` + $('#S3_numLancamento').val() + `";
							var infoObs = "` + $('#S3_Obs').val() + `";
							
							if (infoAprovacao == "SIM"){ $('#Anx_CEF_OK').prop("checked", true) }
							if (infoAprovacao == "NAO"){ $('#Anx_CEF_NO').prop("checked", true) }
							
							$("#Anx_CEF_lancamento").val(infoLancamento);
							$("#Anx_CEF_obs").val(infoObs);
							
							$("#Anx_CEF_OK").click(function () {
							 	if ($(this).is(':checked') ){
									$("#Anx_CEF_NO").prop("checked", false); 
								} 
							});
							$("#Anx_CEF_NO").click(function () {
								if ($(this).is(':checked') ){
									$("#Anx_CEF_OK").prop("checked", false); 
								} 
							});
							
							
							//$("#Anx_pgto_DataPagamento").val(infoDataPagamento);
							
							function validateForm(){
								function _msgDanger (msg,titlex) {
									titlex = (_validaObj(titlex))?titlex:"";
									FLUIGC.toast({
								        title:titlex,
								        message:msg,
								        type:"danger",
								    });
								}
								
								function _validaObj (obj) {
									 if (obj != null && 
										 obj != "null" &&
										 obj != undefined && 
										 obj != "undefined" && 
										 obj != false &&
										 obj != "false" &&
										 obj != "") {
										return(true);
									}else{
										return(false);
									}
								}						
								
								if (infoAtv == 13) {
									
									$('#Anx_CEF_obs').removeClass('error');
									$('#Anx_CEF_lancamento').removeClass('error');
									
									if ($("#Anx_CEF_NO").is(':checked') != true && $("#Anx_CEF_OK").is(':checked') != true ){
										_msgDanger('Informe no checkbox se está aprovado ou não');
										return false;
										
									} if ($("#Anx_CEF_NO").is(':checked')) {
										if ($('#Anx_CEF_obs').val() == undefined || $('#Anx_CEF_obs').val() == null || $('#Anx_CEF_obs').val() == "") {
											$('#Anx_CEF_obs').addClass('error');
											_msgDanger('O campo observação é obrigatório');
											return false;
										
										} else {
											$('.modal').modal('toggle');
										}
									
									}  else {
										$('.modal').modal('toggle');
									}
								
								}
								
							}
					</script>
					<style>
						.error {
							border: solid 1px red !important;
						}
					</style>`
					
		return infoForm;
	}
	
	bindAtualizaForm(){
		
		var infoAtv = JSON.parse($('#hid_infoForm').val()).atividade.id;
		
		if (infoAtv == 13) { 
			if (window.parent.$("#Anx_CEF_OK").is(':checked') ){
				$('#S3_aprovacao').val('SIM');
			
			} else if (window.parent.$("#Anx_CEF_NO").is(':checked') ){
				$('#S3_aprovacao').val('NAO');
			
			} else {
				$('#S3_aprovacao').val('');
			}
			
			$('#S3_retencao').val(window.parent.$('input[name=Anx_CEF_retencao]:checked').val());
			$('#S3_numLancamento').val(window.parent.$('#Anx_CEF_lancamento').val());
			$('#S3_Obs').val(window.parent.$('#Anx_CEF_obs').val());
		
		}
		
	}
	
	loadSelectOption(){
		
		var template = `<option value="">==SELECIONE==</option>
						<option value="SIM">Pagamento Agendado/Efetuado</option>
						<option value="pendenciaFiscal">Lançamento Fiscal com Inconsistência</option>
						<option value="pendenciaInicio">Falta Dados de Pagamento</option>`;
		
		return template;
	}
	
	loadTable() {
				
		if ($('#S0_complemento').val() == "NOTA FISCAL SEM PAGAMENTO"){
			return;
		
		} else {
			var infoParcelas = JSON.parse($('#hid_parcelas').val());
			var infoNaoPagos = infoParcelas.filter(x => x.data_pagamento == undefined);

			var template = `<div class="col-xs-12 col-md-12">
								<table class="table table-condensed table-bordered">
									<tbody>
										<tr>
											<th>Parcela</th>
											<td>`+ infoNaoPagos[0].numero +`</td>
										</tr>
										<tr>
											<th>Valor</th>
											<td>R$ `+ infoNaoPagos[0].valor +`</td>
										</tr>
										<tr>
											<th>Data de Vencimento</th>
											<td>`+ infoNaoPagos[0].data_vencimento +`</td>
										</tr>
									</tbody>
								</table>
							</div>`;

			return template;
		}
		
	}
	
	async uploadToBucket () {
		var that = this;
		
		var prefix = $('#hid_folder').val();
			
		var constraints = []
		
		var flag = true
		
		if (that.uploads.length > 0) {
		
			for (var file of that.uploads) {
				
				if (file.value.thumbName != '') 
					constraints.push(DatasetFactory.createConstraint('fileName',file.value.thumbName,file.value.thumbName,ConstraintType.MUST))
				var c = DatasetFactory.createConstraint('fileName',file.value.name,file.value.name,ConstraintType.MUST)
				constraints.push(c)
				
			}
		
			constraints.push(
						DatasetFactory.createConstraint('prefix',prefix,prefix,ConstraintType.MUST)
					)
				
			var value = DatasetFactory.getDataset(
									'dsSaveBucket',
									null,
									constraints,
									null
						    	).values[0];
				
			if (value.code == '200') {
				
				var result = JSON.parse(value.result)
				
				for (var content of result.content) {
					
					if (content.result.ok) {
						
						console.log(`${content.file} - ${content.result.msg}`)
					
						var index = that.uploads.findIndex(x => x.value.name == content.file)
						
						if (index > -1) {
							
							that.uploads[index].value.link = content.result.link
							that.uploads[index].value.createDate = new Date(content.result.updateAt)
							
						} else {
							
							var thumbIndex = that.uploads.findIndex(x => x.value.thumbName == content.file)
							
							if (thumbIndex > -1)
								that.uploads[thumbIndex].value.thumb = content.result.link
							
						}
					
					} else {
						
						flag = false
						console.err(`${content.file} - ${content.result.msg}`)
						_msgDanger(`Não foi possivel fazer o upload do anexo ${content.file}`);
						
					}
					
				}
				
				var list = [];
				
				if ($('#anexos_listaCarregados').val() != undefined && $('#anexos_listaCarregados').val() != null && $('#anexos_listaCarregados').val() != '')
					list = JSON.parse($('#anexos_listaCarregados').val());
				
				list = list.concat(that.uploads)
				
				$('#anexos_listaCarregados').val(JSON.stringify(list));
				
				that.uploads = []
				
			} else {
				
				_msgDanger("Erro ao realizar o upload de anexos");
				flag = false
				
			}
			
		}
		
		return flag;
		
	}
	
}