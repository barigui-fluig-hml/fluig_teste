//Teste

class atv0 extends form {
	
	constructor() {
		
		super();
		this.marcas = [];
		this.filiais = [];
		this.NFes = [];
		
		this.temp_numNf_fornecedor = `<div class="row">
											<div class="col-xs-12 col-md-4">
												<label for="S_temp_numnota">Nº Nota</label>
												<input type="text" id="S_temp_numnota" name="S_temp_numnota" class="form-control" obrigatorio="true"/>
											</div>
											<div class="col-xs-12 col-md-4">
												<label for="S_temp_serie">Serie</label>
												<input type="text" id="S_temp_serie" name="S_temp_serie" class="form-control" obrigatorio="true"/>
											</div>
									  </div>
									  <div class="row">
											<div class="col-xs-12 col-md-4">
												<label for="S_temp_tipo">Tipo</label>
												<select id="S_temp_tipo" name="S_temp_tipo" class="form-control" obrigatorio="true">
													<option value="CNPJ" selected>CNPJ</option>	
													<option value="CPF">CPF</option>						
												</select>
											</div>
											<div class="col-xs-12 col-md-4">
												<label for="S_temp_CNPFCPF">CNPJ/CPF</label>
												<input type="text" id="S_temp_CNPFCPF" name="S_temp_CNPFCPF" class="form-control " obrigatorio="true"/>
											</div>
											<div class="col-xs-12 col-md-4">
												<label for="S_temp_fornecedor">Fornecedor</label>
												<input type="text" id="S_temp_fornecedor" name="S_temp_fornecedor" class="form-control " readonly obrigatorio="true"/>
											</div>
									  </div>`
		
	}
	
	_loadSection () {
		
		var that = this;
		
		$('#btn_initForm').attr('disabled','disabled');
		
		window.parent.$('#workflowActions').hide();
		
		window.parent.$('button').each(function () {
			
			var attr = $(this).attr('data-send');
			
			if (typeof attr !== typeof undefined && attr !== false) {
				  
				$(this).closest('div').find('button').attr('disabled','disabled');
			
			}
			
		})
		
		$('.lista-anexos').closest('section').hide();
		
		$('#S0_dtrecebimento').val(new Date().toLocaleDateString());
		
		var constraints = [];
		that.marcas = that._getData('dsBARIGUI_marcas',constraints);
		
		that.marcas.sort(function (a, b) {
			
			if (a.Descricao < b.Descricao) {
				return -1 
			}
			
			if (b.Descricao < a.Descricao) {
				return 1
			}
			
			return 0;
			
		});
		
		that._initSelectInput('S_sel_marca',that.marcas.map(x => ({key: x.MarcaID, value: x.Descricao}) ));
		
		$('#S_sel_marca').change(function(){
			
			var selected = $(this).val();
			
			if (selected == "") {
				
				$('#S_sel_filial').html(`<option value="">==SELECIONE==</option>`);
				$('#S_sel_filial').val("");
				$('#S_sel_filial').unbind();
				
			} else {
				
				that.bindFilial(selected);
				
			}
			
			that.bindIniciar();
			
		})
		
		$('#S_sel_tiponota').change(function() {
			
			if ($(this).val() == '0') {
				
				$('#S_sel_tiponota').closest('div').attr('class','col-xs-12 col-md-3');
				$('#S_sel_filial').closest('div').attr('class','col-xs-12 col-md-3');
				$('#S_sel_marca').closest('div').attr('class','col-xs-12 col-md-3');
				
				$('#S_sel_tipoemissao').closest('div').show();
				
				$('#S0_tiponota').val('Produto'.toUpperCase());
				
			} else {
				
				$('#S_sel_tiponota').closest('div').attr('class','col-xs-12 col-md-4');
				$('#S_sel_filial').closest('div').attr('class','col-xs-12 col-md-4');
				$('#S_sel_marca').closest('div').attr('class','col-xs-12 col-md-4');
				
				$('#S_sel_tipoemissao').val('');
				$('#S_sel_tipoemissao').closest('div').hide();
				
				if ($(this).val() == '1') {
					
					$('#S0_tiponota').val('Serviço'.toUpperCase())
					
				} else {
					
					$('#S0_tiponota').val('')
					
				}
				
			}
			
			that.bindIniciar();
			
		});
		
		$('#S_sel_tipoemissao').change(function() { that.bindIniciar() });
		
		$('#S0_infoNFSP').change(function() { 
			if ($('#S0_infoNFSP').val().toUpperCase() == "Sim".toUpperCase()) {
				that._msgDanger('Esse fluxo é aplicável apenas para notas que efetivamente não possuem pagamento. Favor escolher outro fluxo que seja mais adequado');
			}
		});
		
		function convertePlacaParaMercosul(placa) {
			var letraPlaca = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];
			return placa.replace(placa[4], letraPlaca[placa[4]]);
		}

		function converteMercosulParaPlaca(placa) {
			var letraPlaca = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];
			return placa.replace(placa[4], letraPlaca.indexOf(placa[4]));
		}

		function retornaInformacaoUsado(url) {
			var retorno = null;
			var data = {
					companyId: 1,//WCMAPI.organizationId,
					serviceCode: 'CoreREST',
					endpoint : url,
					method: 'get',
					timeoutService: '100'
				};

			window.parent.WCMAPI.Create({
				url: '/api/public/2.0/authorize/client/invoke',
				contentType: "text/json",
				dataType: "json",
				async: false,
				data: JSON.stringify(data),
				success: function(data){
					
					if(data.message && data.message.message == 'OK') {
						var response = data.content;
						if (response.httpStatusResult == 200) {
							if(response.result) {
								
								retorno = JSON.parse(response.result);
								
							}
						}
					}
					
				}
			});
			return retorno;
		}

		

		$('#S0_placa').on('change', function() {
			var filial = that._getData('dsBARIGUI_lojasIntranet',null).find(x => x.CNPJ == $('#S0_CNPJ_filial').val().replace(/[^a-zA-Z0-9]/g,'') && x.Status == "A");
			var url = '/api/fluig/consultasDados/GetVendaVeiculoByPlaca/' + filial.LojaID + '/';
			var placa = $('#S0_placa').val().trim().toUpperCase();
			var placa_opcional = '';

			const regexPlacaNormal = /^[a-zA-Z]{3}[0-9]{4}$/;
			const regexPlacaMercosul = /^[a-zA-Z]{3}[0-9]{1}[a-zA-Z]{1}[0-9]{2}$/;

			if(regexPlacaNormal.test(placa)){
				 
				 placa_opcional = convertePlacaParaMercosul(placa).toUpperCase();

			} else if (regexPlacaMercosul.test(placa)){
				
				placa_opcional = converteMercosulParaPlaca(placa).toUpperCase();
				
			} else {
				
				$('#S0_informacaoPlaca').val('Placa inválida');
				return;

			}
			var infoSeminovo = retornaInformacaoUsado(url + placa);
			
			if (infoSeminovo != null) {
			
				if (infoSeminovo.length == 0) {
					infoSeminovo = retornaInformacaoUsado(url + placa_opcional);
					if (infoSeminovo.length == 0) {
						$('#S0_informacaoPlaca').val('Seminovo fora do Estoque');
					} else {
						$('#S0_informacaoPlaca').val('Seminovo em Estoque');
					}
				
				} else {
					$('#S0_informacaoPlaca').val('Seminovo em Estoque');
				}
			
			} else {
				that._msgWarning('Erro durante a conexão ao Banco de Dados');
				$('#S0_informacaoPlaca').val('');
			}
			
			
		});
		
		
	}
	
	validaAnexos () {
		var that = this;
		
		var ret = true;
		
		$('.error').removeClass('error');
		
		if ($('#S0_informaRateio').val() == 'SIM') {
			
			if (parseInt($('#S0_rateio span')[0].innerHTML) <= 0) {
				
				ret = false;
				$('#S0_rateio').addClass('error')
				that._msgDanger("O Anexo Rateio é obrigatório!!");
				
			}
			
		}
		
		if (parseInt($('#S0_nota span')[0].innerHTML) <= 0) {
			
			ret = false;
			$('#S0_nota').addClass('error')
			that._msgDanger("O Anexo NotaFiscal é obrigatório!!");
			
		}
		
		return ret;
		
	}
	
	bindFilial (MarcaID) {
		
		var that = this;
		
		var constraints = [];
		constraints.push(DatasetFactory.createConstraint('MarcaID',MarcaID,MarcaID,ConstraintType.MUST));
		constraints.push(DatasetFactory.createConstraint('status','A','A',ConstraintType.MUST));
		
		if (MarcaID == 19) {
			
			constraints.push(DatasetFactory.createConstraint('LojaID','152','152',ConstraintType.SHOULD));
			constraints.push(DatasetFactory.createConstraint('LojaID','136','136',ConstraintType.SHOULD));
			
		} else if (MarcaID == 13) {
			
			constraints.push(DatasetFactory.createConstraint('LojaID','14','14',ConstraintType.MUST));
			
		}
		
		that.filiais = that._getData('dsBARIGUI_lojasIntranet',constraints);
		
		that.filiais.sort(function (a, b) {
			
			if (a.Nome < b.Nome) {
				return -1 
			}
			
			if (b.Nome < a.Nome) {
				return 1
			}
			
			return 0;
			
		});
		
		that._initSelectInput('S_sel_filial',that.filiais.map(x => ({key: x.LojaID, value: x.Nome.toUpperCase()}) ));
		
		$('#S_sel_filial').change(function(){
			
			that.bindIniciar();
			
		})
		
	}
	
	bindIniciar() {
		
		var that = this; 
		
		if (!$('#S_sel_marca').val() || $('#S_sel_marca').val() == null || $('#S_sel_marca').val() == ""
				|| !$('#S_sel_filial').val() || $('#S_sel_filial').val() == null || $('#S_sel_filial').val() == ""
				|| !$('#S_sel_tiponota').val() || $('#S_sel_tiponota').val() == null || $('#S_sel_tiponota').val() == ""
				|| ($('#S_sel_tiponota').val() == '0' &&  (!$('#S_sel_tipoemissao').val() || $('#S_sel_tipoemissao').val() == null || $('#S_sel_tipoemissao').val() == ""))) {
			
			$('#btn_initForm').unbind();
			$('#btn_initForm').attr('disabled','disabled');
			$('#S_forTemps').html('');
			$('#S_forTemps').hide();
			
		} else {
			
			if (($('#S_sel_tiponota').val() == '0' && $('#S_sel_tipoemissao').val() == '1') || $('#S_sel_tiponota').val() == '1') {
				
				$('#S_forTemps').html('');
				$('#S_forTemps').append(that.temp_numNf_fornecedor);
				
				$('#S_temp_numnota').val('');
				$('#S_temp_fornecedor').val('');
				$('#S_temp_CNPFCPF').val('');
				
				$('#btn_initForm').unbind();
				$('#btn_initForm').attr('disabled','disabled');
				
				$('#S_forTemps').show();
				
				$('#S_temp_numnota').mask('999999999');
				
				$('#S_temp_CNPFCPF').mask('00.000.000/0000-00');
				
				$('#S_temp_tipo').change(function() {
					
					$('#S_temp_CNPFCPF').val('');
					
					if ($(this).val() == 'CPF') {
						$('#S_temp_CNPFCPF').mask('000.000.000-00');
					} else if ($(this).val() == 'CNPJ') {
						$('#S_temp_CNPFCPF').mask('00.000.000/0000-00');
					} else {
						$('#S_temp_CNPFCPF').attr('disabled','disabled');
					}
					
				})
				
				$('#S_temp_CNPFCPF').keydown(function() {
					
					var func = async function () {
						
						var num = null;
						
						if ($('#S_temp_tipo').val() == 'CPF') {
							if ($('#S_temp_CNPFCPF').val().length == 14) {
								
								num = await $('#S_temp_CNPFCPF').val().split('.').join('').split('-').join('');
								
							}
						} else if ($('#S_temp_tipo').val() == 'CNPJ') {
							if ($('#S_temp_CNPFCPF').val().length == 18) {
								
								num = await $('#S_temp_CNPFCPF').val().split('.').join('').split('-').join('').split('/').join('');
								
							}
						}
						
						if (num != null) {
							
							$('#S_temp_fornecedor').val(
									that._getData('dsBARIGUI_fornecedoresAPI',[
																				DatasetFactory.createConstraint('Pessoa_DocIdentificador'
																												,num
																												,num
																												,ConstraintType.MUST),
																				DatasetFactory.createConstraint('lojaId'
																														,$('#S_sel_filial').val()
																														,$('#S_sel_filial').val()
																														,ConstraintType.MUST)
																			]
										)[0].Pessoa_Nome
									);
							
						} else {
							
							$('#S_temp_fornecedor').val('');
							
						}
						
					}
					
					func();
					
				})
						
				var onChangeTemps = function() {
					
					if (!$('#S_temp_numnota').val() || $('#S_temp_numnota').val() == null || $('#S_temp_numnota').val() == ""
						|| !$('#S_temp_fornecedor').val() || $('#S_temp_fornecedor').val() == null || $('#S_temp_fornecedor').val() == ""
						|| !$('#S_temp_CNPFCPF').val() || $('#S_temp_CNPFCPF').val() == null || $('#S_temp_CNPFCPF').val() == ""
						|| !$('#S_temp_serie').val() || $('#S_temp_serie').val() == null || $('#S_temp_serie').val() == "") {
						
						$('#btn_initForm').unbind();
						$('#btn_initForm').attr('disabled','disabled');
						
					} else {
						
						$('#btn_initForm').removeAttr('disabled');
						
						$('#btn_initForm').unbind();
						
						$('#btn_initForm').click(function() {
							
							var temp_CNPFCPF_isValid = $('#S_temp_tipo').val() == 'CNPJ' ?
															TestaCNPJ($('#S_temp_CNPFCPF').val()) :
															$('#S_temp_tipo').val() == 'CPF' ?
																	TestaCPF($('#S_temp_CNPFCPF').val()) :
																	false;
							
							$('#S_temp_CNPFCPF').removeClass('error');
																	
							if (temp_CNPFCPF_isValid) {
																	
								$('#S0_hid_isNFe').val('false');
								
								var filial = that.filiais.find(x => x.LojaID == $('#S_sel_filial').val());
								
								var groups = that._getData('dsBARIGUI_lojasIntranet',null);
								
								that.NFe = {
										razaoSocialEmissor: '',
										cnpjEmissor: '',
										numero: '',
										numero: '',
										tipoNota: '',
										valorTotal: '',
										empresa: {
											nomeFantasia: '',
											cnpj: '',
										}
								};
								;
								that.NFe.empresa.nomeFantasia = groups.find(x => x.LojaID == filial.LojaID && x.Status == "A").Nome;
								that.NFe.empresa.cnpj = filial.CNPJ;
								
								that.NFe.razaoSocialEmissor = $('#S_temp_fornecedor').val();
								that.NFe.cnpjEmissor = $('#S_temp_CNPFCPF').val();
								that.NFe.tipoNota = $('#S_sel_tiponota').val();
								
								that.NFe.numero = $('#S_temp_numnota').val().trim().replace(/^(0+)(\d)/g,"$2"); //remove “0″ à esquerda
								
								that.NFe.serie =  $('#S_temp_serie').val().trim(); 
								
								that.showDadosNota();
								
							} else {
								
								$('#S_temp_CNPFCPF').addClass('error');
								that._msgDanger($('#S_temp_tipo').val() + ' inválido');
								
							}
							
						})
						
					}
					
				}
				
				$('input[id*="S_temp"]').keyup(onChangeTemps);
				$('select[id*="S_temp"]').change(onChangeTemps);
				
			} else if ($('#S_sel_tiponota').val() == '0' && $('#S_sel_tipoemissao').val() == '0') {
				
				$('#S_forTemps').hide();
				
				$('#btn_initForm').removeAttr('disabled');
				
				$('#btn_initForm').click(function() {
					
						$('#S0_hid_isNFe').val('true');
						
						that.showModalNFe();
					
				})
				
			} else {
				
				$('#S_temp_numnota').val('');
				$('#S_temp_fornecedor').val('');
				$('#S_temp_CNPFCPF').val('');
				
				$('#S_forTemps').hide();
				
				$('#btn_initForm').unbind();
				$('#btn_initForm').attr('disabled','disabled');
				
				that._msgDanger('Tipo de Nota/Emissão inválido!','Erro ao carregar dados NF');
				
			}
			
		}
		
	}
	
	showModalNFe () {
		
		var that = this;
		
		var page = 1;
		
		that.buscaNFes(page);
		
		var view = { linhas: that.NFes };
		
		var template = `<table class="table table-condensed table-striped">
							<tr style="background:#283f63; font-size:16px;">
								<th colspan="5" class="solicitacao-icones" style="text-align:center;color:white;">NFe</th>
							</tr>
							
							<tr style="background:azure; font-size:14px">
	    						<th style="background:azure;text-align: center;"><p style="height:12px;">Nº</p></th>
	    						<th style="background:azure;text-align: center;"><p style="height:12px;">Emissor</p></th>
	    						<th style="background:azure;text-align: center;"><p style="height:12px;">Data Emissão</p></th>
	    						<th style="background:azure;text-align: center;"><p style="height:12px;">Valor</p></th>
	    						<th style="background:azure;text-align: center;"><p style="height:12px;">Situação</p></th>
	    					</tr>
	    					
	    					{{#linhas}}
	    						<tr class="NFe_tr" index="{{index}}">
									<td style="vertical-align:middle;text-align: center;">{{numero}}</td>
									<td style="vertical-align:middle;text-align: center;">{{razaoSocialEmissor}}</td>
									<td style="vertical-align:middle;text-align: center;">{{dataEmissao}}</td>
									<td style="vertical-align:middle;text-align: center;">{{valorTotal}}</td>
									<td style="vertical-align:middle;text-align: center;">{{situacao}}</td>
								</tr>
	    					{{/linhas}}
						</table>`;
		
		var html = 
			`<div class="row">
				<div class="col-xs-8 col-md-8">
					<input type="text" id="S_temp_ModalFilter" name="S_temp_ModalFilter" class="form-control " placeholder="Buscar Por..."/>
				</div>
			</div>
			<div class="row" style="margin-top: 5px;">
				<div class="col-xs-12 col-md-12 ">
					` + Mustache.render(template,view) + `
				</div>
				<div class="col-xs-12 col-md-12">
					<div style="text-align: center;">
						<a class="modal_prev" style="cursor: pointer;"><i class="fluigicon fluigicon-chevron-left icon-xs"></i></a>
						<span style="font-size: larger;"><b>` + page + `</b></span>
						<a class="modal_next" style="cursor: pointer;"><i class="fluigicon fluigicon-chevron-right icon-xs"></i></a>
					</div>
				</div>
			</div>`;
			
		var myModal = FLUIGC.modal({
			    title: 'Selecione a NFe',
			    content: html,
			    id: 'fluig-modal',
				size: 'full',
			    actions: [{
			        'label': 'Selecione',
			        'autoClose': true
			    }]
			}, function(err, data) {
				
			    if(err) {
			       that._msgDanger('Não foi possivel carregar as informações da NFe','ERRO');
			    } else {
			        
					$('.NFe_tr').css('cursor','pointer');
			
					var bindNfe_tr = function () {
						
						$('.NFe_tr').click(function () {
							
							$('.NFe_tr').removeClass('NFe_tr_selected');
							$(this).addClass('NFe_tr_selected');
							$('.modal-footer').find('button').removeAttr('disabled');
							
						});
					}
					
					bindNfe_tr();
					
					$('.modal-footer').find('button').attr('disabled','disabled');
					
					$('.modal-footer').find('button').click(function () {
						
						that.NFe = that.NFes[$('.NFe_tr_selected').attr('index')];
						
						var filial = that.filiais.find(x => x.LojaID == $('#S_sel_filial').val());
						
						var groups = that._getData('dsBARIGUI_lojasIntranet',null);
						;
						that.NFe.empresa.nomeFantasia = groups.find(x => x.LojaID == filial.LojaID && x.Status == "A").Nome;
						that.NFe.empresa.cnpj = filial.CNPJ;
						
						$('#S_temp_ModalFilter').attr('disabled','disabled');
						$('.modal_prev').attr('disabled','disabled');
						$('.modal_next').attr('disabled','disabled');
						$(this).attr('disabled','disabled');
						$(this).html('Loading...');
						
						that.showDadosNota();
						
						$('#S_temp_ModalFilter').removeAttr('disabled');
						$('.modal_prev').removeAttr('disabled');
						$('.modal_next').removeAttr('disabled');
						$(this).removeAttr('disabled');
						$(this).html('Selecione');
						
					})
			
					$('#S_temp_ModalFilter').keyup(function () {
						
						var func = async function () {
							
							await that.buscaNFes(page);
							view = await { linhas: that.NFes };
							await $('#fluig-modal').find('table').closest('div').html(Mustache.render(template,view));
							$('.modal-footer').find('button').attr('disabled','disabled');
							bindNfe_tr();
							
						}
						
						func();
						
					})
					
					$('.modal_prev').click(function() {
						
						if ((page - 1) > 0) {
							page--;
							that.buscaNFes(page);
							view = { linhas: that.NFes };
							$('#fluig-modal').find('table').closest('div').html(Mustache.render(template,view));
							$(this).closest('div').find('span').html(page);
							$('.modal-footer').find('button').attr('disabled','disabled');
							bindNfe_tr();
						}
						
					});
					
					$('.modal_next').click(function() {
						
						page++;
						that.buscaNFes(page);
						view = { linhas: that.NFes };
						$('#fluig-modal').find('table').closest('div').html(Mustache.render(template,view));
						$(this).closest('div').find('span').html(page);
						$('.modal-footer').find('button').attr('disabled','disabled');
						bindNfe_tr();
						
					});
			
			    }
			});
		
	}
	
	buscaNFes (page = 1) {
		
		var that = this;
		
		var filial = that.filiais.find(x => x.LojaID == $('#S_sel_filial').val());
		
		var pesquisar = $('#S_temp_ModalFilter').val();
		
		var constraints = [];
		constraints.push(DatasetFactory.createConstraint('filial',filial.CNPJ,filial.CNPJ,ConstraintType.MUST));
		constraints.push(DatasetFactory.createConstraint('pagina',page,page,ConstraintType.MUST));
		
		if (pesquisar && pesquisar != undefined && pesquisar != null && pesquisar != "") {
			
			constraints.push(DatasetFactory.createConstraint('pesquisa',pesquisar,pesquisar,ConstraintType.MUST));
			
		}
		
		var data = that._getData('dsBARIGUI_ConsultaNF',constraints);
		
		var paraObj = [];
		
		for (var i in data) {
			
			paraObj.push(JSON.parse(data[i].JSON));
			
		}
		
		that.NFes = paraObj;
		
		//ADD INDEX
		for (var i in that.NFes) {
			
			that.NFes[i].index = i.toString();
			
		}
				
		//CORRIGE DATA
		for (var i in that.NFes) {
			
			var NFe = that.NFes[i];
			
			that.NFes[i].dataEmissao = new Date(NFe.dataEmissao).toLocaleDateString();
			
		}
		
		//CORRIGE CNPJ
		for (var i in that.NFes) {
			
			var NFe = that.NFes[i];
			
			var arr = NFe.cnpjEmissor.split('');
			
			that.NFes[i].cnpjEmissor = [arr[0],'',arr[1],'.' 
										,arr[2],'',arr[3],'',arr[4],'.' 
										,arr[5],'',arr[6],'',arr[7],'/' 
										,arr[8],'',arr[9],'' + arr[10],'',arr[11],'-'
										,arr[12],'',arr[13]].join('');
			
			arr = NFe.empresa.cnpj.split('');
			
			that.NFes[i].empresa.cnpj = [arr[0],'',arr[1],'.' 
										,arr[2],'',arr[3],'',arr[4],'.' 
										,arr[5],'',arr[6],'',arr[7],'/' 
										,arr[8],'',arr[9],'' + arr[10],'',arr[11],'-'
										,arr[12],'',arr[13]].join('');
			
		}
		
		//CORRIGE VALOR
		for (var i in that.NFes) {
			
			var NFe = that.NFes[i];
			
			var val = NFe.valorTotal.toFixed(2).split('.')
			
			if (val.length != 2)
				val[1] = '00';
			
			var txt = ',' + val[1];
			
			var aux = val[0].split('');
			var j = (aux.length - 1);
			
			var count = 0;
			
			do {
				
				count++;
				
				if (count == 4) {
					
					txt = '.' + txt;
					count = 0;
				}
				
				txt = aux[j] + '' + txt;
				
				j--;
				
			} while (j >= 0);
			
			that.NFes[i].valorTotal = txt;
			
		}
		
	}
	
	showDadosNota () {
		
		var that = this;
		
		if (that.validaNota()) {
			
			window.parent.$('#workflowActions').show();
			
			window.parent.$('button').each(function () {
				
				var attr = $(this).attr('data-send');
				
				if (typeof attr !== typeof undefined && attr !== false) {
					  
					$(this).closest('div').find('button').removeAttr('disabled');
				
				}
				
			})
			
			$(".S0-dadosClassNF").hide();
			
			$('#S0').show();
			
			$('.lista-anexos').closest('section').show();
			
			$('.S0_forComAF').hide();
			
			$('#S0_hid_SemAF').val('true');
			
			$('#S0_marca').val($('#S_sel_marca').find(':selected').html());
			$('#S0_filial').val(that.NFe.empresa.nomeFantasia);
			$('#S0_CNPJ_filial').val(that.NFe.empresa.cnpj.replace(/^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/, "$1.$2.$3/$4-$5"));
			
			$('#S0_numnota').val(that.NFe.numero.toString());
			$('#S0_serie').val(that.NFe.serie.toString());
			$('#S0_fornecedor').val(that.NFe.razaoSocialEmissor);
			$('#S0_tipo_CNPJCPF').val(($('#S0_hid_isNFe').val() == 'true'? 'CNPJ' : $('#S_temp_tipo').val()));
			$('#S0_CNPJCPF').val(that.NFe.cnpjEmissor);
		
			$('#S0_valor').val(that.NFe.valorTotal);
			$('#S0_dat_emissao').val(that.NFe.dataEmissao);
			
			$('#S').hide();
			
			$('#S0_marca').attr('readonly','');
			$('#S0_filial').attr('readonly','');
			$('#S0_CNPJ_filial').attr('readonly','');
			
			$('#S0_numnota').attr('readonly','');
			$('#S0_serie').attr('readonly','');
			$('#S0_valor').attr('readonly','');
			$('#S0_dat_emissao').attr('readonly','');
			
			$('#S0_fornecedor').attr('readonly','');
			$('#S0_tipo_CNPJCPF').attr('readonly','');
			$('#S0_CNPJCPF').attr('readonly','');
			
			$('#S0_numParcelas').attr('readonly','');
			
			$('#S0_2').hide();
			
			if ($('#S0_hid_isNFe').val() == undefined || $('#S0_hid_isNFe').val() == null || $('#S0_hid_isNFe').val() == 'false') {
				
				$('#S0_valor').removeAttr('readonly');
				$('#S0_dat_emissao').removeAttr('readonly');
				$('#S0_tipo').removeAttr('readonly');
				$('#S0_custo').removeAttr('readonly');
				$('#S0_despesa').removeAttr('readonly');
				$('#S0_tpdoc').removeAttr('readonly');
				
				FLUIGC.calendar('#S0_dat_emissao', {
				    pickDate: true,
				    pickTime: false
				});
				
				var str2date = function(str) {
					if(str == null || str == '')
						return null;
					var partes = str.split(' ');
					
					partes[1] = '23:59:59.999';
					
					var arrdata = partes[0].split('/').reverse();
					var arrhora = partes[1].split(':');
					
					var rtn = new Date(arrdata[0], arrdata[1], arrdata[2], arrhora[0], arrhora[1], arrhora[2]);
					rtn.setMonth(rtn.getMonth() - 1);
					rtn.setHours(rtn.getHours() - 3);

					return rtn;
					
				}
				
				$('#S0_dat_emissao').on('change',function(e) {
					var dt = str2date($(this).val());
					var hj = new Date();
					
					hj.setDate(hj.getDate() + 1);
					
					$(this).css('border','1px solid #ccc');
					
					if (hj < dt) {
						$(this).val(null);
						$(this).css('border','1px solid red');
					}
					
					if ($('#S0_dat_emissao').val !='')  {
						that._msgWarning('Por favor confirme a Data de Emissão!!');
						return false;
					}
					
					
				})
				
				window.UTILS.bindNumParcelas();
				
			} else {
				
				$('#S0_link_verPacelas').attr('disabled','disabled');
				
				$('#S0_forNFe').show();
				
				var func = function (formato,idInput) {
					
					var chaves 	= 'key=' + that.NFe.chave;
					var mime 	= null;
			    	var encode 	= false;
			    	var url		= '';
			    	var name	= '';
			    	var tipo	= idInput;
			    	
			    	switch(formato) {
			    	case 'pdf':
			    		mime = 'application/pdf';
			    		encode = true;
			    		url = chaves + '&format=' + formato + '&download=false&encode=' + (encode?'true':'false');
			    		name = 'NFe.pdf';
			    		break
			    	case 'xml':
			    		mime = 'text/html';
			    		url = chaves + '&format=' + formato + '&download=true&encode=' + (encode?'true':'false');
			    		name = 'XML_nota.xml';
			    		break
			    	}
					
					var constraint = [DatasetFactory.createConstraint('url',url,url,ConstraintType.MUST)];
					var data = that._getData('dsBARIGUI_DOC_CEF',constraint);
					
					var str = [];
					
					for (var i in data) {
						
						str.push(data[i].BLOB);
						
					}
					
					var blob = encode? window.ANEXOS.base64toBlob(str.join(''),mime) : new Blob([str.join('')], {type: mime});
					
					if (!encode) {
						
						func_xml(blob);
						
						var form = new FormData();
						form.append("blob", blob, 'XML_nota.xml');
						
						$.ajax({
			                async : false,
			                type : 'POST',
			                processData: false,
			                contentType: false,
			                url : '/ecm/upload',
			        		data: form,
			        		error: function(e) {
			        			console.error('error on load file')
			        		},
			        		success: function(data) {
			        			
			        			$('#S0_link_xml').val(that.NFe.chave);
			        			$('#S0_xml').attr('href',URL.createObjectURL(blob));
			        			$('#S0_xml').attr('chave',that.NFe.chave);
		        				$('#S0_xml').attr('download','xml_nota_' + $('#S0_numnota').val() + '.xml');
		        				
			        		}
			        		
			        	});
						
					} else {
					
						var form = new FormData();
						form.append("blob", blob, name);
						
			            $.ajax({
			                async : true,
			                type : 'POST',
			                processData: false,
			                contentType: false,
			                url : '/ecm/upload',
			        		data: form,
			        		error: function() {
			        			
			        		},
			        		success: function(data) {
			        			
			        			var f = JSON.parse(data).files[0]
				    			
				    			var doc = {
										key: tipo,
										value: {
											link: URL.createObjectURL(blob),
											thumb: '',
											thumbName: '',
											name: f.name,
											createDate: new Date()
										}
									}
				    			
				    			var value = DatasetFactory.getDataset(
					        					'dsGeraThumbnail',
					        					null,
					        					[DatasetFactory.createConstraint('fileName',f.name,f.name,ConstraintType.MUST)],
					        					null
					        		    	).values[0];
				    			
				    			doc.value.name = value.fileName;
				    			
				    			if (value.resultado == 'OK') {
				    				doc.value.thumbName = value.thumbName;
				    			}
				    			
				    			window.ANEXOS.uploads.push(doc);
				    			that.contaDocumentosAnexados();
			        			
			        		}
			        	});
			            
					}
					
				}
				
				func('pdf','S0_nota');
				
				var func_xml = function (xmlString) {
					
					var parser = new DOMParser();
					
					var dt = parser.parseFromString(xmlString, "text/xml");
					
					var cobr = dt.getElementsByTagName("cobr")[0]
					
					if (cobr != undefined && cobr != null) {
						
						var dupList = dt.getElementsByTagName("dup");
						
						if (dupList != undefined && dupList != null) {
							
							$('#S0_numParcelas').val(dupList.length);
							
							var list = [];
							
							for (var i = 0; i < dupList.length; i++) {
								
								var val = dupList[i].childNodes;
								
								var aux = val[1].innerHTML.split('-');
								var data = aux[2] + '/' + aux[1] + '/' + aux[0]
								
								var obj = {
										
										numero: val[0].innerHTML,
										data_vencimento: data,
										valor: val[2].innerHTML.replace('.',',')
										
								}
								
								list.push(obj);
								
							}
							
							
							$('#hid_parcelas').val(JSON.stringify(list));
							$('#S0_dtvencimento').val(list[0].data_vencimento);
							
						}
						
					}
				
					$('#S0_link_verPacelas').removeAttr('disabled');
					window.UTILS.bindNumParcelas();
					
				}
				
				func('xml','S0_xml');
				
				window.UTILS.bindNumParcelas();
				
			}
			
			/*$('.anexo').attr('disabled','disabled');
			$('.anexo').css('cursor','wait');
			
			that._criaEstruturaDocumentos();*/
			
			$('#S0_complemento').closest('div').hide();
			$('#S0_departamento').closest('div').hide();
			$('#S0_aprovador').closest('div').hide();
			
			that.bindAtv0();
		
		}
		
	}
	
	bindAtv0 () {
		
		var that = this;
		
		that.bindTipo();
		
		window.UTILS.configuraS0();
		
	}
	
	bindTipo () {
		
		var that = this;
		
		$('#S0_tipo').html(`<option value="">==SELECIONE==</option>
				<option value="CUSTO">CUSTO</option>
				<option value="DESPESA">DESPESA</option>`);
		
		$('#S0_tipo').change(function() {
			
			window.FORMULARIO._startLoading();
			
			that.changeTipo();
			
			window.FORMULARIO._stopLoading();
			
		})
		
	}
	
	bindComplemento () {
		
		var that = this;
		
		var div = $('#S0_complemento').closest('div');
		$('#S0_complemento').remove();
		
		$(div).hide();
		
		if ($('#S0_tipo').val().toUpperCase() == 'CUSTO') {
			
			$(div).append(`<select id="S0_complemento" name="S0_complemento" class="form-control" obrigatorio="true">
								<option value="">==SELECIONE==</option>
								<option value="OFICINA">OFICINA</option>
								<option value="SEMINOVOS">SEMINOVOS</option>
							</select>`);
			
		} else if ($('#S0_tipo').val().toUpperCase() == 'DESPESA') {
			
			var constraints = [DatasetFactory.createConstraint('workflowColleagueRolePK.colleagueId',that.responsavel.id,that.responsavel.id,ConstraintType.MUST)];
			var roles = that._getData('workflowColleagueRole',constraints);
			
			if (roles.find(x => x['workflowColleagueRolePK.roleId'] == 'CEF_tipo_NF_extraordinaria') != undefined) {
				
				$(div).append(`<select id="S0_complemento" name="S0_complemento" class="form-control" obrigatorio="true">
						<option value="">==SELECIONE==</option>
						<option value="EXTRAORDINÁRIA">EXTRAORDINÁRIA</option>
						<option value="MARKETING">MARKETING</option>
						<option value="NOTA FISCAL SEM PAGAMENTO">NOTA FISCAL SEM PAGAMENTO</option>
						<option value="NOVO VAREJO">NOVO VAREJO</option>
						<option value="TECNOLOGIA DA INFORMAÇÃO">TECNOLOGIA DA INFORMAÇÃO</option>
					</select>`);
				
			} else {
				
				$(div).append(`<select id="S0_complemento" name="S0_complemento" class="form-control" obrigatorio="true">
						<option value="">==SELECIONE==</option>
						<option value="MARKETING">MARKETING</option>
						<option value="NOTA FISCAL SEM PAGAMENTO">NOTA FISCAL SEM PAGAMENTO</option>
						<option value="NOVO VAREJO">NOVO VAREJO</option>
						<option value="TECNOLOGIA DA INFORMAÇÃO">TECNOLOGIA DA INFORMAÇÃO</option>
					</select>`);
				
			}
			
			
			
		} else {
			
			$(div).append(`<select id="S0_complemento" name="S0_complemento" class="form-control" obrigatorio="true">
								<option value="">==SELECIONE==</option>
							</select>`);
			
		}
		
		$(div).show();
		
		$('#S0_complemento').change(function () {
			
			that.changeComplemento();
			
		})
		
	}
	
	bindDepartamento () {
		
		var that = this;
		
		var div = $('#S0_departamento').closest('div');
		$('#S0_departamento').remove();
		
		var linhas = [];
		var constraint = [];
		var constraint1 = [];
		
		if ($('#S0_complemento').val().toUpperCase() == 'OFICINA') {
			
			constraint.push(DatasetFactory.createConstraint('nome_departamento','41 OFICINA P.VENDAS(LOJA)','41 OFICINA P.VENDAS(LOJA)',ConstraintType.SHOULD));
			constraint.push(DatasetFactory.createConstraint('nome_departamento','49 FUN/PINT-P.VENDAS(LOJA)','49 FUN/PINT-P.VENDAS(LOJA)',ConstraintType.SHOULD));
		
		} else if ($('#S0_complemento').val().toUpperCase() == 'SEMINOVOS') {
			
			constraint.push(DatasetFactory.createConstraint('nome_departamento','10 S.NOVOS-VENDAS(LOJA)','10 S.NOVOS-VENDAS(LOJA)',ConstraintType.SHOULD));
			constraint.push(DatasetFactory.createConstraint('nome_departamento','13 REVISÃO CENTRAL S.NOVOS','13 REVISÃO CENTRAL S.NOVOS',ConstraintType.SHOULD));
		
		} else if ($('#S0_complemento').val().toUpperCase() == 'NOVO VAREJO') {

			constraint.push(DatasetFactory.createConstraint('nome_departamento','30 PEÇAS-P.VENDAS','30 PEÇAS-P.VENDAS',ConstraintType.SHOULD));
			constraint.push(DatasetFactory.createConstraint('nome_departamento','77 DESENVOLVIMENTO/TECNOLOGIA','77 DESENVOLVIMENTO/TECNOLOGIA',ConstraintType.SHOULD));
			constraint.push(DatasetFactory.createConstraint('nome_departamento','NOVO VAREJO - GERAL','NOVO VAREJO - GERAL',ConstraintType.SHOULD));
			constraint.push(DatasetFactory.createConstraint('nome_departamento','NOVO VAREJO - PLATAFORMA COMMERCE','NOVO VAREJO - PLATAFORMA COMMERCE',ConstraintType.SHOULD));
			constraint.push(DatasetFactory.createConstraint('nome_departamento','NOVO VAREJO - PLATAFORMA HUB','NOVO VAREJO - PLATAFORMA HUB',ConstraintType.SHOULD));
			constraint.push(DatasetFactory.createConstraint('nome_departamento','NOVO VAREJO - PLATAFORMA ICARROS','NOVO VAREJO - PLATAFORMA ICARROS',ConstraintType.SHOULD));

		}	
		;
		var filial = that._getData('dsBARIGUI_lojasIntranet',null).find(x => x.Nome == $('#S0_filial').val());
		
		var constraints = [];
		constraints.push(DatasetFactory.createConstraint('cnpj_filial',filial.CNPJ,filial.CNPJ,ConstraintType.MUST));
		
		if ($('#S0_complemento').val().toUpperCase() == 'NOVO VAREJO') {
			var departamentos = that._getData('dsBARIGUI_aprovadoresNovoVarejo',constraint);
		}
		
		else {
			var departamentos = that._getData('dsTELEMETRIX_view_Fluig_Aprovadores',constraint);
		}

		departamentos.sort(function (a, b) {
			
			if (a.nome_departamento < b.nome_departamento) {
				return -1 
			}
			
			if (b.nome_departamento < a.nome_departamento) {
				return 1
			}
			
			return 0;
			
		}).forEach(function (departamento) { 
			if (linhas.length == 0 || linhas.map(x => x.nome_departamento).indexOf(departamento.nome_departamento) == -1) 
			{ 
				linhas.push( { nome_departamento: departamento.nome_departamento.toUpperCase() } )
			} 
		});
		
		var template = `<select id="S0_departamento" name="S0_departamento" class="form-control" obrigatorio="true">
							<option value="">==SELECIONE==</option>
							{{#linhas}}
								<option value="{{nome_departamento}}">{{nome_departamento}}</option>
							{{/linhas}}
						</select>`

		var view = { linhas }
		
		$(div).append(Mustache.render(template,view));
		$(div).show();
		
		$('#S0_departamento').change(function () {
			
			that.changeDepartamento();
			
		})
		
	}
	
	bindContagerencial () {
		
		var that = this;
		var div = $('#S0_contagerencial').closest('div');
		$('#S0_contagerencial').remove();
		
		var filial = that._getData('dsBARIGUI_lojasIntranet',null).find(x => x.CNPJ == $('#S0_CNPJ_filial').val().replace(/[^a-zA-Z0-9]/g,'') && x.Status == 'A');
		
		var contas = that._getData('dsBARIGUI_contaGerencialPorTipoNF',[]).filter(x => x.ErpID == filial.ErpID && x.tipoNF.toUpperCase() == $('#S0_complemento').val().toUpperCase())
									.map(function (v) { return {value: v.Conta.toUpperCase()} });
		
		
		
		function removeDuplicates(originalArray, prop) {
            var newArray = [];
            var lookupObject  = {};

            for(var i in originalArray) {
               lookupObject[originalArray[i][prop]] = originalArray[i];
            }

            for(i in lookupObject) {
                newArray.push(lookupObject[i]);
            }
             return newArray;
        }

        contas = removeDuplicates(contas, "value");
        
		contas.sort(function (a, b) {
			
			if (a.value < b.value) {
				return -1 
			}
			
			if (b.value < a.value) {
				return 1
			}
			
			return 0;
			
		})
		
		var view = { linhas: contas };
		
		var template = `<select id="S0_contagerencial" name="S0_contagerencial" class="form-control" obrigatorio="true">
							<option value="">==SELECIONE==</option>
							{{#linhas}}
								<option value="{{value}}">{{value}}</option>
							{{/linhas}}
						</select>`;
		
		$(div).append(Mustache.render(template,view));
		
	}
	
	bindCondicaoPagamento () {
		
		var that = this;
		
		var div = $('#S0_condicaoPagamento').closest('div');
		$('#S0_condicaoPagamento').remove();
		
		var ds_condPagamento = [
				{CODIGO:'1',NOME:'Acerto pelo Caixa',STATUS:'A'}
				,{CODIGO:'2',NOME:'Adiantamento',STATUS:'A'}
				,{CODIGO:'3',NOME:'Boleto',STATUS:'A'}
				,{CODIGO:'4',NOME:'Débito Automático',STATUS:'A'}
				,{CODIGO:'5',NOME:'Depósito',STATUS:'A'}
				,{CODIGO:'6',NOME:'Encontro de Contas',STATUS:'A'}
				,{CODIGO:'7',NOME:'Pix',STATUS:'A'}
		    ]
		
		var condPagamento = ds_condPagamento.map(function (v) { return {value: v.NOME.toUpperCase()} });
		
		condPagamento.sort(function(a,b){
			
			if (a.NOME > b.NOME)
				return 1
			if (a.NOME < b.NOME)
				return -1
				
			return 0
			
		})
		
		var view = { linhas: condPagamento };
		
		var template = `<select id="S0_condicaoPagamento" name="S0_condicaoPagamento" class="form-control" obrigatorio="true">
							<option value="">==SELECIONE==</option>
							{{#linhas}}
								<option value="{{value}}">{{value}}</option>
							{{/linhas}}
						</select>`;
		
		$(div).append(Mustache.render(template,view));
		$(div).show();
		
		$('#S0_condicaoPagamento').change(function () {
			
			that.changeCondicaoPagamento();
			
		})
		
	}
	
	bindDeposito_Banco () {
		
		var that = this;
		
		var bancos = that._getData('dsCadastroBancos',[]);
		
		var div = $('#S0_deposito_banco').closest('div');
		$('#S0_deposito_banco').remove();
		
		bancos.sort(function(a,b){
			
			if (a.Banco_Sigla > b.Banco_Sigla)
				return 1
			if (a.Banco_Sigla < b.Banco_Sigla)
				return -1
				
			return 0
			
		})
		
		var view = { linhas: bancos.map(function (x) { return { id: x['Banco_Sigla'].toUpperCase() + " - " + x['Banco_Descricao'].toUpperCase(), nome: x['Banco_Sigla'].toUpperCase() + " - " + x['Banco_Descricao'].toUpperCase() } }) }
		
		var template = `<select id="S0_deposito_banco" name="S0_deposito_banco" class="form-control">
							<option value="">==SELECIONE==</option>
							{{#linhas}}
								<option value="{{id}}">{{nome}}</option>
							{{/linhas}}
						</select>`;
		
		
		
		$(div).append(Mustache.render(template,view));
		
	}
	
	bindAprovador () {
		var that = this;
		
		$('#S0_temp_forTempAprovador').html('');
		$('#S0_aprovador').closest('div').hide();
		
		if ($('#S0_complemento').val().toUpperCase() == 'MARKETING') {
			
			var constraints_group = [];
			constraints_group.push(DatasetFactory.createConstraint('colleagueGroupPK.groupId','aprov_CEF_mktg','aprov_CEF_mktg',ConstraintType.MUST));
			var group = that._getData('colleagueGroup',constraints_group);
			
			var constraints = [];
			
			for (var i in group) {
				
				var user = group[i];
				
				constraints.push(DatasetFactory.createConstraint('colleaguePK.colleagueId',user['colleagueGroupPK.colleagueId'],user['colleagueGroupPK.colleagueId'],ConstraintType.SHOULD));
				
			}
	
		} else if ($('#S0_complemento').val().toUpperCase() == 'NOVO VAREJO') {
			
			var constraints_group = [];
			constraints_group.push(DatasetFactory.createConstraint('colleagueGroupPK.groupId','aprov_CEF_NovoVarejo','aprov_CEF_NovoVarejo',ConstraintType.MUST));
			var group = that._getData('colleagueGroup',constraints_group);
			
			var constraints = [];
			
			for (var i in group) {
				
				var user = group[i];
				
				constraints.push(DatasetFactory.createConstraint('colleaguePK.colleagueId',user['colleagueGroupPK.colleagueId'],user['colleagueGroupPK.colleagueId'],ConstraintType.SHOULD));
				
			}
		} else {
			
			var constraints = [];
			constraints.push(DatasetFactory.createConstraint('nome_departamento',$('#S0_departamento').val(),$('#S0_departamento').val(),ConstraintType.MUST));
			
			var filial = that._getData('dsBARIGUI_lojasIntranet',null).find(x => x.Nome.toUpperCase() == $('#S0_filial').val().toUpperCase());
			
			constraints.push(DatasetFactory.createConstraint('cnpj_filial',filial.CNPJ,filial.CNPJ,ConstraintType.MUST));
			
			var aprovadores = that._getData('dsTELEMETRIX_view_Fluig_Aprovadores',constraints);
			
			constraints = [];//[DatasetFactory.createConstraint('login','','',ConstraintType.SHOULD)];
			aprovadores.forEach(function (ap) { constraints.push(DatasetFactory.createConstraint('login',ap.login_usuario,ap.login_usuario,ConstraintType.SHOULD)); });
			
			//var constraints = [];
			/*constraints.push(DatasetFactory.createConstraint('login','andref','andref',ConstraintType.SHOULD));
			constraints.push(DatasetFactory.createConstraint('login','edersonr','edersonr',ConstraintType.SHOULD));
			constraints.push(DatasetFactory.createConstraint('login','heloisahf','heloisahf',ConstraintType.SHOULD));
			constraints.push(DatasetFactory.createConstraint('login','yonaraa','yonaraa',ConstraintType.SHOULD));
			constraints.push(DatasetFactory.createConstraint('login','nazirt','nazirt',ConstraintType.SHOULD));*/
			
		}
		
		var users = that._getData('colleague',constraints);
		
		users.sort(function(a, b){
			
			if (a.colleagueName > b.colleagueName)
				return 1
			
			if (a.colleagueName < b.colleagueName)
				return -1
			
			return 0
			
		})
		
		var view = { linhas: users.map(function (x) { return { id: x['colleaguePK.colleagueId'], nome: x.colleagueName.toUpperCase() } }) }
		
		var template = `<label for="S0_temp_aprovador">Aprovador</label>
						<select id="S0_temp_aprovador" name="S0_temp_aprovador" class="form-control">
							<option value="">==SELECIONE==</option>
							{{#linhas}}
								<option value="{{id}}">{{nome}}</option>
							{{/linhas}}
						</select>`;
		
		$('#S0_temp_forTempAprovador').append(Mustache.render(template,view));
		$('#S0_temp_forTempAprovador').show();
		
		$('#S0_temp_aprovador').change(function() {
			
			that.changeAprovador();
			
		})
		
	}
	
	changeDepartamento () {
		
		$('#S0_loginaprovador').val('');
		$('#S0_emailaprovador').val('');
		$('#S0_aprovador').val('');
		$('#S0_aprovador_matricula').val('');
		
		this.bindAprovador();
		
	}
	
	changeComplemento () {
		
		window.FORMULARIO._startLoading();
		
		var that = this;
		
		var func = async function () {
			
			if ($('#S0_complemento').val().toUpperCase() != 'NOTA FISCAL SEM PAGAMENTO') {
				
				that.bindCondicaoPagamento();
			
			}
			
			that.bindContagerencial();
			that.bindDepartamento();
			await window.UTILS.configuraS0();
			window.FORMULARIO._stopLoading();
			
		}
		
		func();
		
	}
	
	changeCondicaoPagamento () {
		
		var that = this;
		
		$('.S0-dadosDeposito').hide();
		$('.S0-dadosPix').hide();
		
		if ($('#S0_condicaoPagamento').val() == 'DEPÓSITO') {
			
			$('.S0-dadosDeposito').show();
			
			that.bindDeposito_Banco();
			
			$('#S0_deposito_CPF_CNPJ').change(function () {
				
				$('#S0_deposito_CPF_CNPJ').removeClass('error');
				
				if ($('#S0_deposito_tipo').val() == 'CPF') {
					
					if (!TestaCPF($('#S0_deposito_CPF_CNPJ').val())) {
						
						$('#S0_deposito_CPF_CNPJ').addClass('error');
						$('#S0_deposito_CPF_CNPJ').val('');
						$('#S0_deposito_nome').val('');
						
					} else {
						
						var constraint = [];
						
						var val = $('#S0_deposito_CPF_CNPJ').val().split('.').join('').split('-').join('');
						
						constraint.push(DatasetFactory.createConstraint('Pessoa_DocIdentificador',val,val,ConstraintType.MUST));
						constraint.push(DatasetFactory.createConstraint('lojaId',$('#S_sel_filial').val(),$('#S_sel_filial').val(),ConstraintType.MUST));
						var pessoa = that._getData('dsBARIGUI_fornecedoresAPI',constraint);
						
						if (!pessoa || pessoa == undefined || pessoa == null || pessoa == '' || pessoa.length == 0) {
							
							$('#S0_deposito_CPF_CNPJ').addClass('error');
							$('#S0_deposito_CPF_CNPJ').val('');
							$('#S0_deposito_nome').val('');
							
						} else {
							
							$('#S0_deposito_nome').val(pessoa[0].Pessoa_Nome);
							
						}
						
					}
					
				} else if ($('#S0_deposito_tipo').val() == 'CNPJ') {
					
					if (!TestaCNPJ($('#S0_deposito_CPF_CNPJ').val())) {
						
						$('#S0_deposito_CPF_CNPJ').addClass('error');
						$('#S0_deposito_CPF_CNPJ').val('');
						$('#S0_deposito_nome').val('');
						
					} else {
						
						var constraint = [];
						
						var val = $('#S0_deposito_CPF_CNPJ').val().split('.').join('').split('-').join('').split('/').join('');
						
						constraint.push(DatasetFactory.createConstraint('Pessoa_DocIdentificador',val,val,ConstraintType.MUST));
						constraint.push(DatasetFactory.createConstraint('lojaId',$('#S_sel_filial').val(),$('#S_sel_filial').val(),ConstraintType.MUST));
						var pessoa = that._getData('dsBARIGUI_fornecedoresAPI',constraint);
						
						if (!pessoa || pessoa == undefined || pessoa == null || pessoa == '' || pessoa.length == 0) {
							
							$('#S0_deposito_CPF_CNPJ').addClass('error');
							$('#S0_deposito_CPF_CNPJ').val('');
							$('#S0_deposito_nome').val('');
							
						} else {
							
							$('#S0_deposito_nome').val(pessoa[0].Pessoa_Nome);
							
						}
						
					}
					
				} else {
					$('#S0_deposito_nome').val('');
					$('#S0_deposito_CPF_CNPJ').val('');
					$('#S0_deposito_CPF_CNPJ').attr('disabled','disabled');
				}
				
			})
			
			$('#S0_deposito_tipo').unbind();
			$('#S0_deposito_tipo').change(function () {
				
				if ($(this).val() == 'CPF') {
					$('#S0_deposito_CPF_CNPJ').mask('000.000.000-00');
				} else if ($(this).val() == 'CNPJ') {
					$('#S0_deposito_CPF_CNPJ').mask('00.000.000/0000-00');
				} else {
					$('#S0_deposito_CPF_CNPJ').attr('disabled','disabled');
				}
				
			});
			
			$('#S0_deposito_CPF_CNPJ').mask('00.000.000/0000-00');
			
		} else if ($('#S0_condicaoPagamento').val().toUpperCase() == 'PIX') {
			
			$('.S0-dadosPix').show();
			
			var div = $('#S0_pix_tipo_chave').closest('div');
			var val = $('#S0_pix_tipo_chave').val();
			$('#S0_pix_tipo_chave').remove();
			
			var view = { linhas: [ 
				{ value: 'Chave Aleatoria'.toUpperCase() },
				{ value: 'e-mail'.toUpperCase() },
				{ value: 'CNPJ'.toUpperCase() },
				{ value: 'CPF'.toUpperCase() },
				{ value: 'Telefone'.toUpperCase() }
			] };
			
			var template = `<select id="S0_pix_tipo_chave" name="S0_pix_tipo_chave" class="form-control" obrigatorio="true">
								<option value="">==SELECIONE==</option>
								{{#linhas}}
									<option value="{{value}}">{{value}}</option>
								{{/linhas}}
							</select>`;
			
			$(div).append(Mustache.render(template,view));
			$('#S0_pix_tipo_chave').val(val);
			
			$('#S0_pix_tipo_chave').change(function () {
				
				var v = $(this).val()
				
				$('#S0_pix_chave').val('')
				
				if (v == 'CPF') {
					
					$('#S0_pix_chave').mask("000.000.000-00", {reverse: true});
					
				} else if (v == 'CNPJ') {
					
					$('#S0_pix_chave').mask("00.000.000/0000-00", {reverse: true});
					
				} else if (v == 'Telefone') {
					
					$('#S0_pix_chave').mask("000000000000");
					
				} else if (v == 'e-mail') {
					
					$('#S0_pix_chave').unmask();
					
				} else if (v == 'Chave Aleatoria') {
					
					$('#S0_pix_chave').unmask();
					
				}
				
			})
			
			$('#S0_pix_tipo_chave').trigger('change');
			
		}
		
	}
	
	changeAprovador () {
		
		var that = this;
		
		var login = $('#S0_temp_aprovador').val();
		var nome = $('#S0_temp_aprovador').find(':selected').html();
		
		$('#S0_aprovador').val(nome);
		$('#S0_aprovador_matricula').val(login);
		
		var constraints = [];
		constraints.push(DatasetFactory.createConstraint('colleaguePK.colleagueId',login,login,ConstraintType.MUST));
		var user = that._getData('colleague',constraints);
		
		$('#S0_loginaprovador').val(user[0].login);
		$('#S0_emailaprovador').val(user[0].mail);
		
	}
	
	changeTipo () {
		
		this.bindComplemento();
		window.UTILS.configuraS0();
		
	}
	
	validaNota () {
		
		var that = this;
		var constraints = [];
		constraints.push(DatasetFactory.createConstraint('S0_numnota',that.NFe.numero,that.NFe.numero,ConstraintType.MUST));
		constraints.push(DatasetFactory.createConstraint('S0_serie',that.NFe.serie,that.NFe.serie,ConstraintType.MUST));
		constraints.push(DatasetFactory.createConstraint('S0_CNPJCPF',that.NFe.cnpjEmissor,that.NFe.cnpjEmissor,ConstraintType.MUST));
		constraints.push(DatasetFactory.createConstraint('S_sel_tiponota',that.NFe.tipoNota,that.NFe.tipoNota,ConstraintType.MUST));
		var process = that._getData('BARIGUI_COMPRAS',constraints);
		
		if (!process || process.length == 0) {
			
			return true;
			
		}
		
		var constraintsProcess = [];
		for (var index in process) {
			
			var id = process[index]['metadata#id'];
			
			constraintsProcess.push(DatasetFactory.createConstraint('cardDocumentId',id,id,ConstraintType.SHOULD));
			
		}
		
		constraintsProcess.push(DatasetFactory.createConstraint('processId','BARIGUI_COMPRAS','BARIGUI_COMPRAS',ConstraintType.MUST));
		var hasProcess = that._getData('workflowProcess',constraintsProcess);
		
		if (hasProcess && hasProcess.find(x => x.status == 0 || x.status == 2) != undefined) {
			
			var p = hasProcess.find(x => x.status != 1);
			that._msgDanger('A Nota selecionada já esta com um processo (' + p['workflowProcessPK.processInstanceId'] + ') em andamento','ERRO');
			return false;
			
		}
		
		return true;
		
	}
	
	_sectionValidate () {
		
		var that = this;
		
		if ($('#S0:visible').length == 0 || $('#S:visible').length > 0) {
			
			this._msgDanger('Verifique se a Nota esta disponível clicando no "Iniciar" antes de continuar');
			return false;
			
		}
		
		if ($('#S0_condicaoPagamento').val() == 'DEPÓSITO') {
			
			$('.S0-dadosDeposito').find('input').each(function () {
				
				if ($(this).val() == undefined || $(this).val() == null || $(this).val() == '') {
					
					$(this).addClass('error');
					
				}
				
			})
			
			$('.S0-dadosDeposito').find('select').each(function () {
				
				if ($(this).val() == undefined || $(this).val() == null || $(this).val() == '') {
					
					$(this).addClass('error');
					
				}
				
			})
			
		}
		
		if ($('.error').length > 0) {
			
			this._msgDanger('Preencha os campos obrigatórios em destaque');
			return false;
			
		}

		if ($('#S0_complemento').val() == 'Oficina') {
			
			if (($('#S0_nraf').val() == '' || $('#S0_nraf').val() == null || $('#S0_nraf').val() == undefined) &&
				($('#S0_numOS').val() == '' || $('#S0_numOS').val() == null || $('#S0_numOS').val() == undefined)) {
				
				$('#S0_nraf').addClass('error');
				$('#S0_numOS').addClass('error');
				that._msgDanger('O número da AF ou OS precisam ser preenchidos');
				return false;
				
			} else {
				
				if ($('#S0_nraf').val() != ''){
					
					if ($('#S0_nraf').val().length < 6) {
						$('#S0_nraf').addClass('error');
						that._msgDanger('O número da AF deve conter no mínimo 6 dígitos');
						return false;
						
					} else if ($('#S0_nraf').val() == 0) {
						$('#S0_nraf').addClass('error');
						that._msgDanger('O número da AF não pode conter somente zeros');
						return false;
						
					} else if (/\d\d(\d)\1{2,3}/.test($('#S0_nraf').val()) == true || $('#S0_nraf').val() == "123456" || $('#S0_nraf').val() == "1234567" || $('#S0_nraf').val() == "12345678"  ) {
						$('#S0_nraf').addClass('error');
						that._msgDanger('O número da AF não pode ser sequencial ou repetir o mesmo número mais de 4 vezes');
						return false;
						
					}
					
				} else if ($('#S0_numOS').val() != '') {
					
					if ($('#S0_numOS').val() == 0) {
						$('#S0_numOS').addClass('error');
						that._msgDanger('O número da OS não pode conter somente zeros');
						return false;
							
					} else if (/\d\d(\d)\1{2,3}/.test($('#S0_numOS').val()) == true || $('#S0_numOS').val() == "12345" || $('#S0_numOS').val() == "1234") {
						$('#S0_numOS').addClass('error');
						that._msgDanger('O número da OS não pode ser sequencial ou repetir o mesmo número mais de 4 vezes');
						return false;
					}
				}
			}
		}
		
		if ($('#S0_tipo').val().toUpperCase() != 'DESPESA' 
			&& ($('#S0_aprovador').val() == undefined || $('#S0_aprovador').val() == null || $('#S0_aprovador').val() == "")) {
			
			$('#S0_aprovador').addClass('error');
			
			that._msgDanger('É obrigatório informar um aprovador');
			return false;
			
		}
		
		if ($('#S0_complemento').val().toUpperCase() != 'NOTA FISCAL SEM PAGAMENTO') {
			if ((!$('#S0_numParcelas').val() || $('#S0_numParcelas').val() == undefined 
					|| $('#S0_numParcelas').val() == null || $('#S0_numParcelas').val() == '')
				|| (!$('#hid_parcelas').val() || $('#hid_parcelas').val() == undefined 
						|| $('#hid_parcelas').val() == null || $('#hid_parcelas').val() == '')) 
			{
				
				this._msgDanger('Informe os dados de pagamento da NF');
				$('#S0_numParcelas').addClass('error');
				$('#S0_link_verPacelas').css('color','red');
				return false;
				
			} else {
				
				var list = JSON.parse($('#hid_parcelas').val())
				
				if (list != undefined && list != null && list != {} && list != []) {
					
					var vl_total = 0;
					
					for (var i in list) {
						
						var obj = list[i];
						
						if (obj.numero == undefined || obj.numero == null || obj.numero == ""
							|| obj.data_vencimento == undefined || obj.data_vencimento == null || obj.data_vencimento == ""
							|| obj.valor == undefined || obj.valor == null || obj.valor == "") 
						{
							
							this._msgDanger('Dados da parcela "' 
									+ ((obj.numero == undefined || obj.numero == null || obj.numero == "")? "XXX" : obj.numero) 
									+ '" Incompletos ou inválidos');
							$('#S0_numParcelas').addClass('error');
							$('#S0_link_verPacelas').css('color','red');
							return false;
							
						} else {
							
							vl_total += parseFloat(obj.valor.replace('R$','').split('.').join('').replace(',','.'));
							
						}
						
					}
					
					var S0_valor = $('#S0_valor').val().replace('R$','').split('.').join('').replace(',','.');
					if (vl_total.toFixed(2) != S0_valor) {
						
						this._msgDanger('O valor das parcelas não esta correto!! verificar valor total da nota');
						$('#S0_numParcelas').addClass('error');
						$('#S0_link_verPacelas').css('color','red');
						return false;
						
					}
					
				} else {
					
					this._msgDanger('Informe os dados de pagamento da NF');
					$('#S0_numParcelas').addClass('error');
					$('#S0_link_verPacelas').css('color','red');
					return false;
					
				}
				
			}
		}
		
		if ($('#S0_complemento').val() == 'NOTA FISCAL SEM PAGAMENTO') {
			
			if ($('#S0_infoNFSP').val() == 'NÃO' && !$("#S0_check_NFSP").prop("checked")){
				this._msgDanger('Confirme no checkbox que está ciente do processo não acionar a atividade do Contas a Pagar');
				return false;

			} if ($('#S0_infoNFSP').val() == 'SIM') {
				this._msgDanger('Esse fluxo é aplicável apenas para notas que efetivamente não possuem pagamento. Favor escolher outro fluxo que seja mais adequado');
				return false;
			}
			
		}
		
		if ($('#S0_dtvencimento').val() != undefined && $('#S0_dtvencimento').val() != null && $('#S0_dtvencimento').val() != "") {
			
			
			var S0_dtvencimento = $('#S0_dtvencimento').val();
			var arr = S0_dtvencimento.split('/');
			var dt = new Date(arr[2] + '-' + arr[1] + '-' + arr[0]);
			var aux = new Date();
			
			aux.setDate(aux.getDate() + 9);

			/* antiga validação de 3 dias úteis 
			 
			if ( aux.getDay() == 3 || aux.getDay() == 6 ) {
				
				aux.setDate(aux.getDate() + 4);
				
			} else if ( aux.getDay() == 4 || aux.getDay() == 5 ) {
				
				aux.setDate(aux.getDate() + 5);
				
			} else {
				
				aux.setDate(aux.getDate() + 3);
				
			} */
			
			if (dt < aux) {
				
				this._msgDanger('Data de Vencimento não permitida, necessário solicitar ao Fornecedor nova data de pagamento e sem acréscimo de juros conforme Política de Compra.');
				$('#S0_dtvencimento').addClass('error');
				return false;
				
			}
			
		}
		
		if (!that.validaAnexos()) {
			return false;
		}
		
		window.parent.$('#workflowActions').hide();
		
		window.parent.$('button').each(function () {
			
			var attr = $(this).attr('data-send');
			
			if (typeof attr !== typeof undefined && attr !== false) {
				  
				$(this).closest('div').find('button').attr('disabled','disabled');
			
			}
			
		})
		
		$('#S0_serie').val($('#S0_serie').val().toUpperCase());
		
		
		
		return true;
		
	}
	
}