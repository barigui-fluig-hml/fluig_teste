class atv40 extends form {
	
	constructor(){
		
		super();
		
		$(".S0-disabled").addClass("disabled");
		$( ".S0-disabled" ).keydown(function() {
			return false
		});
		$(".S1-disabled").addClass("disabled");
		$(".S2-disabled").addClass("disabled");
		$(".S3-disabled").addClass("disabled");
		
		$('#S0').show();
		$('#S3').show();
		
		if ( $('#S0_hid_SemAF').val().toUpperCase() == 'TRUE' ) {
			
			$('.S0_forComAF').hide();
			
		}
		
		if ($('#S1_aprovacao').val() != '') {
			
			$('#S1').show();
			
		}
		
		if ($('#S2_aprovacao').val() != '') {
			
			$('#S2').show();
			
		}
		
		if ($('#S0_hid_isNFe').val() != undefined && $('#S0_hid_isNFe').val() != null && $('#S0_hid_isNFe').val().toUpperCase() != 'FALSE') {
			
			$('#S0_forNFe').show();
			
			if ($('#S0_hid_link_xml').val() == '') {
				
				$('#S0_xml').closest('div').hide();
				$('#S0_hid_link_xml').val('');
				
			} else {
				
				$('#S0_xml').attr('href',$('#S0_hid_link_xml').val());
				$('#S0_xml').attr('download','xml_nota_' + $('#S0_numnota').val() + '.xml');
				
			}
			
		}
		
		window.UTILS.bindNumParcelas();
		
	}
	
	_loadSection () {
		
		var that = this;
		
		//that._carregaEstruturaDocumentos();
		
	}
	
}