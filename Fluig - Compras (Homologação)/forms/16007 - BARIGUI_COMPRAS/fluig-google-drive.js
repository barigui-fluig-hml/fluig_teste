

var FluigGDriveFactory = (function() {
	/**
	 * Initialise a Google Driver file picker
	 */
	var FluigGDrive = window.FluigGDrive = function(options) {

		// Config
		this.apiKey = options.apiKey;
		this.clientId = options.clientId;
		this.onSelect = options.onSelect;
		this.onStart = options.onStart;
		this.onEnd = options.onEnd;
		if(options.buttonEl) {
			this.buttonEl = options.buttonEl;
			// Disable the button until the API loads, as it won't work properly until then.
			this.buttonEl.disabled = true;		
			this.buttonHandler = this.open.bind(this);
			this.buttonEl.addEventListener('click', this.buttonHandler);
		}
		
		gapi.load('auth', {'callback': this._onAuthLoaded.bind(this)});
		gapi.load('picker', {'callback': this._onPickerLoaded.bind(this)});
	
		// Scope to use to access user's Drive items.
		this.scope = ['https://www.googleapis.com/auth/drive'];
	}

	FluigGDrive.prototype = {
		open: function() {
			if(typeof(this.onStart) == 'function')
				this.onStart(this.buttonEl);
			if(!this.token) {
			  window.gapi.auth.authorize(
				{
					'client_id': this.clientId,
					'scope': this.scope,
					'immediate': false
				},
				this._onAuthResult.bind(this));
		  
			} else {
				this._createPicker();
			}
		},
		
		rebind: function(options) {
			if(this.buttonHandler) {
				this.buttonEl.removeEventListener('click', this.buttonHandler);
			}
			if(options.buttonEl) {
				this.buttonEl = options.buttonEl;
				// Disable the button until the API loads, as it won't work properly until then.
				this.buttonHandler = this.open.bind(this);
				this.buttonEl.addEventListener('click', this.buttonHandler);
			}
			
			this.onSelect = options.onSelect;
			
			this.onStart = options.onStart;
			
			this.onEnd = options.onEnd;
		},
		
		_loaded: function() {
			if(this.authLoaded && this.pickerLoaded)
				this.buttonEl.disabled = false;
		},
		
		_onAuthLoaded: function() {
			this.authLoaded = true;
			this._loaded();
		},
		
		_onPickerLoaded: function() {
			this.pickerLoaded = true;
			this._loaded();
		},
		
		_onAuthResult: function(authResult) {
		  if (authResult && !authResult.error) {
			this.token = authResult.access_token;
			this._createPicker();
		  } else {
			if(typeof(this.onEnd) == 'function')
				this.onEnd(true);
		  }
		},

		
		_createPicker: function() {
			if (this.pickerLoaded && this.token) {
				
				var view = new google.picker.DocsView()
					.setMimeTypes("application/pdf,image/jpeg,image/jpg,application/vnd.google-apps.folder")
					.setIncludeFolders(true);
				
				var picker = new google.picker.PickerBuilder()
					.enableFeature(google.picker.Feature.NAV_HIDDEN)
					.setOAuthToken(this.token)
					.addView(view)
					.setLocale('pt-BR')
					.setDeveloperKey(this.apiKey)
					.setCallback(this._onPickerCallback.bind(this))
					.build();
				picker.setVisible(true);
				var elements= document.getElementsByClassName('picker-dialog');
				for(var i=0;i<elements.length;i++)
				    elements[i].style.zIndex = "2000";
			}
		},

		// A simple callback implementation.
		_onPickerCallback: function(data) {
			if (data.action == google.picker.Action.PICKED) {
				var fileId = data.docs[0].id;
				this._fileDetails(fileId, this._fileGetCallback.bind(this));
			} else if(data.action == google.picker.Action.CANCEL) {
				if(typeof(this.onEnd) == 'function')
					this.onEnd(true);
			}
		},
		
		_fileGetCallback: function(file) {
			if (typeof(this.onSelect) == 'function')
				this.onSelect(file);
			if(typeof(this.onEnd) == 'function')
				this.onEnd(false);
		},		

		//base64ArrayBuffer from https://gist.github.com/jonleighton/958841
		_base64ArrayBuffer: function(arrayBuffer) {
			var base64			= ''
			var encodings		= 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
			var bytes			= new Uint8Array(arrayBuffer)
			var byteLength		= bytes.byteLength
			var byteRemainder	= byteLength % 3
			var mainLength		= byteLength - byteRemainder
			var a, b, c, d, chunk
			for (var i = 0; i < mainLength; i = i + 3) {
				chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2]
				a = (chunk & 16515072) >> 18; b = (chunk & 258048) >> 12; c = (chunk & 4032) >>  6
				d = chunk & 63
				base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d]
			}
			if (byteRemainder == 1) {
				chunk = bytes[mainLength]
				a = (chunk & 252) >> 2; b = (chunk & 3) << 4
				base64 += encodings[a] + encodings[b] + '=='
			} else if (byteRemainder == 2) {
				chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1]
				a = (chunk & 64512) >> 10; b = (chunk & 1008)  >>  4; c = (chunk & 15)    <<  2
				base64 += encodings[a] + encodings[b] + encodings[c] + '='
			}
			return base64
		},
		
		removeAcentos: function(string) {
			var mapaAcentosHex 	= {
				a : /[\xE0-\xE6]/g,
				A : /[\xC0-\xC6]/g,
				e : /[\xE8-\xEB]/g,
				E : /[\xC8-\xCB]/g,
				i : /[\xEC-\xEF]/g,
				I : /[\xCC-\xCF]/g,
				o : /[\xF2-\xF6]/g,
				O : /[\xD2-\xD6]/g,
				u : /[\xF9-\xFC]/g,
				U : /[\xD9-\xDC]/g,
				c : /\xE7/g,
				C : /\xC7/g,
				n : /\xF1/g,
				N : /\xD1/g,
			};
			for ( var letra in mapaAcentosHex ) {
				var expressaoRegular = mapaAcentosHex[letra];
				string = string.replace( expressaoRegular, letra );
			}
			var string = string.replace(/[^\w\s\.]/gi, '');
			var string = string.replace(/\s+/gi, ' ');
			return string;
		},
		
		_request: function(url, returnType, successCallback, errorCallback) {
			var xhr = new XMLHttpRequest();
			xhr.responseType = returnType;
			xhr.open("GET", url, true);
			xhr.setRequestHeader('Authorization', 'Bearer '+this.token);
			xhr.onload = function(){
				if(typeof(successCallback) == 'function') {
					//var data = returnType == 'arraybuffer' ? this._base64ArrayBuffer(xhr.response) : xhr.response;
					//successCallback(data);
					successCallback(xhr.response);
				};
			}.bind(this);
			xhr.error = function(error) {
				if(typeof(errorCallback) == 'function')
					errorCallback(error);
			}.bind(this);
			xhr.send();		
		},
		
		_fileDetails: function(fileId, successCallback, errorCallback) {
			this._request('https://www.googleapis.com/drive/v3/files/'+fileId+'?fields=id,name,size,mimeType', 'json', successCallback, errorCallback);
		},
		
		downloadFile: function(fileId, successCallback, errorCallback) {
			this._request('https://www.googleapis.com/drive/v3/files/'+fileId+'?alt=media', 'arraybuffer', successCallback, errorCallback);
		}

	}
	
	
	var instance;
	return {
		getInstance: function(options){
			if (instance == null) {
				instance = new FluigGDrive(options);
				// Hide the constructor so the returned object can't be new'd...
				instance.constructor = null;
			} else {
				instance.rebind(options);
			}
			return instance;
		}
	};
	
})();


