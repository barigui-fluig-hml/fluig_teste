
/* ===========================================================================
* @	AUTHOR: 		Jean C. J. de Oliveira / <jean.oliveira@totvs.com.br>
* @ DATE: 			16/07/2018
* @ DESCRIPTION: 	FUNÇÃO PARA INICIALIZAR A FUNÇÃO DE ZOOM
* @ PARAMS: 		
* @ RETURNS: 	
* 
* 
* @ ATUALIZADO:		Willian Fellipe Laynes  / <willian.laynes@totvs.com.br>
* @ DATA:			15/10/2018
* @	DESCRICAO:		ALTERADO PARA ACEITAR CONSULTAS EM DATASET 
* @ ALTERAÇÕES: 	ACRECENTADOS OS CAMPOS "type","style","filter"
* 
* 
* @ ATUALIZADO:		Willian Fellipe Laynes  / <willian.laynes@totvs.com.br>
* @ DATA:			20/11/2018
* @	DESCRICAO:		ALTERADO PARA ACEITAR CONSULTAS EM WS002 
* -----------------------------------------------------------------------------
* ========================================================================= */
class GetEventZoom {
	constructor(path,service){
		this.path 		= path
		this.service 	= service
	}
	
    get init(){
    	this._init()
    }
    
    _init(){
		var that = this;
		// MOSTRAR LISTA DE OPÇÕES 
		document.querySelectorAll("*[zoomify]").forEach(function (zoom) {
			//console.log('zoom',zoom);
			const data = JSON.parse(zoom.getAttribute("zoomify"));
			zoom.addEventListener("click", () => {
				FLUIGC.loading(window, {textMessage: '<h1 style="z-index:1001">Processando...</h1>'}).show();
				setTimeout(function(){
			    	Promise.resolve()
				     .then(function(result) {
						//RODA SCROLL PARA EVITAR BUG DO MENU PADRÃO
						var scrollAtual = $('body').scrollTop()
						$('body').animate({scrollTop: 500}, 500);
						////////////////////
		
						if (zoom.disabled || zoom.readOnly) {
							return;
						}
						var b = document.createElement("div");
							b.classList.add("zoomify-background");
						var c = null;
						try{
							switch (data.type.toUpperCase().trim()) {
								case "ARRAY":
									console.log("ARRAY");
									dados	=	that.M_ARRAY(data)
									break;
								case "DATASET":
										console.log("DATASET");
										dados	=	that.M_DATASET(data)
									break;
								case "XTABELAS":
										console.log("XTABELAS");
									var dados 	= that.M_XTABELAS(data);
										dados 	= dados[0].response;
										dados 	= JSON.parse(dados);
										dados 	= dados.SX3.TABLEDATA;
									break;
								case "XCONSULTAS":
									console.log("XCONSULTAS");
								var dados 	= that.M_XCONSULTAS(data);
									dados 	= dados[0].response;
									dados 	= JSON.parse(dados);
									dados 	= dados.SX3.TABLEDATA;
								break;	
								case "CUSTOMIZADO":
									console.log("CUSTOMIZADO");
								var dados 	= that.M_CUSTOMIZADO(data);
									/*dados 	= dados[0].response;
									dados 	= JSON.parse(dados);*/
								break;	
								case "FWMODEL":
									console.log("FWMODEL");
									var dados 	= that.M_FWMODEL(data);
										dados 	= dados[0].response;
										dados 	= JSON.parse(dados);
										dados 	= dados.resources;
										
								break;	
								default:
										console.log("sem tipo de zoom");
									break;
							}
							console.log(dados)
						}catch(e){
							console.log("Erro getDadosZooms:",e)
						}

						
						var j = document.createElement("div");
						b.addEventListener("click", () => {
							j.remove();
							b.remove();
						});

						j.classList.add("zoomify-window");

						var h = document.createElement("div");
						
						var html = `; 
						          <div class="faixapretadatotvs ">
							            <div class="row">
							            	<div class="col-md-10 col-xs-10">
							            		<h3 id="logo" class="aui-header-logo aui-header-logo-custom">
													<img src="http://tdn.totvs.com/download/attachments/327682/atl.site.logo?version=2&modificationDate=1533936917000&api=v2" >
													<strong>`+data.title+`</strong>
												</h3>
											</div>
											<div class="col-md-2 col-xs-2">
												<span id="zoomify-close" class="fluigicon fluigicon-remove-circle fluigicon-md" onclick="$('.zoomify-background').remove(); $('.zoomify-window').remove()"></span>
											</div>
										</div>
							      </div>`;
								
						h.innerHTML = html;
						
											
						
						j.appendChild(h);
						
						var t = document.createElement("table");
							t.classList.add("table");
							
						var thr = document.createElement("tr");

						var th = document.createElement("thead");
						var tb = document.createElement("tbody");
						
						var fb = document.createElement("input");
							fb.classList.add("form-control");
							fb.setAttribute("placeholder", "Digite para filtrar");
							
						var thh = document.createElement("th");
							thh.setAttribute("colspan", data.fields.length);
							thh.appendChild(fb);
							thr.appendChild(thh);
							th.appendChild(thr);
						
						switch (data.type.toUpperCase().trim()) {
							case "ARRAY":
								[j,h,t,thr,th,tb,fb,thh,b] = that.V_DATASET_table(dados,data,j,h,t,thr,th,tb,fb,thh,b,scrollAtual);
							break;
							case "DATASET":
								[j,h,t,thr,th,tb,fb,thh,b] = that.V_DATASET_table(dados,data,j,h,t,thr,th,tb,fb,thh,b,scrollAtual);
							break;
							case "XTABELAS":
								[j,h,t,thr,th,tb,fb,thh,b] = that.V_XTABELAS_table(dados,data,j,h,t,thr,th,tb,fb,thh,b,scrollAtual);
								break;	
							case "XCONSULTAS":
								[j,h,t,thr,th,tb,fb,thh,b] = that.V_XCONSULTAS_table(dados,data,j,h,t,thr,th,tb,fb,thh,b,scrollAtual);
								break;		
							case "FWMODEL":
								[j,h,t,thr,th,tb,fb,thh,b] = that.V_FWMODEL_table(dados,data,j,h,t,thr,th,tb,fb,thh,b,scrollAtual);
							break;
							case "CUSTOMIZADO":
								[j,h,t,thr,th,tb,fb,thh,b] = that.V_CUSTOMIZADO_table(dados,data,j,h,t,thr,th,tb,fb,thh,b,scrollAtual);
							break;	
							default:

								break;
						}

						t.appendChild(th);
						t.appendChild(tb);
						j.appendChild(t);
						document.querySelector(".fluig-style-guide").appendChild(b);
						document.querySelector(".fluig-style-guide").appendChild(j);
						FLUIGC.loading(window, {textMessage: '<h1>Processando...</h1>'}).hide();
				    }
				)
					
				},500)
	

			})
			
			
		});
	}
    
    V_ARRAY_table(dados,data,j,h,t,thr,th,tb,fb,thh,b,scrollAtual){
		if (dados && dados.length) {
			var count = 1;
			dados.forEach((linha) => {
				var tr = document.createElement("tr");
				var f=0;
				data.fields.forEach((field) => {
					var td = document.createElement("td");
					td.innerHTML = linha[field];
					td.classList.add("class_"+count);
					td.style    = data.style[f];	
					tr.appendChild(td);
					f++;
					count++;
				});

				tr.addEventListener("click", () => {
					if (data.callback) {
						if(typeof data.callback == 'object'){
							data.callback.forEach(function(e,v){
								console.log("E:",e," Value:",linha[data.select[v]]);
								if(e!="null" && e!=null && e!=undefined && e!=""){
									document.querySelector("#"+e).value=linha[data.select[v]];	
								}
								
								$("#"+e).change();
							});
						}else{
							zoom.value=linha[data.select];
						}
					}
					j.remove();
					b.remove();
					//RETORNA SCROLL PARA POSIÇÃO INICIAL
					$('body').animate({scrollTop: scrollAtual}, 500);
				});
				tb.appendChild(tr);
			});
			fb.addEventListener("keyup", (e) => {
				var val = fb.value.toLowerCase();
				for (var i = 0; i < tb.rows.length; i++) {
					var tx = tb.rows[i].innerText.toLowerCase();
					if (tx.indexOf(val) == -1) {
						tb.rows[i].style.display = 'none';
					} else {
						tb.rows[i].style.display = '';
					}
				}
			});
		
		} else {
			var _tr = document.createElement("tr"),
				_td = document.createElement("td"),
				_txt = "Nenhum resultado para exibir.";
			_td.innerText = _txt;
			_tr.appendChild(_td);
			tb.appendChild(_tr);
			fb.setAttribute("disabled", "disabled");
		}
		return [j,h,t,thr,th,tb,fb,thh,b] 
	}
    
	V_DATASET_table(dados,data,j,h,t,thr,th,tb,fb,thh,b,scrollAtual){
		if (dados && dados.length) {
			var count = 1;
			dados.forEach((linha) => {
				var tr = document.createElement("tr");
				var f=0;
				data.fields.forEach((field) => {
					var td = document.createElement("td");
					td.innerHTML = linha[field];
					td.classList.add("class_"+count);
					td.style    = data.style[f];	
					tr.appendChild(td);
					f++;
					count++;
				});

				tr.addEventListener("click", () => {
					if (data.callback) {
						if(typeof data.callback == 'object'){
							data.callback.forEach(function(e,v){
								console.log("E:",e," Value:",linha[data.select[v]]);
								if(e!="null" && e!=null && e!=undefined && e!=""){
									document.querySelector("#"+e).value=linha[data.select[v]];	
								}
								
								$("#"+e).change();
							});
						}else{
							zoom.value=linha[data.select];
						}
					}
					j.remove();
					b.remove();
					//RETORNA SCROLL PARA POSIÇÃO INICIAL
					$('body').animate({scrollTop: scrollAtual}, 500);
				});
				tb.appendChild(tr);
			});
			fb.addEventListener("keyup", (e) => {
				var val = fb.value.toLowerCase();
				for (var i = 0; i < tb.rows.length; i++) {
					var tx = tb.rows[i].innerText.toLowerCase();
					if (tx.indexOf(val) == -1) {
						tb.rows[i].style.display = 'none';
					} else {
						tb.rows[i].style.display = '';
					}
				}
			});
		
		} else {
			var _tr = document.createElement("tr"),
				_td = document.createElement("td"),
				_txt = "Nenhum resultado para exibir.";
			_td.innerText = _txt;
			_tr.appendChild(_td);
			tb.appendChild(_tr);
			fb.setAttribute("disabled", "disabled");
		}
		return [j,h,t,thr,th,tb,fb,thh,b] 
	}
	
	V_XTABELAS_table(dados,data,j,h,t,thr,th,tb,fb,thh,b,scrollAtual){
		if (dados && dados.length) {
			var count = 1;
			dados.forEach((linha) => {
				var tr = document.createElement("tr");
				var f=0;
				data.fields.forEach((field) => {
					var td = document.createElement("td");
					td.innerHTML = linha[field];
					td.classList.add("class_"+count);
					td.style    = data.style[f];	
					tr.appendChild(td);
					f++;
					count++;
				});
				tr.addEventListener("click", () => {
					if (data.callback) {
						if(typeof data.callback == 'object'){
							data.callback.forEach(function(e,v){
								console.log("E:",e," Value:",linha[data.select[v]]);
								if(e!="null" && e!=null && e!=undefined && e!=""){
									document.querySelector("#"+e).value=linha[data.select[v]];	
								}
								
								$("#"+e).change();
							});
						}else{
							zoom.value=linha[data.select];
						}
					}
					j.remove();
					b.remove();
					//RETORNA SCROLL PARA POSIÇÃO INICIAL
					$('body').animate({scrollTop: scrollAtual}, 500);
				});
				tb.appendChild(tr);
			});
			fb.addEventListener("keyup", (e) => {
				var val = fb.value.toLowerCase();
				for (var i = 0; i < tb.rows.length; i++) {
					var tx = tb.rows[i].innerText.toLowerCase();
					if (tx.indexOf(val) == -1) {
						tb.rows[i].style.display = 'none';
					} else {
						tb.rows[i].style.display = '';
					}
				}
			});
		
		} else {
			var _tr = document.createElement("tr"),
				_td = document.createElement("td"),
				_txt = "Nenhum resultado para exibir.";
			_td.innerText = _txt;
			_tr.appendChild(_td);
			tb.appendChild(_tr);
			fb.setAttribute("disabled", "disabled");
		}
		return [j,h,t,thr,th,tb,fb,thh,b] 
	}
	V_XCONSULTAS_table(dados,data,j,h,t,thr,th,tb,fb,thh,b,scrollAtual){
		if (dados && dados.length) {
			var count = 1;
			dados.forEach((linha) => {
				var tr = document.createElement("tr");
				var f=0;
				data.fields.forEach((field) => {
					var td = document.createElement("td");
					td.innerHTML = linha[field];
					td.classList.add("class_"+count);
					td.style    = data.style[f];	
					tr.appendChild(td);
					f++;
					count++;
				});
				tr.addEventListener("click", () => {
					if (data.callback) {
						if(typeof data.callback == 'object'){
							data.callback.forEach(function(e,v){
								console.log("E:",e," Value:",linha[data.select[v]]);
								if(e!="null" && e!=null && e!=undefined && e!=""){
									document.querySelector("#"+e).value=linha[data.select[v]];	
								}
								
								$("#"+e).change();
							});
						}else{
							zoom.value=linha[data.select];
						}
					}
					j.remove();
					b.remove();
					//RETORNA SCROLL PARA POSIÇÃO INICIAL
					$('body').animate({scrollTop: scrollAtual}, 500);
				});
				tb.appendChild(tr);
			});
			fb.addEventListener("keyup", (e) => {
				var val = fb.value.toLowerCase();
				for (var i = 0; i < tb.rows.length; i++) {
					var tx = tb.rows[i].innerText.toLowerCase();
					if (tx.indexOf(val) == -1) {
						tb.rows[i].style.display = 'none';
					} else {
						tb.rows[i].style.display = '';
					}
				}
			});
		
		} else {
			var _tr = document.createElement("tr"),
				_td = document.createElement("td"),
				_txt = "Nenhum resultado para exibir.";
			_td.innerText = _txt;
			_tr.appendChild(_td);
			tb.appendChild(_tr);
			fb.setAttribute("disabled", "disabled");
		}
		return [j,h,t,thr,th,tb,fb,thh,b] 
	}
	V_FWMODEL_table(dados,data,j,h,t,thr,th,tb,fb,thh,b,scrollAtual){
		if (dados && dados.length) {
			var count = 1;
			/*MAPEIA O INDICE DOS CAMPOS DO RETORNO, PARA NAO FAZER LOOP EM CADA LINHA*/
			var mapF	= 	dados[0].models[0].fields;
			var mapFields	=	new Array();
			for (var i = 0; i < mapF.length; i++) {
				mapFields[mapF[i].id]	=	i;
			}
			
			
			dados.forEach((linha) => {
				var tr = document.createElement("tr");
				var f=0;
				data.fields.forEach((field) => {
					var valueField		=	linha.models[0].fields[mapFields[field]].value;
					var td 				= 	document.createElement("td");
						td.innerHTML 	= 	valueField;
						td.style    	= 	data.style[f];
						td.classList.add("class_"+count);
						tr.appendChild(td);
						f++;
						count++;
				});

				tr.addEventListener("click", () => {
					if (data.callback) {
						if(typeof data.callback == 'object'){
							data.callback.forEach(function(e,v){
								if(e!="null" && e!=null && e!=undefined && e!=""){
									document.querySelector("#"+e).value	= linha.models[0].fields[mapFields[data.select[v]]].value;	
								}
								$("#"+e).change();
							});
						}else{
							zoom.value	=	linha[data.select];
						}
					}
					j.remove();
					b.remove();
					//RETORNA SCROLL PARA POSIÇÃO INICIAL
					$('body').animate({scrollTop: scrollAtual}, 500);

				});

				tb.appendChild(tr);
			});

			fb.addEventListener("keyup", (e) => {

				var val = fb.value.toLowerCase();

				for (var i = 0; i < tb.rows.length; i++) {
					var tx = tb.rows[i].innerText.toLowerCase();
					if (tx.indexOf(val) == -1) {
						tb.rows[i].style.display = 'none';
					} else {
						tb.rows[i].style.display = '';
					}
				}

			});
		
		} else {

			var _tr = document.createElement("tr"),
				_td = document.createElement("td"),
				_txt = "Nenhum resultado para exibir.";

			_td.innerText = _txt;
			_tr.appendChild(_td);
			tb.appendChild(_tr);
			fb.setAttribute("disabled", "disabled");
		}
		return [j,h,t,thr,th,tb,fb,thh,b] 
		
	}
	V_CUSTOMIZADO_table(dados,data,j,h,t,thr,th,tb,fb,thh,b,scrollAtual){
		console.log(dados)
		if (dados && dados.length) {
			var count = 1;
			dados.forEach((linha) => {
				var tr = document.createElement("tr");
				var f=0;
				data.fields.forEach((field) => {
					var td = document.createElement("td");
					td.innerHTML = linha[field];
					td.classList.add("class_"+count);
					td.style    = data.style[f];	
					tr.appendChild(td);
					f++;
					count++;
				});
				tr.addEventListener("click", () => {
					if (data.callback) {
						if(typeof data.callback == 'object'){
							data.callback.forEach(function(e,v){
								console.log("E:",e," Value:",linha[data.select[v]]);
								if(e!="null" && e!=null && e!=undefined && e!=""){
									document.querySelector("#"+e).value=linha[data.select[v]];	
								}
								
								$("#"+e).change();
							});
						}else{
							zoom.value=linha[data.select];
						}
					}
					j.remove();
					b.remove();
					//RETORNA SCROLL PARA POSIÇÃO INICIAL
					$('body').animate({scrollTop: scrollAtual}, 500);
				});
				tb.appendChild(tr);
			});
			fb.addEventListener("keyup", (e) => {
				var val = fb.value.toLowerCase();
				for (var i = 0; i < tb.rows.length; i++) {
					var tx = tb.rows[i].innerText.toLowerCase();
					if (tx.indexOf(val) == -1) {
						tb.rows[i].style.display = 'none';
					} else {
						tb.rows[i].style.display = '';
					}
				}
			});
		
		} else {
			var _tr = document.createElement("tr"),
				_td = document.createElement("td"),
				_txt = "Nenhum resultado para exibir.";
			_td.innerText = _txt;
			_tr.appendChild(_td);
			tb.appendChild(_tr);
			fb.setAttribute("disabled", "disabled");
		}
		return [j,h,t,thr,th,tb,fb,thh,b,scrollAtual] 
		
	}
	
	M_ARRAY(data){
	   var that 	=	this;
		
	   var consulta = data.dataset;
	   if (consulta != null && consulta != null && consulta.length > 0) {
		   return(consulta); 
	   }else{
		   return(consulta);
	   }
	}
	M_DATASET(data){
		var that 	=	this;
		var dados = {
						"dataset":data.dataset,
						"ordem":null,
						"campos":null,
						"filtros":JSON.stringify(data.filter)
	           		}
		console.log(dados)
	   var consulta = SOLVS.getDados(dados);
		console.log(consulta)
	   if (consulta != null && consulta.values != null && consulta.values.length > 0) {
		   return(consulta.values); 
	   }else{
		   return(consulta);
	   }
	}
	
	M_XTABELAS(data){
		var that 	=	this;
		if(data.dataset == 'SX5'){
			if(data.where != undefined){
				var where = "X5_TABELA='"+data.where+"'";
			}else{
				var where = '';
			}
		}else if(data.dataset == 'CC2'){
			if(data.callback[0] == "B1_CODMUN"){
				var EST = $("#B1_ESTADO").val().trim();
			}else{
				var EST = $("#B1_ESTADO").val().trim();
			}

			if(EST != ''){
				var where = "CC2_EST='"+EST+"'";;
			}else{
				var where = '';
			}
		}else{
			var where = '';
		}
		
		// Caso tenha que aplicar algum filtro no dataset
		var param =  JSON.stringify({
			       'contexto': 'interno',
			    	   'path': this.path+'/XTABELAS',
			    	'service': this.service,
			    	'filtro':{
					        'tabela': data.dataset,
					        'campos': '',
					        'queryWhere': where,
					        'filial': data.filter.filial,
					        'struct': true
					    }
					})
		
		var dados = {
				  		"dataset":this.service,
				  		"ordem":null,
				  		"campos":null,
				  		"filtros":[{
				          	"tipo":"MUST",
				          	"campo":"param",
				          	"initial":param,
				          	"final":param,
				          	"like":false
		                		}]
		           }
		var consulta = SOLVS.getDados(dados);
		console.log("dados",dados);
		console.log("consulta",consulta);
		if (consulta != null && consulta.values != null && consulta.values.length > 0) {
		   return(consulta.values); 
		}else{
			return(consulta);
		}
	}
	M_XCONSULTAS(data){
		var that 	=	this;
		if(data.dataset == 'SX5'){
			if(data.where != undefined){
				var where = "X5_TABELA='"+data.where+"'";
			}else{
				var where = '';
			}
		}else if(data.dataset == 'CC2'){
			if(data.callback[0] == "A1_CODMUNE"){
				var EST = $("#A1_ESTE").val().trim();
			}else{
				var EST = $("#A1_EST").val().trim();
			}

			if(EST != ''){
				var where = "CC2_EST='"+EST+"'";;
			}else{
				var where = '';
			}
		}else{
			var where = '';
		}
		
		// Caso tenha que aplicar algum filtro no dataset
		var param =  JSON.stringify({
			       'contexto': 'interno',
			    	   'path': this.path+'/XCONSULTAS',
			    	'service': this.service,
			    	'filtro':{
					        'tabela': data.dataset,
					        'campos': '',
					        'queryWhere': where,
					        'filial': '',
					        'struct': true
					    }
					})
		
		var dados = {
				  		"dataset":this.service,
				  		"ordem":null,
				  		"campos":null,
				  		"filtros":[{
				          	"tipo":"MUST",
				          	"campo":"param",
				          	"initial":param,
				          	"final":param,
				          	"like":false
		                		}]
		           }
		var consulta = SOLVS.getDados(dados);
		console.log("dados",dados);
		console.log("consulta",consulta);
		if (consulta != null && consulta.values != null && consulta.values.length > 0) {
		   return(consulta.values); 
		}else{
			return(consulta);
		}
	}
	
	M_FWMODEL(data){
		console.log("this",this)
		var param1 = JSON.stringify({
						        path: this.path+"fwmodel",
						        service: data.dataset,
						        filtro: data.filter,
						        contexto: "interno"
						     	})
     	var dados = {
		     		"dataset":this.service,
		     		"ordem":null,
		            "campos":null,
		            "filtros":[{
		            			"tipo":"MUST",
		            			"campo":"param1",
		            			"initial":param1,
		            			"final":param1,
		            			"like":false
		                	}]
		           }
		console.log("dados",dados)
		var consulta = SOLVS.getDados(dados);
		console.log("consulta",consulta)
		if (consulta != null && consulta.values != null && consulta.values.length > 0) {
			   return(consulta.values); 
		}else{
			   return(false);
		   }
	}
	M_CUSTOMIZADO(data){
		var that = this 
		var param1 = JSON.stringify({
						        path: this.path,
						        service: data.dataset,
						        filtro: data.filter,
						        contexto: "interno"
						     	})
     	var dados = {
		     		"dataset":this.service,
		     		"ordem":null,
		            "campos":null,
		            "filtros":[{
		            			"tipo":"MUST",
		            			"campo":"param1",
		            			"initial":param1,
		            			"final":param1,
		            			"like":false
		                	}]
		           }
		console.log("dados",dados)
		var consulta = SOLVS.getDados(dados);
		console.log("consulta",consulta)
		if (consulta != null && consulta.values != null && consulta.values.length > 0) {
				//INFORME AQUI NOME DOS WS SERVICES
				switch(data.dataset){
					case 'WS002':
						return that.WS002(consulta.values);
						break;
				}
			   return(consulta.values); 
		}else{
			   return(false);
		   }
	}
	
	//INFORME AQUI O TRATAMENTO DOS RETORNOS WS SERVICES
	WS002(consulta){
		var status = JSON.parse(consulta[0].response).Status
		if(status=="OK"){
			return(JSON.parse(consulta[0].response).empresas)
		}else{
			return false
		}
	}
	

}


