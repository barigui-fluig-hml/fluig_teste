class atv13 extends form {
	
	constructor() {
		
		super();
		
		$(".S0-disabled").addClass("disabled");
		
		$('#S0_serie').closest('div').removeClass('S0-disabled');
		$('#S0_valor').closest('div').removeClass('S0-disabled');
		
		$( ".S0-disabled" ).keydown(function() {
			return false
		});
		$(".S1-disabled").addClass("disabled");
		$(".S2-disabled").addClass("disabled");
		
		if ( $('#S0_hid_SemAF').val().toUpperCase() == 'TRUE' ) {
			
			$('.S0_forComAF').hide();
			
		}
		
		window.UTILS.bindNumParcelas();
		
	}
	
	_loadSection () {
		
		var that = this;
		
		$('#S0_hid_SemAF').val($('#S0_hid_SemAF').val().toUpperCase())
		
		that.carregaLoginRecebedor()
		
		if ( $('#S0_hid_SemAF').val().toUpperCase() == 'TRUE' ) {
			
			if ($('#S0_hid_isNFe').val().toUpperCase() == 'LANCADOR NFE TI') {
				
				var lancador = new lancadorIntegracao()
				lancador.formataAnexos();
				
			}
			
			that.loadSemAF();
			
		} else {
			
			that.loadComAF();
			
		}
		
		if ($('#S0_hid_isNFe').val() != undefined && $('#S0_hid_isNFe').val() != null && $('#S0_hid_isNFe').val().toUpperCase() != 'FALSE')
			$('#S0_forNFe').show();
		
		var S0_link_xml = $('#S0_link_xml').val();
		
		if (!S0_link_xml || S0_link_xml == undefined || S0_link_xml == null 
				|| S0_link_xml == '' || S0_link_xml.length <= 0) {
			
			$('#S0_xml').closest('div').hide();
			$('#S0_link_xml').val('');
			
		} else {
			
			$('#S0_xml').attr('chave',$('#S0_link_xml').val());
			//$('#S0_xml').attr('download','xml_nota_' + $('#S0_numnota').val() + '.xml'); 
			
		}
		
		$('#S0_xml').click(function(){
			
			var data = {
				companyId: 1,
				serviceCode: 'CapturaNFE',
				endpoint : '/api/document/?key='+$(this).attr('chave')+'&format=xml&download=true&encode=false',
				method: 'get',
				timeoutService: '100'
			};
			
			window.parent.WCMAPI.Create({
	    	    url: '/api/public/2.0/authorize/client/invoke',
	    	    contentType: "text/json",
	    	    dataType: "json",
	    	    async: false,
	    	    data: JSON.stringify(data),
	    	    success: function(data){
	    	    	
	    	    	var result = data.content.result
	    	    	var base64 = btoa(result)
	    	    	var blob = window.ANEXOS.base64toBlob(base64,"text/xml")
	    	    	const url = window.URL.createObjectURL(blob);
	    	        const a = document.createElement('a');
	    	        a.style.display = 'none';
	    	        a.href = url;
	    	        a.download = 'xml-' + $('#S0_numnota').val() + '.xml';
	    	        document.body.appendChild(a);
	    	        a.click();
	    	        window.URL.revokeObjectURL(url);
	    	    	
	    	    }
			})
			
		})
		
		$('#S3').show();
		$('#S0').show();
		
		if ($('#S1_aprovacao').val() != '') {
			
			$('#S1').show();
			
		}
		
		if ($('#S2_aprovacao').val() != '') {
			
			$('#S2').show();
			
		}
		 
		 $('#S3_aprovacao').val('');
		 $('#S3_Obs').val('');
		 
		 $('#S0_valor').closest('div').removeClass('disabled');
		 $('#S0_serie').closest('div').removeClass('disabled');
		 
		 if($('#S0_dat_emissao').is('input')) {
		 
			 $('#S0_dat_emissao').closest('div').removeClass('disabled');
			 
			 $('#S0_dat_emissao').on('keydown', function(e){
				if (e.which == 8)
					$(this).val('');
				else
					$(this).val($(this).val());
				
				e.preventDefault();
			})
			
			FLUIGC.calendar('#S0_dat_emissao', {
			    pickDate: true,
			    pickTime: false
			});
		 
		 }
		 
		 $('#S3_numLancamento').attr('readonly','');
		 
		 $('#S0_valor').mask("#.##0,00", {reverse: true});
		 $('#S0_valor').removeAttr('readonly');
		 
		 
		 that._validaMarcaFilial();
		
	}
	
	loadSemAF () {
		
		var that = this;
		that.bindCondicaoPagamento();
		window.UTILS.bindNumParcelas();
		
	}
	
	carregaLoginRecebedor () {
		
		var that = this;
		
		if ($('#S0_loginrecebedor').val()!= undefined && $('#S0_loginrecebedor').val() != null && $('#S0_loginrecebedor').val() != "") {
		
			var constraints = [];
			constraints.push(DatasetFactory.createConstraint('login',$('#S0_loginrecebedor').val().toLowerCase().trim(),$('#S0_loginrecebedor').val().toLowerCase().trim(),ConstraintType.SHOULD));
			var users = that._getData('colleague',constraints);
			
			if (users != undefined && users != null && users != "") {
				
				var user = users.find(x => x.login.toUpperCase() == $('#S0_loginrecebedor').val().toUpperCase().trim());
				
				if (user && user != undefined && user != null) {
					
					$('#S0_recebedor_matricula').val(user['colleaguePK.colleagueId'].toUpperCase());
					$('#S0_emailrecebedor').val(user['mail'].toUpperCase());
					
				} else {
					
					that._msgDanger('O usuário recebedor '.toUpperCase() + $('#S0_loginrecebedor').val().toUpperCase() + ' não foi encontrado no sistema'.toUpperCase());
					
				}
				
			}
		
		}
		
	}
	
	loadComAF () {
		
		var that = this;
		
		that.carregaLoginRecebedor()
		
		if ($('#S0_tipo').val() == "TELEMETRIX") {
			
			var telemetrix = new telemetrixIntegracao(that.modeView)
			telemetrix.formataAnexos();
			
		}
		
		if ($('#S0_tipo').val() == "BASE B") {
			
			var baseB = new basebIntegracao()
			baseB.formataAnexos();
			
		}
			
		
		window.UTILS.bindNumParcelas();
		
	}
	
	bindCondicaoPagamento () {
		
		var that = this;
		
		var hid_infoForm = JSON.parse($('#hid_infoForm').val());
		
		if (hid_infoForm.modeView != 'VIEW') {
		
			var val = $('#S0_condicaoPagamento').val();
			var div = $('#S0_condicaoPagamento').closest('div');
			$(div).removeClass("disabled");
			$('#S0_condicaoPagamento').remove();
			
			var condPagamento = [
				   {NOME:'Acerto pelo Caixa'}
					,{NOME:'Adiantamento'}
					,{NOME:'Boleto'}
					,{NOME:'Débito Automático'}
					,{NOME:'Depósito'}
					,{NOME:'Encontro de Contas'}
					,{NOME:'Pix'}
			    ]
			
			condPagamento = condPagamento.map(function (v) { return {value: v.NOME.toUpperCase()} });
			
			condPagamento.sort(function(a,b){
				
				if (a.NOME > b.NOME) return 1
				if (a.NOME < b.NOME) return -1
				return 0
				
			})
			
			var view = { linhas: condPagamento };
			
			var template = `<select id="S0_condicaoPagamento" name="S0_condicaoPagamento" class="form-control" obrigatorio="true">
								<option value="">==SELECIONE==</option>
								{{#linhas}}
									<option value="{{value}}">{{value}}</option>
								{{/linhas}}
							</select>`;
			
			$(div).append(Mustache.render(template,view));
			$(div).show();
			$('#S0_condicaoPagamento').val(val.toUpperCase());
			
		}
		
	}
	
	_validaMarcaFilial() {
		
		var that = this;
		
		if (JSON.parse($('#hid_infoForm').val()).modeView == "VIEW") {

			var filial = that._getData('dsBARIGUI_lojasIntranet',null).find(x => x.CNPJ == $('#S0_CNPJ_filial').text().replace(/[^a-zA-Z0-9]/g,'') && x.Status == "A");
			
            var marca = getMarca();

            $('#S0_marca').text(marca[0].Descricao);
		    $('#S0_filial').text(filial.Fantasia);

		} else {
			
			var filial = that._getData('dsBARIGUI_lojasIntranet',null).find(x => x.CNPJ == $('#S0_CNPJ_filial').val().replace(/[^a-zA-Z0-9]/g,'') && x.Status == "A");
			
            var marca = getMarca();
            
            $('#S0_marca').val(marca[0].Descricao);
		    $('#S0_filial').val(filial.Fantasia);
		}
		
		function getMarca() {
			var constraints = [];
			constraints.push(DatasetFactory.createConstraint('MarcaID',filial.MarcaID,filial.MarcaID,ConstraintType.MUST));
			var marcaLocal = that._getData('dsBARIGUI_marcas',constraints);

            return marcaLocal;
		}
		
		
	}
	
	_sectionValidate () {
		
		var that = this;
		
		if ($('#S3_aprovacao').val().toUpperCase() == 'NAO') {
			
			if ($('#S3_Obs').val() == undefined || $('#S3_Obs').val() == null || $('#S3_Obs').val() == "") {
				
				$('#S3_Obs').addClass('error');
				
				that._msgDanger('O Campo observação é obrigatório');
				return false;
				
			}
			
			return true;
			
		}
		
		if ($('#S0_complemento').val() != 'NOTA FISCAL SEM PAGAMENTO' && $('#S0_marca').val() != 'BRAUTO') {
			if ((!$('#S0_numParcelas').val() || $('#S0_numParcelas').val() == undefined || $('#S0_numParcelas').val() == null || $('#S0_numParcelas').val() == '' || $('#S0_numParcelas').val() == '0')
				|| (!$('#hid_parcelas').val() || $('#hid_parcelas').val() == undefined || $('#hid_parcelas').val() == null || $('#hid_parcelas').val() == '')) 
			{
				
				this._msgDanger('Informe os dados de pagamento da NF');
				$('#S0_numParcelas').addClass('error');
				$('#S0_link_verPacelas').css('color','red');
				return false;
				
			} else {
				
				var list = JSON.parse($('#hid_parcelas').val())
				
				if (list != undefined && list != null && list != {} && list != []) {
					
					var vl_total = 0;
					
					for (var i in list) {
						
						var obj = list[i];
						
						if (obj.numero == undefined || obj.numero == null || obj.numero == ""
							|| obj.data_vencimento == undefined || obj.data_vencimento == null || obj.data_vencimento == ""
							|| obj.valor == undefined || obj.valor == null || obj.valor == "") 
						{
							
							this._msgDanger('Dados da parcela "' 
									+ ((obj.numero == undefined || obj.numero == null || obj.numero == "")? "XXX" : obj.numero) 
									+ '" Incompletos ou inválidos');
							$('#S0_numParcelas').addClass('error');
							$('#S0_link_verPacelas').css('color','red');
							return false;
							
						} else {
							
							vl_total += parseFloat(obj.valor.split('.').join('').replace(',','.'));
							
						}
						
					}
					
					var S0_valor = $('#S0_valor').val().replace('R$','').split('.').join('').replace(',','.');
					if (vl_total.toFixed(2) != S0_valor) {
						
						this._msgDanger('O valor das parcelas não está correto!! verificar valor total da nota');
						$('#S0_numParcelas').addClass('error');
						$('#S0_link_verPacelas').css('color','red');
						return false;
						
					}
					
				} else {
					
					this._msgDanger('Informe os dados de pagamento da NF');
					$('#S0_numParcelas').addClass('error');
					$('#S0_link_verPacelas').css('color','red');
					return false;
					
				}
				
			}
		
			var nota =  $('#S0_numnota').val();
			
			var serie =  $('#S0_serie').val();
			
			var constraints = [];
			
			constraints.push(DatasetFactory.createConstraint('status','A','A',ConstraintType.MUST));
			
			var list_filiais = DatasetFactory.getDataset("dsBARIGUI_lojasIntranet",null,constraints,null).values;
			
			var lojaId = 0;
			
			var nome = $('#S0_filial').val();
			
			for (var i in list_filiais) {
				if (list_filiais[i].CNPJ == $('#S0_CNPJ_filial').val().split('/').join('').split('-').join('').split('.').join('')) {
					lojaId = list_filiais[i].LojaID;
					nome = list_filiais[i].Nome.toUpperCase();
					break;
				}
			}
			
			var fornecedorDoc = $('#S0_CNPJCPF').val().split('.').join('').split('-').join('').split('/').join('');
			
			var url = '/api/fluig/pagamentoTitulos/GetTitulosByNumero/' 
						+ lojaId.toString() 
						+ '?tituloNumero=' 
						+ nota.toString()
						+ '&docFornecedor='
						+ fornecedorDoc.toString()
						+ '&notaSerie=' + serie.toString();
			
			var data = {
					companyId: 1,//WCMAPI.organizationId,
					serviceCode: 'CoreREST',
					endpoint : url,
					method: 'get',
					timeoutService: '100'
				};
			
			var ret = false;
			
			var msg = 'Nota Fiscal inválida - Verifique se informou o número ou a serie corretos e se a Nota foi lançada para a filial e fornecedor corretos';
			
			window.parent.WCMAPI.Create({
	    	    url: '/api/public/2.0/authorize/client/invoke',
	    	    contentType: "text/json",
	    	    dataType: "json",
	    	    async: false,
	    	    data: JSON.stringify(data),
	    	    success: function(data){
	    	    	
	    	    	if(data.message && data.message.message == 'OK') {
	    	    		var response = data.content;
	    	    		if(response.result) {
	    	    			
	    	    			var hid_parcelas = JSON.parse($('#hid_parcelas').val());
	    	    			
	    	    			var rep = JSON.parse(response.result);
	    	    			
	    	    			if (rep && rep != undefined && rep != null 
	    	    					&& rep.listaTitulos && rep.listaTitulos != undefined && rep.listaTitulos != null ) {
	    	    			
	    	    				if (!$('#S0_marca').val().includes('TOYOTA')) {
	    	    				
			    	    			if (rep.listaTitulos.length != hid_parcelas.length)
			    	    			{
			    	    				
			    	    				$('#S0_numParcelas').addClass('error');
			    	        	    	
			    	    				msg = 'Número de Parcelas está Divergente do Lançamento';
			    	        	    	return;
			    	    				
			    	    			}
			    	    			
			    	    			if ($('#S0_dat_emissao').val() != rep.listaTitulos[0].tituloDataEmissao) {
			    	    				
			    	    				$('#S0_dat_emissao').addClass('error');
			    	    				
			    	    				msg = 'Data de Emissão está Divergente do Lançamento';
			    	        	    	return;
			    	    				
			    	    			}
			    	    			
			    	    			if (parseInt($('#S0_numnota').val()).toString() != rep.notaFiscalNumero) {
			    	    				
			    	    				$('#S0_numnota').addClass('error');
			    	        	    	
			    	    				msg = 'Número da Nota está Divergente do Lançamento';
			    	        	    	return;
			    	    				
			    	    			}
			    	    			
			    	    			if ($('#S0_serie').val().toUpperCase() != rep.notaFiscalSerie.toUpperCase()) {
			    	    				
			    	    				$('#S0_serie').addClass('error');
			    	        	    	
			    	    				msg = 'Serie da Nota está Divergente do Lançamento';
			    	        	    	return;
			    	    				
			    	    			}
			    	    			
			    	    			if (!$('#S0_marca').val().toUpperCase().includes('TOYOTA')) {
			    	    			
				    	    			for (var i in hid_parcelas) {
				    	    				
				    	    				if (hid_parcelas[i].valor.trim() != rep.listaTitulos[i].tituloValor.trim()) {
				    	    					
				    	    					$('#temp_valor_' + hid_parcelas[i].numero).addClass('error');
				        	        	    	
				    	    					msg = 'Valor da Parcela ' + hid_parcelas[i].numero + ' está Divergente do Lançamento';
				        	        	    	return;
				    	    					
				    	    				}
				    	    				
				    	    				if (hid_parcelas[i].data_vencimento != rep.listaTitulos[i].tituloDataVencimento) {
				    	    					
				    	    					$('#temp_data_' + hid_parcelas[i].numero).addClass('error');
				        	        	    	
				    	    					msg = 'Data de Vencimento da Parcela ' + hid_parcelas[i].numero + ' está Divergente do Lançamento';
				        	        	    	return;
				    	    					
				    	    				}
				    	    				    	    				
				    	    			}
				    	    			
			    	    			}
			    	    			
	    	    				}
		    	    			
		    	    			$('#S3_numLancamento').val(rep.notaFiscalNumero);
		    	    			$('#S0_filial').val(nome);
		    	    			
		    	    			var criaProcessosUrl = '/api/fluig/pagamentoTitulos/integraTituloByNumero/' 
		    	    				+ lojaId.toString()
		    	    				+ "/"
		    	    				+ fornecedorDoc;
		    	    			
		    	    			criaProcessosUrl = encodeURI(criaProcessosUrl 
										+ "/" 
										+ rep.notaFiscalNumero
										+ '/' + serie.toString()
										+ "/"
										+ "Centro de Escrituração Fiscal"
										+ "/"
										+ window.parent.$('#detailStatusTab').html().split('Solicitação:')[1].trim())
										;
		    	    				
			    				var request = {
			    						companyId: 1,//WCMAPI.organizationId,
			    						serviceCode: 'CoreREST',
			    						endpoint : criaProcessosUrl,
			    						method: 'post',
			    						timeoutService: '100'
			    					};
		    	    				
			    				window.parent.WCMAPI.Create({
			    		    	    url: '/api/public/2.0/authorize/client/invoke',
			    		    	    contentType: "text/json",
			    		    	    dataType: "json",
			    		    	    async: true,
			    		    	    data: JSON.stringify(request)
			    				});
		    	    			
			    				ret = true;
		    	    			return;
		    	    			
	    	    			}
	    	    		}
	    	    	}
	    			
	    	    	$('#S0_marca').addClass('error');
	    	    	$('#S0_filial').addClass('error');
	    	    	$('#S0_CNPJ_filial').addClass('error');
	    	    	
	    			$('#S0_numnota').addClass('error');
	    	    	$('#S0_serie').addClass('error');
	    	    	
	    	    	$('#S0_valor').addClass('error');
	    	    	$('#S0_dat_emissao').addClass('error');
	    	    	
	    	    	return;
	    	    	
	    	    }
			});
			
			if (!ret) {
				
				that._msgDanger(msg);
				
			}
			
			return ret;
			
		}
		
		return true;
		
	}
	
}