class basebIntegracao {
	
	formataAnexos() {
		var that = this
		if ($('#hid_folder').val() != '') {
			
			var prefix = $('#hid_folder').val();
			if (prefix.charAt((prefix.length - 1)) === '/') { prefix = prefix.substr(0, (prefix.length - 1)) }
			$('#hid_folder').val(prefix);
		}
		
		var S0_hid_link_nota = eval($('#S0_hid_link_nota').val());
		var S0_hid_link_boleto = eval($('#S0_hid_link_boleto').val());
		var S0_hid_link_rateio = eval($('#S0_hid_link_rateio').val());
		var S0_hid_link_outros = eval($('#S0_hid_link_outros').val());
		var S0_hid_link_xml =  $('#S0_hid_link_xml').val();
		
		$('#S0_link_xml').val(S0_hid_link_xml.split('NFe').join(''));
		
		for (var i in S0_hid_link_nota) {
			that._uploadDocs('S0_nota',S0_hid_link_nota[i],'NF.pdf')
		}
		
		for (var i in S0_hid_link_boleto) {
			that._uploadDocs('S0_boleto',S0_hid_link_boleto[i],'Boleto.pdf')
		}
		
		for (var i in S0_hid_link_rateio) {
			that._uploadDocs('S0_rateio',S0_hid_link_rateio[i],'Rateio.pdf')
		}

		for (var i in S0_hid_link_outros) {
			that._uploadDocs('S0_outros',S0_hid_link_outros[i],'Arquivo.pdf')
		}
			
		$('#S0_hid_link_nota').val('')
		$('#S0_hid_link_boleto').val('')
		$('#S0_hid_link_rateio').val('')
		$('#S0_hid_link_outros').val('')
		
		window.FORMULARIO.contaDocumentosAnexados()
		
	}
	
	__createDocs (tipo,link,file) {
		
		var that = this
		
		var doc = {
				key: tipo,
				value: {
					link: link,
					thumb: '',
					thumbName: '',
					name: window.ANEXOS.renameFile(file),
					createDate: new Date()
				}
			}
		
		var list = [];
		
		if ($('#anexos_listaCarregados').val() != undefined && $('#anexos_listaCarregados').val() != null && $('#anexos_listaCarregados').val() != '')
			list = JSON.parse($('#anexos_listaCarregados').val());
		
		list.push(doc)
		
		$('#anexos_listaCarregados').val(JSON.stringify(list));
		
	}
	
	_uploadDocs (tipo, link, file) {
		
		var that = this;
		
		var data = {
			companyId: 1,
			serviceCode: 'bucketBari',
			endpoint : link,
			method: 'get',
			timeoutService: '100'
		};
		
		window.parent.WCMAPI.Create({
    	    url: '/api/public/2.0/authorize/client/invoke',
    	    contentType: "text/json",
    	    dataType: "json",
    	    async: false,
    	    data: JSON.stringify(data),
    	    success: function(data){
    	    	
    	    	var result = data.content.result.split('"').join('')
    	    	var blob = that.base64toBlob(result,'application/octet-stream')
    	    	
    			var form = new FormData();
    			form.append("blob", blob, window.ANEXOS.renameFile(file));
    	    	
    	    	$.ajax({
		            async : false,
		            type : 'POST',
		            processData: false,
		            contentType: false,
		            url : '/ecm/upload',
		    		data: form,
		    		error: function(e) {
		    			console.error('error on load file')
		    		},
		    		success: function(data) { 
		    			
		    			var f = JSON.parse(data).files[0]
		    			
		    			var doc = {
								key: tipo,
								value: {
									link: link,
									thumb: '',
									thumbName: '',
									name: '',
									createDate: new Date()
								}
							}
		    			
		    			var value = DatasetFactory.getDataset(
			        					'dsGeraThumbnail',
			        					null,
			        					[DatasetFactory.createConstraint('fileName',f.name,f.name,ConstraintType.MUST)],
			        					null
			        		    	).values[0];
		    			
		    			//doc.value.name = value.fileName;
		    			
		    			
		    			var blobThumb = that.base64toBlob(value.base64,'application/pdf');
		    			var blob_link = URL.createObjectURL(blobThumb);
		    			
		    			if (value.resultado == 'OK') {
		    				doc.value.thumb = blob_link;
		    				doc.value.thumbName = value.thumbName;
		    			}
		    			
		    			window.ANEXOS.uploads.push(doc);
		    			window.FORMULARIO.contaDocumentosAnexados()
		    			
		    		}
		    	});
    	    	
    	    }
		})
	}
	
	base64toBlob(base64Data, contentType) {
		
		try {
			
	        contentType = contentType || '';
	        var sliceSize = 1024;
	        var byteCharacters = atob(base64Data);
	        var bytesLength = byteCharacters.length;
	        var slicesCount = Math.ceil(bytesLength / sliceSize);
	        var byteArrays = new Array(slicesCount);
	        for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
	            var begin = sliceIndex * sliceSize;
	            var end = Math.min(begin + sliceSize, bytesLength);
	            var bytes = new Array(end - begin);
	            for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
	                bytes[i] = byteCharacters[offset].charCodeAt(0);
	            }
	            byteArrays[sliceIndex] = new Uint8Array(bytes);
	        }
	        
	        return new Blob(byteArrays, { type: contentType });
	        
		} catch (e) {
			
			return new Blob();
			
		}
		
    }
	
}