function beforeStateLeave(sequenceId) {
	
	try{
		
		var obj 					= 	{}
			obj.WKUser 				= 	getValue("WKUser");
			obj.hid_idAtividade 	= 	String(getValue("WKNumState"));
			obj.obs					=	('<p style="padding: 0px;margin: 0px;margin-left: 25px;">Data de vencimento: ' + hAPI.getCardValue("S0_dtvencimento") + '</p>'
										+ '\n<p style="padding: 0px;margin: 0px;margin-left: 25px;">Tipo: ' + hAPI.getCardValue("S0_tipo") + ' - ' + hAPI.getCardValue("S0_complemento") + '</p>'
										+ '\n<p style="padding: 0px;margin: 0px;margin-left: 25px;">Conta Gerencial: ' + hAPI.getCardValue("S0_contagerencial") + '</p>'
										+ '\n<p style="padding: 0px;margin: 0px;margin-left: 25px;">Condição de Pagamento: ' + hAPI.getCardValue("S0_condicaoPagamento") + '</p>'
										+ '\n<p style="padding: 0px;margin: 0px;margin-left: 25px;">Valor NF: ' + hAPI.getCardValue("S0_valor") + '</p>'
										+ '\n<p style="padding: 0px;margin: 0px;margin-left: 25px;">Numero NF: ' + hAPI.getCardValue("S0_numnota") + '</p>'
										+ '\n<p style="padding: 0px;margin: 0px;margin-left: 25px;">Serie NF: ' + hAPI.getCardValue("S0_serie") + '</p>'
										+ '\n<p style="padding: 0px;margin: 0px;margin-left: 25px;">Data de Emissão: ' + hAPI.getCardValue("S0_dat_emissao") + '</p>');
		
		if ( hAPI.getCardValue("S0_complemento") == "SEMINOVOS" ) {
			
			obj.obs += '\n<p style="padding: 0px;margin: 0px;margin-left: 25px;">Placa: ' + hAPI.getCardValue("S0_placa") + '</p>';
			
		} else if ( hAPI.getCardValue("S0_complemento") ==  "OFICINA" ) {
			
			obj.obs += '\n<p style="padding: 0px;margin: 0px;margin-left: 25px;">Numero OS: ' + hAPI.getCardValue("S0_numOS") + '</p>';
			obj.obs += '\n<p style="padding: 0px;margin: 0px;margin-left: 25px;">Numero AF: ' + hAPI.getCardValue("S0_nraf") + '</p>';
			
		}
		
		if (hAPI.getCardValue("S0_complemento") != "NOTA FISCAL SEM PAGAMENTO") {
			obj.obs += parcelas();
		}
			
		switch (obj.hid_idAtividade) {
		case "0":
			//INICIO
		case "7":
			//INICIO
			atv7(obj)
			break;	
		case "9":
			//Gestor Imediato
			atv9(obj)
			break;	
		case "11":
			//CONTROLLER
			atv11(obj)
			break;
		case "13":
			//CEF
			atv13(obj)
			break;
		case "15":
			//Contas a Pagar
			atv15(obj)
			break;
		default:
			break;
		}
		
	}catch(e){
		log.info("DISPLAY FIELD ERROR :"+e)
	}
	
}

function parcelas() {
	
	var json = hAPI.getCardValue("hid_parcelas");
	var parcelas = JSON.parse(json);
	
	var msg = '\n<p style="padding: 0px;margin: 0px;margin-left: 25px;">Parcelas: </p>';
	
	for (var i in parcelas) {
		
		var parcela = parcelas[i];
		
		msg += '\n<p style="padding: 0px;margin: 0px;margin-left: 50px;">' 
			+ parcela.numero + 
			' - '
			+ parcela.data_vencimento + 
			' - R$'
			+ parcela.valor + 
			'</p>';
		
	}
	
	return msg;
	
}

function atv7(obj) {
	
	hAPI.setTaskComments(
			getValue("WKUser"), 
			getValue("WKNumProces"), 
			hAPI.getActualThread(1,getValue("WKNumProces"),getValue("WKNumState")), 
			obj.obs
			+ '\n<p style="padding: 0px;margin: 0px;margin-left: 25px;">Observação: ' + hAPI.getCardValue("S0_Obs") + '</p>');
	
}

function atv9(obj) {
	
	hAPI.setTaskComments(
			getValue("WKUser"), 
			getValue("WKNumProces"), 
			hAPI.getActualThread(1,getValue("WKNumProces"),getValue("WKNumState")), 
			obj.obs
			+ '\n<p style="padding: 0px;margin: 0px;margin-left: 25px;">Aprovação : ' + hAPI.getCardValue("S1_aprovacao") + '</p>'
			+ '\n<p style="padding: 0px;margin: 0px;margin-left: 25px;">Observação: ' + hAPI.getCardValue("S1_Obs") + '</p>');
	
}

function atv11(obj) {
	
	hAPI.setTaskComments(
			getValue("WKUser"), 
			getValue("WKNumProces"), 
			hAPI.getActualThread(1,getValue("WKNumProces"),getValue("WKNumState")), 
			obj.obs
			+ '\n<p style="padding: 0px;margin: 0px;margin-left: 25px;">Aprovação : ' + hAPI.getCardValue("S2_aprovacao") + '</p>'
			+ '\n<p style="padding: 0px;margin: 0px;margin-left: 25px;">Observação: ' + hAPI.getCardValue("S2_Obs") + '</p>');
	
}

function atv13(obj) {
	
	hAPI.setTaskComments(
			getValue("WKUser"), 
			getValue("WKNumProces"), 
			hAPI.getActualThread(1,getValue("WKNumProces"),getValue("WKNumState")), 
			obj.obs
			+ '\n<p style="padding: 0px;margin: 0px;margin-left: 25px;">Aprovação : ' + hAPI.getCardValue("S3_aprovacao") + '</p>'
			+ '\n<p style="padding: 0px;margin: 0px;margin-left: 25px;">Observação: ' + hAPI.getCardValue("S3_Obs") + '</p>');
	
}

function atv15(obj) {
	
	var prv = hAPI.getCardValue("S4_aprovacao");
	
	switch (hAPI.getCardValue("S4_aprovacao")) {
	
		case 'pendenciaFiscal' :
			prv = "Lançamento Fiscal com Inconsistência";
			break;
		case 'pendenciaInicio' :
			prv = "Falta Dados de Pagamento";
			break;
		case 'pendenciaGestor' :
			prv = "Cobrança de Juros";
			break;
		case 'sim':
			prv = "Pagamento Agendado/Efetuado";
			break;
	
	}
	
	hAPI.setTaskComments(
			getValue("WKUser"), 
			getValue("WKNumProces"), 
			hAPI.getActualThread(1,getValue("WKNumProces"),getValue("WKNumState")), 
			obj.obs
			+ '\n<p style="padding: 0px;margin: 0px;margin-left: 25px;">Aprovação : ' + prv + '</p>'
			+ '\n<p style="padding: 0px;margin: 0px;margin-left: 25px;">Observação: ' + hAPI.getCardValue("S4_Obs") + '</p>');
	
}